<?php

Route::middleware(['admin'])
    ->namespace('Admin')
    ->group(function () {
        Route::get('/', 'UserController@index');
        Route::get('curriculums/lessons', 'CurriculumController@getLessons')->name('curriculums.lessons');
        Route::delete('curriculums/lessons/{id}', 'CurriculumController@detachLesson')->name('curriculums.lessons.detach');
        Route::get('questions/get-list', 'QuestionController@getList')->name('questions.get-list');
        Route::resources([
            'users'           => 'UserController',
            'teachers'        => 'TeacherController',
            'students'        => 'StudentController',
            'business'        => 'BusinessAreaController',
            'knowledge'       => 'KnowledgeAreaController',
            'knowledge-type'  => 'KnowledgeTypeController',
            'lessons'         => 'LessonController',
            'curriculums'     => 'CurriculumController',
            'curriculum-type' => 'CurriculumTypeController',
            'questions'       => 'QuestionController',
            'tests'           => 'TestController',
        ]);
        Route::get('questions-lessons', 'QuestionController@indexLession')->name('questions.lessons');
    });
