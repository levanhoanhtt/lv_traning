<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('student')
    ->as('student.')
    ->group(function () {
        require base_path('routes/student.php');
    });

Route::prefix('teacher')
    ->as('teacher.')
    ->group(function () {
        Route::middleware(['admin'])
            ->namespace('Teacher')
            ->group(function () {
                Route::get('{lessonId}/index','QuestionController@index')->name('index');
                Route::post('{lessonId}/create-essay-questions','QuestionController@createEssayQuestions')->name('create-essay-questions');
                Route::post('{lessonId}/create-choice-questions','QuestionController@createChoiceQuestions')->name('create-choice-questions');
                Route::get('{lessonId}/questions','QuestionController@listQuestions')->name('lesson.questions');
                Route::get('{lessonId}/lessons','QuestionController@listLessons')->name('lesson.lessons');
                Route::get('{lessonId}/add-wish-list','QuestionController@addWishList')->name('lesson.add-wish-list');
            });
    });


Route::prefix('admin')
    ->as('admin.')
    ->group(function () {
        require base_path('routes/admin.php');
    });



