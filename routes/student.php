<?php

Route::middleware(['admin'])
    ->namespace('Student')
    ->group(function () {
    Route::get('index/{lessonId}','ForStudentController@index')->name('index');
    Route::get('detail','ForStudentController@show')->name('detail');
    Route::post('insert_synthetics/{lessonId}','ForStudentController@storeSynthetics')->name('storeSynthetics');
    Route::post('update_synthetics/{id}','ForStudentController@updateSynthetics')->name('updateSynthetics');
    Route::post('delete_synthetic', 'ForStudentController@deleteSynthetic')->name('deleteSynthetic');
    Route::post('insert_discuss/{lessonId}', 'ForStudentController@insertDiscuss')->name('insertDiscuss');
    Route::post('insert_sub_discuss/{userLessonId}', 'ForStudentController@insertSubDiscuss')->name('insertSubDiscuss');
    Route::post('like_discuss/{userLessonId}', 'ForStudentController@likeDiscuss')->name('likeDiscuss');
    Route::post('dislike_discuss/{userLessonId}', 'ForStudentController@dislikeDiscuss')->name('dislikeDiscuss');
    Route::post('user_lesson_log/{lessonId}', 'ForStudentController@userLessonLog')->name('userLessonLog');
});