<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('logs', function (Blueprint $table){
           $table->string('item_type')->change();
           $table->dropColumn('fullcontent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('logs', function (Blueprint $table){
            $table->unsignedInteger('item_type')->change();
            $table->mediumText('fullcontent');
        });
    }
}
