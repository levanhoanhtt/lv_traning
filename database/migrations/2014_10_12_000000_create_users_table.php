<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone_number')->nullable();
            $table->string('gender')->default('male');
            $table->date('birthday')->nullable();
            $table->string('email')->unique();
            $table->integer('role_id')->default(3);
            $table->string('company')->nullable();
            $table->string('department')->nullable();//bo phan lam viec
            $table->string('address')->nullable();
            $table->string('password');
            $table->string('level')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
        \App\Models\User::create([
            'name' => 'admin',
            'email'=> 'admin@admin.com',
            'password' => bcrypt('123456'),
            'role_id' => 1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
