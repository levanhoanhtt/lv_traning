<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableUserLesson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_lesson', function (Blueprint $table) {
            $table->dropColumn(['status']);
        });
        Schema::table('user_lesson', function (Blueprint $table) {
            $table->string('image', 256);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('user_lesson', function (Blueprint $table) {
            $table->dropColumn(['image']);
        });
        Schema::table('user_lesson', function (Blueprint $table) {
            $table->tinyInteger('status')->default(0);
        });
    }
}
