<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurriculumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculums', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('code')->unique();
            $table->unsignedInteger('admin_id');
            $table->string('image')->nullable();
            $table->string('curriculum_type_id');
            $table->tinyInteger('level')->default(1);
            $table->tinyInteger('status')->default(1);
            $table->unsignedInteger('price')->default(0);
            $table->tinyInteger('published')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('curriculum_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('addendums', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('curriculum_id');
            $table->text('note')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculums');
        Schema::dropIfExists('curriculum_type');
        Schema::dropIfExists('addendums');
    }
}
