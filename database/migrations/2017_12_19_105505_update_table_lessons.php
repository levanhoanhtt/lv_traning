<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableLessons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lessons', function (Blueprint $table) {
            $table->unsignedInteger('addendum_id')->after('id');
            $table->renameColumn('title', 'name');
            $table->text('content')->change();
            $table->string('file');
            $table->string('image')->nullable();
            $table->tinyInteger('level')->default(1);
            $table->smallInteger('status')->default(1)->change();
            $table->unsignedInteger('price')->default(0);
            $table->unsignedInteger('knowledge_area_id');
            $table->unsignedInteger('knowledge_type');
            $table->unsignedInteger('create_by');
            $table->softDeletes();
        });

        Schema::create('curriculum_lesson', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('curriculum_id');
            $table->unsignedInteger('lesson_id');
        });

        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('type');
            $table->timestamps();
        });

        Schema::create('itemtags', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lesson_id');
            $table->unsignedInteger('tag_id');
        });

        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('item_id');
            $table->unsignedInteger('item_type');
            $table->string('content');
            $table->mediumText('fullcontent');
            $table->unsignedInteger('created_by');
            $table->dateTime('created_at');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lessons', function (Blueprint $table) {
            $table->dropColumn(['addendum_id', 'file', 'image', 'level', 'price', 'knowledge_area_id', 'knowledge_type', 'create_by']);
            $table->renameColumn('name', 'title');
            $table->mediumText('content')->change();
            $table->integer('status')->change();
            $table->dropSoftDeletes();
        });

        Schema::dropIfExists('curriculum_lesson');
        Schema::dropIfExists('tags');
        Schema::dropIfExists('itemtags');
        Schema::dropIfExists('logs');
    }
}
