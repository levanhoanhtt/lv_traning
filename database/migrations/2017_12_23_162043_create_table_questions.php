<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lesson_id');
            $table->enum('type', ['essay', 'choice', 'match', 'missing ']); //tự luận/trắc nghiệm/ghép nối/khuyết thiếu
            $table->string('code')->unique();
            $table->text('content');
            $table->tinyInteger('preview_flg')->default(0);
            $table->tinyInteger('number_of_ans')->nullable();
            $table->tinyInteger('answer_type')->nullable();
            $table->string('image')->nullable();
            $table->string('url_video')->nullable();
            $table->tinyInteger('level')->default(0); //0:dễ , 1: trung bình, 2:khó
            $table->tinyInteger('question_type');
            $table->tinyInteger('approve_flg')->default(0); // 0: chờ duyệt, 1: Đã duyệt, 2: không duyệt, 3: xem xét lại
            $table->tinyInteger('apply_flg')->default(1); //0: Tạm dừng, 1: Đang áp dụng
            $table->unsignedInteger('created_by'); //người tạo
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
