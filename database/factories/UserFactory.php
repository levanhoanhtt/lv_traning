<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone_number' => $faker->phoneNumber,
        'status' => $faker->numberBetween(0, 2),
        'role_id' => $faker->numberBetween(1, 3),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Curriculum::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->unique()->numberBetween(0, 1000),
        'admin_id' => 1,
        'image' => $faker->imageUrl($width = 200, $height = 200),
        'curriculum_type_id' => $faker->numberBetween(1, 4),
    ];
});

$factory->define(App\Models\Addendum::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'curriculum_id' => $faker->numberBetween(1, 50),
    ];
});

$factory->define(App\Models\Lesson::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'addendum_id' => $faker->numberBetween(1, 50),
        'type' => 'video',
        'image' => 'http://via.placeholder.com/150x150',
        'content' => $faker->paragraph,
        'file' => 'test.mp4',
        'knowledge_area_id' => $faker->numberBetween(1, 50),
        'knowledge_type' => $faker->numberBetween(1, 50),
        'create_by' => $faker->numberBetween(1, 50)
    ];
});

$factory->define(App\Models\UserLesson::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 50),
        'lesson_id' => $faker->numberBetween(1, 50),
        'type' => $faker->numberBetween(1, 2),
        'status' => $faker->numberBetween(0, 2),
        'content' => $faker->paragraph,
        'title' => $faker->name,
    ];
});

$factory->define(App\Models\UserLessonSub::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 50),
        'user_lesson_id' => $faker->numberBetween(2100, 2300),
        'content' => $faker->paragraph,
        'image' => $faker->imageUrl($width = 200, $height = 200),
    ];
});
