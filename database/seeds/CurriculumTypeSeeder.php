<?php

use Illuminate\Database\Seeder;
use App\Models\CurriculumType;

class CurriculumTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	CurriculumType::truncate();
        CurriculumType::insert([
        	[
        		'name' => 'THPT'
        	],
        	[
        		'name' => 'Đại học chính quy'
        	],
        	[
        		'name' => 'Cao đẳng'
        	],
        	[
        		'name' => 'Trung cấp'
        	],
        ]);
    }
}
