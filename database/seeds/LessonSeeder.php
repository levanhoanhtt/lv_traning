<?php

use Illuminate\Database\Seeder;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //App\Models\Lesson::truncate();
        factory(App\Models\Lesson::class, 250)->create();
    }
}
