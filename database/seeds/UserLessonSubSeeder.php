<?php

use Illuminate\Database\Seeder;

class UserLessonSubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Models\UserLessonSub::class, 500)->create();
    }
}
