<?php

use Illuminate\Database\Seeder;

class AddendumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Models\Addendum::class, 250)->create();
    }
}
