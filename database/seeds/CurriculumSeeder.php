<?php

use Illuminate\Database\Seeder;

class CurriculumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        App\Models\Curriculum::truncate();
        factory(App\Models\Curriculum::class, 50)->create();
    }
}
