<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessKnowledge extends Model
{
    protected $table = 'business_knowledge';

    protected $fillable = [
        'business_id',
        'knowledge_id',
    ];
}
