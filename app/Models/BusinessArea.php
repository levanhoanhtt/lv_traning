<?php

namespace App\Models;

use App\Presenters\CommonPresenters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laracodes\Presenter\Traits\Presentable;

class BusinessArea extends Model
{
    use Notifiable, SoftDeletes, Presentable;

    const BUSINESSAREA_STATUS = [
        1 => 'Đang hoạt động',
        2 => 'Tạm dừng',
        3 => 'Đã dừng'
    ];

    protected $table = 'business_area';

    protected $fillable = [
        'name',
        'code',
        'description',
        'status'
    ];

    public function knowledge()
    {
        return $this->belongsToMany('App\Models\KnowledgeArea', 'business_knowledge', 'business_id', 'knowledge_id');
    }

    protected $presenter = CommonPresenters::class;
}
