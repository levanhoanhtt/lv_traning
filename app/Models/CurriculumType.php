<?php

namespace App\Models;

use App\Presenters\CurriculumTypePresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laracodes\Presenter\Traits\Presentable;

class CurriculumType extends Model
{
    use Notifiable, SoftDeletes, Presentable;

    protected $table = 'curriculum_type';

    protected $fillable = [
        'id',
        'name',
        'status',
        'deleted_at',
        'created_at'
    ];

    /**
     * The presenter class.
     *
     * @var string
     */
    protected $presenter = CurriculumTypePresenter::class;
}
