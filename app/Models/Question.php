<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracodes\Presenter\Traits\Presentable;
use App\Presenters\CommonPresenters;

class Question extends Model
{
    use Notifiable, SoftDeletes, Presentable;

    protected $fillable = [
        'lesson_id',
        'type',
        'code',
        'content',
        'preview_flg',
        'number_of_ans',
        'answer_type',
        'image',
        'url_video',
        'level',
        'question_type',
        'approve_flg',
        'apply_flg',
        'created_by'
    ];

    public function getImagelAttribute($value){
        return asset('/storage/questions').'/'.$this->id.'/'.$value;
    }

    /**
     * Bài học
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lesson(){
        return $this->belongsTo('App\Models\Lesson', 'lesson_id');
    }

    /**
     * Người tạo
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creater(){
        return $this->belongsTo('App\Models\User', 'created_by');
    }

    /**
     * Người tạo
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function answers(){
        return $this->hasMany(Answer::class);
    }

    protected $presenter = CommonPresenters::class;
}
