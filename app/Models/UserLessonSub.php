<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLessonSub extends Model
{
    //
    protected $table = 'user_lesson_sub';
    protected $fillable = [
        'user_id',
        'user_lesson_id',
        'content',
        'image',
        'deleted_at',
        'created_at'
    ];

    public function userLesson()
    {
        return $this->belongsTo('App\Models\UserLesson', 'user_lesson_id');
    }

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
