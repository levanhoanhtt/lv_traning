<?php

namespace App\Models;

use App\Presenters\LessonPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laracodes\Presenter\Traits\Presentable;

class Lesson extends Model
{
    use Notifiable, SoftDeletes, Presentable;
    //
    protected $fillable = [
        'name',
        'content',
        'type',
        'note',
        'status',
        'addendum_id',
        'knowledge_area_id',
        'knowledge_type',
        'create_by',
        'file',
        'thumbnail'
    ];


    const LESSON_TYPE_TEXT = 'text';
    const LESSON_TYPE_VIDEO = 'video';
    const LESSON_TYPE_AUDIO = 'audio';
    public function addendums()
    {
        return $this->belongsTo('App\Models\Addendum', 'addendum_id');
    }

    public function userLesson()
    {
        return $this->hasMany('App\Models\UserLesson');
    }

    public function knowledgeArea()
    {
        return $this->belongsTo('App\Models\KnowledgeArea', 'knowledge_area_id');
    }

    public function knowledgeType()
    {
        return $this->belongsTo('App\Models\KnowledgeType', 'knowledge_type');
    }

    public function getThumbnailAttribute($value){
        return asset('/storage/lessons/thumbnail').'/'.$this->id.'/'.$value;
    }

    public function getFileAttribute($value){
        return asset('/storage/lessons').'/'.$this->id.'/'.$value;
    }

    /**
     * return list essay questions
     */
    public function questions($type = null, $userId = null)
    {
        $rs = $this->hasMany('App\Models\Question', 'lesson_id', 'id');
        if ($type) {
            $rs->where('type', $type);
        }
        if ($userId) {
            $rs->where('created_by', $userId);
        }
        return $rs;
    }

    /**
     * return list Q_ESSAY questions
     */
    public function questionsEssay(){
        return  $this->hasMany('App\Models\Question', 'lesson_id', 'id')->where('type', Q_ESSAY);
    }

    /**
     * return list Q_ESSAY User questions
     */
    public function questionsEssayUser(){
        return  $this->hasMany('App\Models\Question', 'lesson_id', 'id')->where('type', Q_ESSAY)->where('created_by', user()->id);
    }

    /**
     * return list Q_CHOICE questions
     */
    public function questionsChoice(){
        return  $this->hasMany('App\Models\Question', 'lesson_id', 'id')->where('type', Q_CHOICE);
    }

    /**
     * return list Q_CHOICE User questions
     */
    public function questionsChoiceUser(){
        return  $this->hasMany('App\Models\Question', 'lesson_id', 'id')->where('type', Q_CHOICE)->where('created_by', user()->id);
    }

    /**
     * return list tesquestions
     */
    public function testQuestions()
    {
        return $this->belongsToMany(Question::class, 'test_question', 'test_id', 'question_id')->withPivot('order_display')
            ->withTimestamps();
    }

    public function logs(){
        return $this->morphMany('App\Models\Log', 'item');
    }

    /**
     * The presenter class.
     *
     * @var string
     */
    protected $presenter = LessonPresenter::class;

}
