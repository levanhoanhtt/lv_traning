<?php

namespace App\Models;

use App\Presenters\UserPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laracodes\Presenter\Traits\Presentable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, Presentable;

    const USER_STATUS = [
        'Bình thường',
        'Đã nghỉ',
        'Tạm nghỉ'
    ];
    /**
     * @var string
     */
    const UPLOAD_DISK = 'public';

    /**
     * @var string
     */
    const AVATAR_DIR = 'upload/users/avatar';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone_number',
        'gender',
        'birthday',
        'role_id',
        'company',
        'department',
        'address',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'name'         => 'required|min:2',
        'email'        => 'required|email|unique:users',
        'password'     => 'required|min:6',
        'phone_number' => 'required|unique:users',
        'gender'       => 'in:male,female',
        'birthday'     => 'nullable|date|before:today',
        'avatar'       => 'image',
        'address'      => 'nullable|min:5',
        'department'   => 'nullable|min:5',
        'company'      => 'nullable|min:5',
    ];


    protected $dates = ['birthday'];


    public function role(){
        return $this->belongsTo('App\Models\Role', 'role_id');
    }
    /**
     * The presenter class.
     *
     * @var string
     */
    protected $presenter = UserPresenter::class;

    public function userLesson()
    {
        return $this->hasMany('App\Models\UserLesson');
    }

    public function userLessonSub()
    {
        return $this->hasMany('App\Models\userLessonSub');
    }
}
