<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    //
    const UPDATED_AT = null;
    protected $fillable = [
        'item_id',
        'item_type',
        'content',
        'fullcontent',
        'created_by'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'created_by');
    }
}
