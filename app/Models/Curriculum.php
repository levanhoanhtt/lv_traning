<?php

namespace App\Models;

use App\Presenters\CurriculumPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laracodes\Presenter\Traits\Presentable;

class Curriculum extends Model
{
    use Notifiable, SoftDeletes, Presentable;

    const INTERNAL = 0;
    const TRADE    = 1;

    protected $table = 'curriculums';

    /**
     * @var string
     */
    const UPLOAD_DISK = 'public';

    /**
     * @var string
     */
    const IMAGE_DIR = 'upload/curriculums/image';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'admin_id',
        'level',
        'status',
        'price',
        'published',
        'image',
        'curriculum_type_id',
        'deleted_at',
        'created_at'
    ];
    public function addendums()
    {
        return $this->hasMany('App\Models\Addendum');
    }

    /**
     * The presenter class.
     *
     * @var string
     */
    protected $presenter = CurriculumPresenter::class;

    /**
     * Get all addendums.
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */

}
