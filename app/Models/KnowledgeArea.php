<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class KnowledgeArea extends Model
{
    use Notifiable, SoftDeletes;

    protected $table = 'knowledge_area';

    protected $fillable = [
        'name',
        'code',
        'description',
    ];

    public function business()
    {
        return $this->belongsToMany('App\Models\BusinessArea', 'business_knowledge', 'knowledge_id', 'business_id');
    }
}
