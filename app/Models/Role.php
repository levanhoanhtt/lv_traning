<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
     const ROLE_ADMIN = 1;
     const ROLE_TEACHER = 2;
     const ROLE_STUDENT = 3;

     public function users()
     {
     	return $this->hasMany('App\Models\User');
     }
}
