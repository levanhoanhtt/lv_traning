<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Addendum extends Model
{
    use Notifiable, SoftDeletes;

    protected $table = 'addendums';

    protected $fillable = [
        'id',
        'name',
        'curriculum_id',
        'note',
        'image',
        'status',
        'deleted_at',
        'created_at',
        'note',
    ];
    
    public function curriculums()
    {
        return $this->belongsTo('App\Models\Curriculum', 'curriculum_id');
    }


    /**
     * Get all lessons.
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function lessons()
    {
        return $this->hasMany(Lesson::class)->orderBy('order_display');
    }

    public function getShowAttribute()
    {
        return false;
    }
    protected $appends = ['show'];
}
