<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class KnowledgeType extends Model
{
    use Notifiable, SoftDeletes;

    protected $table = 'knowledge_type';

    protected $fillable = [
        'name',
        'description',
    ];
}
