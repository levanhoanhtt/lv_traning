<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class UserLesson extends Model
{
    use Notifiable, SoftDeletes;

    protected $table = 'user_lesson';

    protected $fillable = [
        'id',
        'user_id',
        'lesson_id',
        'type',
        'status',
        'title',
        'content',
        'deleted_at',
        'created_at'
    ];

    const USER_LESSON_TYPE_ID_SYNTHETICS = 1;
    const USER_LESSON_TYPE_ID_DISCUSS = 2;
    
    public function users()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function lessons()
    {
        return $this->belongsTo('App\Models\Lesson', 'lesson_id');
    }

    public function userLessonSub()
    {
        return $this->hasMany('App\Models\UserLessonSub');
    }

    public function userLessonLike() {
        return $this->hasMany('App\Models\UserLessonLike');
    }
}
