<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class WishList extends Model
{
    use Notifiable;

    protected $table = 'wish_lists';
}
