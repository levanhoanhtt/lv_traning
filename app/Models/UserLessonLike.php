<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLessonLike extends Model
{
    //
    const TYPE_NORMAL = 0;
    const TYPE_LIKE = 1;
    const TYPE_DISLIKE = 2;
    protected $table = 'user_lesson_like';
    protected $fillable = [
        'user_id',
        'user_lesson_id',
        'type',
        'deleted_at',
        'created_at'
    ];

    public function userLesson()
    {
        return $this->belongsTo('App\Models\UserLesson', 'user_lesson_id');
    }
}
