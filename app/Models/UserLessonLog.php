<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLessonLog extends Model
{
    //
    const STATUS_PAUSE = 1;
    const STATUS_SEEK = 2;
    const STATUS_COMPLETE = 3;
    protected $table = 'user_lesson_log';
    protected $fillable = [
        'user_id',
        'lesson_id',
        'status',
        'start',
        'end',
        'count',
        'date',
        'deleted_at',
        'created_at'
    ];
}
