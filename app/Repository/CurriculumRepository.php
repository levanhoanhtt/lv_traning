<?php

namespace App\Repository;

use App\Models\Curriculum;

class CurriculumRepository
{
    public function getList(){
        return Curriculum::where('status', 1)->pluck('name','id');
    }
    public function findOrFail($id){
        return Curriculum::findOrFail($id);
    }
    public function findFeilds($input, $itemsPerPage = 10){
        return Curriculum::paginate($itemsPerPage);
    }

    public function insert($info){
        Curriculum::create($info);
    }

    public function save($request, $businessArea){
        $data = $request->all();
        $businessArea->update($data);
    }

    public function delete($id){
        Curriculum::destroy($id);
    }
}