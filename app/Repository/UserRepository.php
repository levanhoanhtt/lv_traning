<?php
namespace App\Repository;
use App\Models\Role;
use App\Models\User;

/**
 * Created by PhpStorm.
 * User: admin-pc
 * Date: 12/10/2017
 * Time: 5:08 PM
 */
class UserRepository
{
    public function findOrFail($id){
        return User::findOrFail($id);
    }
    public function findTeacher($input, $itemsPerPage = 10){
        $teachers = User::where('role_id', Role::ROLE_TEACHER)->with('role');
        if(isset($input['name'])){
            $teachers = $teachers->where('name', 'like', '%'.$input['name'].'%');
        }
        $teachers = $teachers->paginate($itemsPerPage);
        return $teachers;
    }
    
    public function findStudent($input, $itemsPerPage = 10){
        $users = User::where('role_id', Role::ROLE_STUDENT)->with('role');
        if(isset($input['name'])){
            $users = $users->where('name', 'like', '%'.$input['name'].'%');
        }
        $users = $users->paginate($itemsPerPage);
        return $users;
    }

    public function insert($info){
        $info['password'] = bcrypt($info['password']);
        User::create($info);
    }

    public function save($request, $teacher){
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        if($request['password'] == ''){
            $data = $request->except('password');
        }
        $teacher->update($data);
    }

    public function delete($id){
        User::destroy($id);
    }
}
