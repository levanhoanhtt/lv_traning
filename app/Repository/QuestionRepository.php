<?php
namespace App\Repository;

use App\Http\Requests\QuestionRequest;
use App\Models\Question;

class QuestionRepository
{
    public function findQuestions($input, $itemsPerPage = 10){
        $questions = Question::with('lesson', 'creater');
        if(isset($input['content'])){
            $questions = $questions->where(function ($query) use($input) {
                $query->where('content', 'like', '%'.trim($input['content']).'%')
                        ->orWhere('code', 'like', '%'.trim($input['content']).'%');
            });
        }
        if(isset($input['k_area']) || isset($input['k_type'])){
            $questions = $questions->whereHas('lesson', function ($query) use($input) {
                $query->where('knowledge_area_id', $input['k_area'])
                        ->where('knowledge_type', $input['k_type']);
            });
        }
        if(isset($input['q_type'])){
            $questions = $questions->where('question_type', $input['q_type']);
        }
        if(isset($input['type'])){
            $questions = $questions->where('type', $input['type']);
        }
        if(isset($input['level'])){
            $questions = $questions->where('level', $input['level']);
        }
        if(isset($input['apply'])){
            $questions = $questions->where('apply_flg', $input['apply']);
        }
        if(isset($input['approve'])){
            $questions = $questions->where('approve_flg', $input['approve']);
        }
        $questions = $questions->paginate($itemsPerPage);
        return $questions;
    }
    
    public function findOrFail($id){
        return Question::findOrFail($id);
    }

    public function insert($info){
        return Question::create($info);
    }

    public function update($question, $info){
        $question->update($info);
    }

    public function saveFile(QuestionRequest $request, Question $question){
        if($request->file('image')){
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $location = public_path('storage\questions').'/'.$question->id.'/';
            $file->move($location, $fileName);
            $question->image = $fileName;
            $question->save();
        }
    }
}