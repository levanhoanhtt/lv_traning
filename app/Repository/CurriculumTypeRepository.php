<?php

namespace App\Repository;

use App\Models\CurriculumType;

class CurriculumTypeRepository
{
    public function findOrFail($id)
    {
        return CurriculumType::findOrFail($id);
    }

    public function findFeilds($input, $itemsPerPage = 10)
    {
        return CurriculumType::paginate($itemsPerPage);
    }
    public function insert($info)
    {
        CurriculumType::create($info);
    }

    public function save($request, $curriculumType)
    {
        $data = $request->all();
        $curriculumType->update($data);
    }

    public function delete($id)
    {
        CurriculumType::destroy($id);
    }
}
