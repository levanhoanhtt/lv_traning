<?php

namespace App\Repository;

use App\Models\KnowledgeType;

class KnowledgeTypeRepository
{
    public function findOrFail($id){
        return KnowledgeType::findOrFail($id);
    }
    public function findFeilds($input, $itemsPerPage = 10){
        return KnowledgeType::paginate($itemsPerPage);
    }

    public function insert($info){
        KnowledgeType::create($info);
    }

    public function save($request, $knowledgeType){
        $data = $request->all();
        $knowledgeType->update($data);
    }

    public function delete($id){
        KnowledgeType::destroy($id);
    }
}