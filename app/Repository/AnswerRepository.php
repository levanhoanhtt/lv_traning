<?php
namespace App\Repository;
use App\Http\Requests\AnswerRequest;
use App\Models\Answer;

class AnswerRepository
{
    public function findOrFail($id){
        return Answer::findOrFail($id);
    }

    public function insert($info){
        return Answer::create($info);
    }

    public function update($answer, $info){
        $answer->update($info);
    }

    public function saveFile($request, Answer $answer){
        if($request){
            $file = $request;
            $fileName = $file->getClientOriginalName();
            $location = public_path('storage\answers').'/'.$answer->id.'/';
            $file->move($location, $fileName);
            $answer->image = $fileName;
            $answer->save();
        }
    }
}