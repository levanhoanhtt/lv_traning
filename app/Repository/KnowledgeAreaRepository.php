<?php

namespace App\Repository;

use App\Models\KnowledgeArea;

class KnowledgeAreaRepository
{
    public function findOrFail($id){
        return KnowledgeArea::findOrFail($id);
    }
    public function findFeilds($input, $itemsPerPage = 10){
        return KnowledgeArea::paginate($itemsPerPage);
    }

    public function insert($info){
        $business  = [];
        if (!empty($info['business'])) {
            $business = array_merge($info['business']);
        }
        $ka = KnowledgeArea::create($info);
        $ka->business()->sync($business);
    }

    public function save($request, $knowledgeArea){
        $data = $request->all();
        $business  = [];
        if (!empty($data['business'])) {
            $business = array_merge($data['business']);
        }
        $knowledgeArea->update($data);
        $knowledgeArea->business()->sync($business);
    }

    public function delete($id){
        $knowledgeArea = KnowledgeArea::findOrFail($id);
        $knowledgeArea->business()->detach();
        $knowledgeArea->destroy($id);
    }
}