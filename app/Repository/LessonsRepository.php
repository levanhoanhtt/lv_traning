<?php
/**
 * Created by PhpStorm.
 * User: admin-pc
 * Date: 12/16/2017
 * Time: 11:05 PM
 */

namespace App\Repository;


use App\Http\Requests\LessonRequest;
use App\Models\Lesson;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;


class LessonsRepository
{
    public function findLessons($input, $itemsPerPage = 10, $without_test = true){
        $lessons = Lesson::with('knowledgeArea', 'knowledgeType');
        if ($without_test) {
            $lessons = $lessons->where('is_test', 0);
        }
        if(isset($input['name'])){
            $lessons = $lessons->where('name', 'like', '%'.trim($input['name']).'%');
        }
        if(isset($input['knowledge_area_id'])){
            $lessons = $lessons->where('knowledge_area_id', $input['knowledge_area_id']);
        }
        if(isset($input['knowledge_type'])){
            $lessons = $lessons->where('knowledge_type', $input['knowledge_type']);
        }
        if(isset($input['type'])){
            $lessons = $lessons->where('type', $input['type']);
        }
        if(isset($input['level'])){
            $lessons = $lessons->where('level', $input['level']);
        }
        if(isset($input['status'])){
            $lessons = $lessons->where('status', $input['status']);
        }
        $lessons = $lessons->paginate($itemsPerPage);
        return $lessons;
    }

    public function findOrFail($id){
        return Lesson::findOrFail($id);
    }

    public function insert($info){

        $lesson =  Lesson::create($info);
        $logData = [
            'created_by' => Auth::user()->id,
            'content' => 'tạo bài học',
            'item_id' => $lesson->id,
            'item_type' => 'lesson'
        ];
        $this->saveLog($logData);
        return $lesson;
    }

    public function update($lesson, $info){
        $logData = [
            'created_by' => Auth::user()->id,
            'content' => 'sửa bài học',
            'item_id' => $lesson->id,
            'item_type' => 'lesson'
        ];
        $this->saveLog($logData);
        $lesson->update($info);
    }

    public function saveFile($request, $lesson){
        if($request->file('file')){
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $location = public_path('storage\lessons').'/'.$lesson->id.'/';
            $file->move($location, $fileName);
            $lesson->file=$fileName;
            $lesson->save();
        }
        if($request->file('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailName = $thumbnail->getClientOriginalName();
            $location = public_path('storage\lessons\thumbnail').'/'.$lesson->id.'/';
            $thumbnail->move($location, $thumbnailName);
            $lesson->thumbnail = $thumbnailName;
            $lesson->save();
        }
    }

    public function saveLog($data){
        Log::create($data);
    }
}