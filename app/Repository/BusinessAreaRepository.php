<?php

namespace App\Repository;

use App\Models\BusinessArea;

class BusinessAreaRepository
{
    public function getList(){
        return BusinessArea::where('status', 1)->pluck('name','id');
    }
    public function findOrFail($id){
        return BusinessArea::findOrFail($id);
    }
    public function findFeilds($input, $itemsPerPage = 10){
        return BusinessArea::paginate($itemsPerPage);
    }

    public function insert($info){
        BusinessArea::create($info);
    }

    public function save($request, $businessArea){
        $data = $request->all();
        $businessArea->update($data);
    }

    public function delete($id){
        BusinessArea::destroy($id);
    }
}