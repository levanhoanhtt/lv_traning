<?php

namespace App\Presenters;

use App\Models\CurriculumType;
use Laracodes\Presenter\Presenter;

class CurriculumTypePresenter extends Presenter
{

    /**
     * Get status user.
     *
     * @return string
     */
    public function status()
    {
        if ($this->model->status == 1) {
            return '<span class="label label-success">Active</span>';
        } else {
            return '<span class="label label-primary">Inactive</span>';
        }
    }
}
