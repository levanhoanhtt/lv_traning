<?php

namespace App\Presenters;

use Laracodes\Presenter\Presenter;

class CommonPresenters extends Presenter
{

    /**
     * Get status.
     *
     * @return string
     */
    public function status()
    {
        if ($this->model->status == 1) {
            return '<span class="label label-success">Đang hoạt đông</span>';
        } else  if ($this->model->status == 2) {
            return '<span class="label label-primary">Tạm dừng</span>';
        } else {
            return '<span class="label label-danger">Đã dừng</span>';
        }
    }

    /**
     * Get question level.
     *
     * @return string
     */
    public function level()
    {
        if ($this->model->level == Q_EASY) {
            return '<span class="label label-success">Dễ</span>';
        } else  if ($this->model->level == Q_MEDIUM) {
            return '<span class="label label-primary">Trung bình</span>';
        } else {
            return '<span class="label label-danger">Khó</span>';
        }
    }

    /**
     * Get question apply.
     *
     * @return string
     */
    public function apply()
    {
        if ($this->model->apply_flg == Q_PAUSE) {
            return '<span class="label label-danger">Tạm dừng</span>';
        } else {
            return '<span class="label label-success">Đang áp dụng</span>';
        }
    }

    /**
     * Get question approve.
     *
     * @return string
     */
    public function approve()
    {
        if ($this->model->approve_flg == Q_PENDING) {
            return '<span class="label label-success">Chờ duyệt</span>';
        } else  if ($this->model->approve_flg == Q_APPROVED) {
            return '<span class="label label-primary">Đã duyệt</span>';
        } else  if ($this->model->approve_flg == Q_CANCLE) {
            return '<span class="label label-warning">Không duyệt</span>';
        } else {
            return '<span class="label label-danger">Xem xét</span>';
        }
    }
}