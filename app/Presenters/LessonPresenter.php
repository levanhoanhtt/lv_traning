<?php

namespace App\Presenters;

use Laracodes\Presenter\Presenter;

class LessonPresenter extends Presenter
{
    /**
     * Get lesson level.
     *
     * @return string
     */
    public function level()
    {
        if ($this->model->level == 0) {
            return '<span class="label label-success">Dễ</span>';
        } else  if ($this->model->level == 1) {
            return '<span class="label label-primary">Trung bình</span>';
        } else {
            return '<span class="label label-danger">Khó</span>';
        }
    }

    /**
     * Get status lesson.
     *
     * @return string
     */
    public function status()
    {
        if ($this->model->status == 1) {
            return '<span class="label label-success">Hoạt động</span>';
        } else {
            return '<span class="label label-danger">Không hoạt động</span>';
        }
    }
}