<?php

namespace App\Presenters;

use App\Models\Curriculum;
use Laracodes\Presenter\Presenter;

class CurriculumPresenter extends Presenter
{
    /**
     * Get image image url.
     *
     * @param  string $size
     * @return string
     */
    public function image($size = 'original')
    {
        $image = $this->model->image ? str_replace('upload/', '', $this->model->image) : 'no_image.png';

        return image_cache_url($image, $size);
    }

    /**
     * Get status user.
     *
     * @return string
     */
    public function status()
    {
        if ($this->model->status == 1) {
            return '<span class="label label-success">Active</span>';
        } else {
            return '<span class="label label-primary">Inactive</span>';
        }
    }
}
