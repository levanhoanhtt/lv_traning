<?php

namespace App\Presenters;

use App\Models\User;
use Laracodes\Presenter\Presenter;

class UserPresenter extends Presenter
{
    /**
     * Get avatar image url.
     *
     * @param  string $size
     * @return string
     */
    public function avatar($size = 'original')
    {
        $avatar = $this->model->avatar ? str_replace('upload/', '', $this->model->avatar) : 'no_image.png';

        return image_cache_url($avatar, $size);
    }

    /**
     * Get status user.
     *
     * @return string
     */
    public function status()
    {
        if ($this->model->status == 1) {
            return '<span class="label label-success">Active</span>';
        } else {
            return '<span class="label label-primary">Inactive</span>';
        }
    }
}
