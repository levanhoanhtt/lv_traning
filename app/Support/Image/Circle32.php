<?php

namespace App\Support\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Circle32 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(32, 32);
    }
}
