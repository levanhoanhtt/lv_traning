<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $req)
    {
        if ($req->type == Q_ESSAY) {
            return [
                'content' => 'required',
                'answer' => 'required',
            ];
        } else {
            return [
                'content_choice' => 'required',
                'answer.*' => 'required',
            ];
        }
    }
}
