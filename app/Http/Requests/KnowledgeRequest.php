<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class KnowledgeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $req)
    {
        if ($req->knowledge) {
            return [
                //
                'name' => 'required|max:191',
                'code' => 'required|unique:knowledge_area,code,'.$req->knowledge,
                'business' => 'required'
            ];
        }
        return [
            //
            'name' => 'required|max:191',
            'code' => 'required|unique:knowledge_area,code',
            'business' => 'required'
        ];
    }
}
