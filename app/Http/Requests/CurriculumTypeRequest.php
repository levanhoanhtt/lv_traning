<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CurriculumTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $req)
    {
        if ($req->curriculum_type) {
            return [
                //
                'name' => 'required|unique:curriculum_type,name,'.$req->curriculum_type
            ];
        }
        return [
            //
            'name' => 'required|unique:curriculum_type,name',
        ];
    }
}
