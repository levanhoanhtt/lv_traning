<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\ForStudentRepository;
use App\Models\Curriculum;
use App\Models\Lesson;
use Auth;
use App\Models\UserLesson;
use Illuminate\Support\Facades\View;
use Validator;
use DB;
use Carbon\Carbon;
use App\Models\UserLessonSub;
use App\Models\UserLessonLike;
use App\Models\UserLessonLog;

class ForStudentController extends Controller
{
    public $forStudent;

    public function __construct(ForStudentRepository $forStudent)
    {
        $this->forStudent = $forStudent;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lessonId)
    {
        $user = Auth::user();
        $lesson = Lesson::find($lessonId);
        if ($lesson == null) {
            return back();
        }

        $synthetics = $lesson->userLesson()
            ->where('user_id', $user->id)
            ->where('type', UserLesson::USER_LESSON_TYPE_ID_SYNTHETICS)
            ->where('deleted_at', null)
            ->orderBy('created_at', 'desc')
            ->get();
        $discuss = $lesson->userLesson()
            ->where('type', UserLesson::USER_LESSON_TYPE_ID_DISCUSS)
            ->where('deleted_at', null)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('student.index')
            ->with('lessons', $lesson)
            ->with('synthetics', $synthetics)
            ->with('discuss', $discuss)
            ->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('student.detail-post');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storeSynthetics(Request $request, $lessonId)
    {
        if ($request->ajax()) {
            $user = Auth::user();
            $rules = array(
                'title' => 'required|max:256',
                'content' => 'required'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $html = View::make('student.validate')
                    ->withErrors($validator)
                    ->render();
                return response()->json(['html' => $html, 'status' => 'error']);
            }

            $id = DB::table('user_lesson')->insertGetId(array(
                'user_id' => $user->id,
                'lesson_id' => $lessonId,
                'status' => 1,
                'title' => $request->title,
                'content' => $request->content
            ));

            $synthetic = UserLesson::find($id);
            $html = View::make('student.synthetics')
                ->with('synthetic', $synthetic)
                ->render();
            return response()->json(['html' => $html, 'status' => 'success']);
        }
    }

    public function updateSynthetics(Request $request, $id)
    {
        if ($request->ajax()) {
            $user = Auth::user();
            $rules = array(
                'title' => 'required|max:256',
                'content' => 'required'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $html = View::make('student.validate')
                    ->withErrors($validator)
                    ->render();
                return response()->json(['html' => $html, 'status' => 'error']);
            }

            DB::table('user_lesson')
                ->where('id', $id)
                ->update(array(
                    'title' => $request->title,
                    'content' => $request->content,
                    'updated_at' => Carbon::now()
                )
            );
            $synthetic = UserLesson::find($id);
            $request->session()->flash('success', 'Chỉnh sửa thành công!');
            $html = View::make('student.validate')
                ->render();
            return response()->json(['synthetic' => $synthetic, 'status' => 'success', 'html' => $html]);
        }
    }

    public function deleteSynthetic(Request $request) {
        if ($request->ajax()) {
            $synthetic = UserLesson::find($request->userLessonId);
            if (!$synthetic) {
                return response()->json(['status' => 'error']);
            } else {
                $synthetic->delete();
                return response()->json(['status' => 'success']);
            }
        }
    }

    public function insertDiscuss(Request $request, $lessonId)
    {
        if ($request->ajax()) {
            $user = Auth::user();
            $filename = '';
            if ($request->hasFile('image_discuss')) {
                $validator = Validator::make($request->all(),
                    [
                        'image_discuss' => 'image',
                    ],
                    [
                        'image_discuss.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                    ]
                );
                if ($validator->fails()) {
                    $html = View::make('student.validate')
                        ->withErrors($validator)
                        ->render();
                    return response()->json(['html' => $html, 'status' => 'error']);
                }
                $extension = $request->file('image_discuss')->getClientOriginalExtension();
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $dir = 'student/uploads/discuss/';
                $request->file('image_discuss')->move($dir, $filename);
            }
            $userLessonData = array(
                'user_id' => $user->id,
                'lesson_id' => $lessonId,
                'type' => UserLesson::USER_LESSON_TYPE_ID_DISCUSS,
                'content' => $request->discuss,
                'image' => $filename,
                'created_at' => Carbon::now(),
                'title' => '',
            );
            $id = DB::table('user_lesson')->insertGetId($userLessonData);

            $discuss = UserLesson::find($id);
            $html = View::make('student.discuss')
                ->with('discuss', $discuss)
                ->with('user', $user)
                ->render();
            return response()->json(['status' => 'success', 'html' => $html]);
        }
    }

    public function insertSubDiscuss(Request $request, $userLessonId)
    {
        if ($request->ajax()) {
            $user = Auth::user();
            $filename = '';
            $content = $request->content ? $request->content : '';
            if ($request->hasFile('image_sub_discuss')) {
                $extension = $request->file('image_sub_discuss')->getClientOriginalExtension();
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $dir = 'student/uploads/subDiscuss/';
                $request->file('image_sub_discuss')->move($dir, $filename);
            }

            $userLessonSubData = array(
                'user_id' => $user->id,
                'user_lesson_id' => $userLessonId,
                'content' => $content,
                'image' => $filename,
                'created_at' => Carbon::now(),
            );
            $id = DB::table('user_lesson_sub')->insertGetId($userLessonSubData);
            $subDiscuss = UserLessonSub::find($id);
            $html = View::make('student.discussSub')
                ->with('userLessonSub', $subDiscuss)
                ->with('user', $user)
                ->render();

            return response()->json(['status' => 'success', 'html' => $html]);
        }
    }

    public function likeDiscuss(Request $request, $userLessonId)
    {
        if ($request->ajax()) {
            $user = Auth::user();
            $status = '';
            $like = UserLessonLike::where('user_id', $user->id)
                ->where('user_lesson_id', $userLessonId)
                ->where('type', UserLessonLike::TYPE_LIKE)
                ->first();
            if ($like == null) {
                $like = UserLessonLike::where('user_id', $user->id)
                    ->where('user_lesson_id', $userLessonId)
                    ->where('type', UserLessonLike::TYPE_NORMAL)
                    ->first();
                if ($like != null) {
                    // update like
                    UserLessonLike::where('user_id', $user->id)
                        ->where('user_lesson_id', $userLessonId)
                        ->where('type', UserLessonLike::TYPE_NORMAL)
                        ->delete();
                }
                // insert like
                UserLessonLike::insert([
                    [
                        'user_id' => $user->id,
                        'user_lesson_id' => $userLessonId,
                        'type' => UserLessonLike::TYPE_LIKE
                    ]
                ]);
                // $status = 1 -> like
                $status = 1;
            } else {
                // bo like
                UserLessonLike::where('user_id', $user->id)
                    ->where('user_lesson_id', $userLessonId)
                    ->where('type', UserLessonLike::TYPE_LIKE)
                    ->delete();
                // $status = 2 -> bo like
                $status = 2;
            }
            $countLike = UserLessonLike::where('user_lesson_id', $userLessonId)
                ->where('type', UserLessonLike::TYPE_LIKE)
                ->get();
            return response()->json(['status' => $status, 'count' => $countLike->count()]);
        }
    }

    public function dislikeDiscuss(Request $request, $userLessonId)
    {
        if ($request->ajax()) {
            $user = Auth::user();
            $status = '';
            $disLike = UserLessonLike::where('user_id', $user->id)
                ->where('user_lesson_id', $userLessonId)
                ->where('type', UserLessonLike::TYPE_DISLIKE)
                ->first();
            if ($disLike == null) {
                $disLike = UserLessonLike::where('user_id', $user->id)
                    ->where('user_lesson_id', $userLessonId)
                    ->where('type', UserLessonLike::TYPE_NORMAL)
                    ->first();
                if ($disLike != null) {
                    // update dislike
                    UserLessonLike::where('user_id', $user->id)
                        ->where('user_lesson_id', $userLessonId)
                        ->where('type', UserLessonLike::TYPE_NORMAL)
                        ->delete();
                }
                // insert dislike
                UserLessonLike::insert([
                    [
                        'user_id' => $user->id,
                        'user_lesson_id' => $userLessonId,
                        'type' => UserLessonLike::TYPE_DISLIKE
                    ]
                ]);
                // $status = 1 -> dislike
                $status = 1;
            } else {
                // bo dislike
                UserLessonLike::where('user_id', $user->id)
                    ->where('user_lesson_id', $userLessonId)
                    ->where('type', UserLessonLike::TYPE_DISLIKE)
                    ->delete();
                // $status = 2 -> bo dislike
                $status = 2;
            }
            $countDisLike = UserLessonLike::where('user_lesson_id', $userLessonId)
                ->where('type', UserLessonLike::TYPE_DISLIKE)
                ->get();
            return response()->json(['status' => $status, 'count' => $countDisLike->count()]);
        }
    }

    public function userLessonLog(Request $request, $lessonId)
    {
        if ($request->ajax()) {
            $user = Auth::user();
            $log = new UserLessonLog();
            $log->user_id = $user->id;
            $log->lesson_id = $lessonId;
            $log->status = $request->input('status');
            $log->start = gmdate('H:i:s', $request->input('start'));
            $log->count = $request->input('count');
            $log->date = Carbon::now();
            $log->save();
            return response()->json(['status' => 'success']);
        }
    }
}
