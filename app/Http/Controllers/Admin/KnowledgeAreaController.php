<?php

namespace App\Http\Controllers\Admin;

use App\Models\KnowledgeArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\KnowledgeAreaRepository;
use App\Repository\BusinessAreaRepository;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\KnowledgeRequest;

class KnowledgeAreaController extends Controller
{
    private $businessArea;
    private $knowledgeArea;
    function __construct(BusinessAreaRepository $businessArea, KnowledgeAreaRepository $knowledgeArea)
    {
        $this->businessArea = $businessArea;
        $this->knowledgeArea = $knowledgeArea;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Input::get();
        $knowledgeAreas = $this->knowledgeArea->findFeilds($input);
        return view('admin.knowledge_area.index', compact('knowledgeAreas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $businessAreaList = $this->businessArea->getList();
        return view('admin.knowledge_area.create', compact('businessAreaList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KnowledgeRequest $request)
    {
        $info = $request->all();
        $this->knowledgeArea->insert($info);
        return redirect(route('admin.knowledge.index'))->with('status', 'Thêm lĩnh vực kiến thức thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
        $knowledgeAreaId = $request->knowledge;
        $knowledgeArea = $this->knowledgeArea->findOrFail($knowledgeAreaId);
        $businessAreaList = $this->businessArea->getList();
        return view('admin.knowledge_area.edit', compact('knowledgeArea', 'businessAreaList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KnowledgeRequest $request, $id)
    {
        $knowledgeArea = $this->knowledgeArea->findOrFail($id);
        $this->knowledgeArea->save($request, $knowledgeArea);
        return redirect(route('admin.knowledge.index'))->with('status', 'Sửa thông tin thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $knowledgeAreaId = $request->knowledge;
        $this->knowledgeArea->delete($knowledgeAreaId);
        return redirect(route('admin.knowledge.index'))->with('status', 'Xóa thành công');
    }
}
