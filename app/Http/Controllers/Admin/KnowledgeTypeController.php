<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KnowledgeType;
use App\Repository\KnowledgeTypeRepository;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\KnowledgeTypeRequest;

class KnowledgeTypeController extends Controller
{
    private $knowledgeType;
    function __construct(KnowledgeTypeRepository $knowledgeType)
    {
        $this->knowledgeType = $knowledgeType;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Input::get();
        $knowledgeTypes = $this->knowledgeType->findFeilds($input);
        return view('admin.knowledge_type.index', compact('knowledgeTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.knowledge_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KnowledgeTypeRequest $request)
    {
        $info = $request->all();
        $this->knowledgeType->insert($info);
        return redirect(route('admin.knowledge-type.index'))->with('status', 'Thêm loại kiến thức thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
        $knowledgeTypeId = $request->knowledge_type;
        $knowledgeType = $this->knowledgeType->findOrFail($knowledgeTypeId);
        return view('admin.knowledge_type.edit', compact('knowledgeType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KnowledgeTypeRequest $request, $id)
    {
        $knowledgeType = $this->knowledgeType->findOrFail($id);
        $this->knowledgeType->save($request, $knowledgeType);
        return redirect(route('admin.knowledge-type.index'))->with('status', 'Sửa thông tin thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $knowledgeTypeId = $request->knowledge_type;
        $this->knowledgeType->delete($knowledgeTypeId);
        return redirect(route('admin.knowledge-type.index'))->with('status', 'Xóa thành công');
    }
}
