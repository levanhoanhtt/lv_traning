<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LessonRequest;
use App\Models\Addendum;
use App\Models\KnowledgeArea;
use App\Models\KnowledgeType;
use App\Models\User;
use App\Repository\LessonsRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Support\Facades\View;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $lessons;
    function __construct(LessonsRepository $lessons)
    {
        $this->lessons = $lessons;
    }

    public function index()
    {
        $input = Input::get();
        $knowledgeAreas = KnowledgeArea::all()->pluck('name', 'id');
        $knowledgeTypes = KnowledgeType::all()->pluck('name', 'id');
        $lessonType = Config::get('constant.LESSON_TYPE');
        $level = Config::get('constant.LEVEL');
        $status = Config::get('constant.STATUS');
        $lessons = $this->lessons->findLessons($input, ITEM_PER_PAGE);
        return view('admin.lessons.index',
            compact('lessons',
                            'knowledgeAreas',
                            'knowledgeTypes',
                            'lessonType',
                            'level',
                            'status'
            ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users = User::all()->pluck('name', 'id');
        $knowledgeAreas = KnowledgeArea::all()->pluck('name', 'id');
        $knowledgeTypes = KnowledgeType::all()->pluck('name', 'id');
        $addendum = Addendum::all()->pluck('name', 'id');
        return view('admin.lessons.create', compact('users', 'knowledgeAreas', 'knowledgeTypes', 'addendum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'addendum_id' => 'required',
            'knowledge_area_id' => 'required',
            'knowledge_type' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $html = View::make('errors.error')
                ->withErrors($validator)
                ->render();
            return response()->json(['html' => $html, 'status' => 'error']);
        } else {
            $lesson = $this->lessons->insert($request->all());
            $this->lessons->saveFile($request, $lesson);
            $url = route('admin.lessons.edit', $lesson->id);
            return response()->json(['status' => 'success', 'url' => $url]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $lesson = $this->lessons->findOrFail($id);
        $users = User::all()->pluck('name', 'id');
        $knowledgeAreas = KnowledgeArea::all()->pluck('name', 'id');
        $knowledgeTypes = KnowledgeType::all()->pluck('name', 'id');
        $addendum = Addendum::all()->pluck('name', 'id');
        return view('admin.lessons.edit', compact('lesson','users', 'knowledgeAreas', 'knowledgeTypes', 'addendum'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = array(
            'name' => 'required',
            'addendum_id' => 'required',
            'knowledge_area_id' => 'required',
            'knowledge_type' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $html = View::make('errors.error')
                ->withErrors($validator)
                ->render();
            return response()->json(['html' => $html, 'status' => 'error']);
        } else {
            $info = $request->all();
            $lesson = $this->lessons->findOrFail($id);
            $this->lessons->update($lesson, $info);
            $this->lessons->saveFile($request, $lesson);
            $url = route('admin.lessons.edit', $lesson->id);
            return response()->json(['status' => 'success', 'url' => $url]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
