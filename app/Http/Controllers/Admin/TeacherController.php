<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TeacherRequest;
use App\Models\Role;
use App\Models\User;
use App\Repository\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $teacher;
    function __construct(UserRepository $user)
    {
        $this->teacher = $user;

    }

    public function index()
    {
        //
        $input = Input::get();
        $teachers = $this->teacher->findTeacher($input);
        return view('admin.teacher.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherRequest $request)
    {
        //
        $info = $request->all();
        $info['role_id'] = Role::ROLE_TEACHER;
        $this->teacher->insert($info);
        return redirect(route('admin.teachers.index'))->with('status', 'Thêm giáo viên thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,User $user)
    {
        //
        $teacherId = $request->teacher;
        $teacher = $this->teacher->findOrFail($teacherId);
        return view('admin.teacher.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        $teacher = $this->teacher->findOrFail($request->teacher);
        $this->teacher->save($request, $teacher);
        return redirect(route('admin.teachers.index'))->with('status', 'Sửa thông tin thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $teacherId = $request->teacher;
        $this->teacher->delete($teacherId);
        return redirect(route('admin.teachers.index'))->with('status', 'Xóa thành công');
    }
}
