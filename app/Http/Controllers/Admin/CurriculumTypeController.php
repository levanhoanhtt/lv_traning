<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CurriculumType;
use App\Repository\CurriculumTypeRepository;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\CurriculumTypeRequest;

class CurriculumTypeController extends Controller
{

    private $curriculumType;

    function __construct(CurriculumTypeRepository $curriculumType)
    {
        $this->curriculumType = $curriculumType;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Input::get();
        $curriculumTypes = $this->curriculumType->findFeilds($input);
        return view('admin.curriculum_type.index', compact('curriculumTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.curriculum_type.edit', $this->createOrEdit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CurriculumTypeRequest $request)
    {
        $info = $request->all();
        if ($request->status) {
            $info['status'] = 1;
        } else{
            $info['status'] = 0;
        }
        $this->curriculumType->insert($info);
        return redirect(route('admin.curriculum-type.index'))->with('status', 'Thêm loại kiến thức thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
        $curriculumTypeId = $request->curriculum_type;
        $curriculumType = $this->curriculumType->findOrFail($curriculumTypeId);
        return view('admin.curriculum_type.edit', $this->createOrEdit($curriculumType));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CurriculumTypeRequest $request, $id)
    {
        $curriculumType = $this->curriculumType->findOrFail($id);
        if ($request->status) {
            $info['status'] = 1;
        } else{
            $info['status'] = 0;
        }
        $this->curriculumType->save($request, $curriculumType);
        return redirect(route('admin.curriculum-type.index'))->with('status', 'Sửa thông tin thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $curriculumTypeId = $request->curriculum_type;
        $this->curriculumType->delete($curriculumTypeId);
        return redirect(route('admin.curriculum-type.index'))->with('status', 'Xóa thành công');
    }
    /**
     * Create or edit a curriculum_type.
     *
     * @param  \App\Models\CurriculumType|null $curriculum_type
     * @return array
     */
    protected function createOrEdit(CurriculumType $curriculumType = null)
    {
        $curriculumType = $curriculumType ?: new CurriculumType;
        $url = $curriculumType->id
            ? route('admin.curriculum-type.update', [$curriculumType->id])
            : route('admin.curriculum-type.store');

        return compact('curriculumType', 'url');
    }
}
