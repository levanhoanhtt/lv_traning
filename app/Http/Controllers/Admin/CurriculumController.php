<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CurriculumRequest;
use App\Models\Addendum;
use App\Models\Curriculum;
use App\Models\CurriculumType;
use App\Models\Lesson;
use App\Models\Role;
use App\Models\User;
use App\Repository\CurriculumRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CurriculumController extends Controller
{
    private $curriculum;

    public function __construct(CurriculumRepository $curriculum)
    {
        $this->curriculum = $curriculum;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input       = Input::get();
        $curriculums = $this->curriculum->findFeilds($input);
        return view('admin.curriculum.index', compact('curriculums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $curriculum = new Curriculum;
        $url        = route('admin.curriculums.store');

        $curriculum_types = CurriculumType::all()->pluck('name', 'id');
        $admins           = User::where('role_id', Role::ROLE_ADMIN)->get()->pluck('name', 'id');

        return view('admin.curriculum.create', compact('curriculum', 'url', 'curriculum_types', 'admins'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CurriculumRequest $request)
    {
        $data       = $request->all();
        $curriculum = new Curriculum;

        $curriculum->fill($data);
        if ($request->hasFile('image')) {
            $curriculum->image = $request->file('image')->storePublicly(Curriculum::IMAGE_DIR, Curriculum::UPLOAD_DISK);
        }
        $curriculum->save();

        $addendums = json_decode($data['curriculumContent']);
        if($addendums){
            foreach ($addendums as $key => $addendum) {
                if ($addendum->name) {
                    $new_addendum                = new Addendum;
                    $new_addendum->name          = $addendum->name;
                    $new_addendum->note          = $addendum->note;
                    $new_addendum->curriculum_id = $curriculum->id;
                    $new_addendum->save();
                    if (is_array($addendum->lessons)) {
                        foreach ($addendum->lessons as $key => $lesson) {
                            $update_lesson = Lesson::find($lesson->id);
                            if ($update_lesson) {
                                $update_lesson->addendum_id   = $new_addendum->id;
                                $update_lesson->order_display = $key;
                                $update_lesson->save();
                            }
                        }
                    }
                }
            }
        }


        return redirect()->route('admin.curriculums.index')->with('status', 'Thêm chương trình học thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curriculum = Curriculum::with('addendums.lessons.knowledgeArea')->findOrFail($id);

        $curriculum_types = CurriculumType::all()->pluck('name', 'id');
        $admins           = User::where('role_id', Role::ROLE_ADMIN)->get()->pluck('name', 'id');
        $url              = route('admin.curriculums.update', [$curriculum->id]);

        return view('admin.curriculum.edit', compact('curriculum', 'url', 'curriculum_types', 'admins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $curriculum = Curriculum::find($id);

        $data = $request->all();
        $curriculum->fill($data);
        if ($request->hasFile('image')) {
            $curriculum->image = $request->file('image')->storePublicly(Curriculum::IMAGE_DIR, Curriculum::UPLOAD_DISK);
        }
        $curriculum->save();
        $oldAddendums = $curriculum->addendums;
        foreach ($oldAddendums as $key => $addendum) {
            $addendum->curriculum_id = 0;
            $addendum->save();
        }
        $addendums = json_decode($data['curriculumContent']);

        foreach ($addendums as $key => $addendum) {
            if ($addendum->name) {
                if (isset($addendum->id)) {
                    $updateAddendum = Addendum::find($addendum->id);
                    if ($updateAddendum) {
                        $updateAddendum->name          = $addendum->name;
                        $updateAddendum->note          = $addendum->note;
                        $updateAddendum->curriculum_id = $id;
                        $updateAddendum->save();
                    }
                } else {
                    $updateAddendum                = new Addendum;
                    $updateAddendum->name          = $addendum->name;
                    $updateAddendum->note          = $addendum->note;
                    $updateAddendum->curriculum_id = $curriculum->id;
                    $updateAddendum->save();
                }
                if (is_array($addendum->lessons)) {
                    foreach ($addendum->lessons as $key => $lesson) {
                        $update_lesson = Lesson::find($lesson->id);
                        if ($update_lesson) {
                            $update_lesson->addendum_id   = $updateAddendum->id;
                            $update_lesson->order_display = $key;
                            $update_lesson->save();
                        }
                    }
                }
            }
        }
        return redirect()->route('admin.curriculums.index')->with('status', 'Cập nhật chương trình học thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Create or edit a curriculum.
     *
     * @param  \App\Models\Curriculum|null $curriculum
     * @return array
     */
    protected function createOrEdit(Curriculum $curriculum = null)
    {
        $curriculum = $curriculum ?: new Curriculum;
        $url        = $curriculum->id
        ? route('admin.curriculums.update', [$curriculum->id])
        : route('admin.curriculums.store');

        $curriculum_types = CurriculumType::all()->pluck('name', 'id');
        $admins           = User::where('role_id', Role::ROLE_ADMIN)->get()->pluck('name', 'id');

        return compact('curriculum', 'url', 'curriculum_types', 'admins');
    }

    /**
     * get list Lesson
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function getLessons(Request $request)
    {
        $name    = $request->name;
        $lessons = Lesson::where('addendum_id', 0)->where('name', 'like', '%' . $name . '%')->with('knowledgeArea')->get();
        return $lessons->toJson();
    }
    /**
     * detach  Lesson
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function detachLesson($id)
    {
        $lesson                = Lesson::findOrFail($id);
        $lesson->addendum_id   = 0;
        $lesson->order_display = null;
        $lesson->save();
        return;
    }
}
