<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use App\Repository\LessonsRepository;
use App\Repository\QuestionRepository;
use App\Models\KnowledgeArea;
use App\Models\KnowledgeType;
use App\Models\Question;

class QuestionController extends Controller
{

    private $question;
    private $lessons;

    function __construct(QuestionRepository $question,
                         LessonsRepository $lessons
    )
    {
        $this->question = $question;
        $this->lessons = $lessons;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Input::get();
        $knowledgeAreas = KnowledgeArea::all()->pluck('name', 'id');
        $knowledgeTypes = KnowledgeType::all()->pluck('name', 'id');
        $level = Config::get('constant.LEVEL');
        $questionCate = Config::get('constant.QUESTION_CATE');
        $questionType = Config::get('constant.QUESTION_TYPE');
        $questionApprove = Config::get('constant.QUESTION_APPROVE');
        $questionApply = Config::get('constant.QUESTION_APPLY');

        $questions = $this->question->findQuestions($input, ITEM_PER_PAGE);
        return view('admin.question.index',
            compact('questions',
                'knowledgeAreas',
                'knowledgeTypes',
                'questionCate',
                'questionType',
                'questionApprove',
                'questionApply',
                'level'
            ));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexLession()
    {
        $input = Input::get();
        $knowledgeAreas = KnowledgeArea::all()->pluck('name', 'id');
        $knowledgeTypes = KnowledgeType::all()->pluck('name', 'id');
        $lessonType = Config::get('constant.LESSON_TYPE');
        $level = Config::get('constant.LEVEL');
        $status = Config::get('constant.STATUS');
        $lessons = $this->lessons->findLessons($input, ITEM_PER_PAGE);
        return view('admin.question.lessons',
            compact('lessons',
                'knowledgeAreas',
                'knowledgeTypes',
                'lessonType',
                'level',
                'status'
            ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * get list question
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function getList(Request $request){
        $input = Input::get();
        $questions = $this->question->findQuestions($input, ITEM_PER_PAGE);
        return $questions->toJson();
    }
}
