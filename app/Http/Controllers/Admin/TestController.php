<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\KnowledgeArea;
use App\Models\KnowledgeType;
use App\Models\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\LessonRequest;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessons = Lesson::where('is_test',1)->with('testQuestions')->paginate();
        
        return view('admin.tests.index',compact('lessons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tests.edit', $this->createOrEdit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required'
        ];
        $this->validate($request, $rules);
        $lesson = $this->storeOrUpdate($request);

        return redirect(route('admin.tests.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $lesson = Lesson::findOrFail($id);
        return view('admin.tests.edit', $this->createOrEdit($lesson));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lesson = Lesson::findOrFail($id);
        $this->storeOrUpdate($request, $lesson);

        return redirect()->route('admin.tests.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lesson = Lesson::findOrFail($id);
        $lesson->testQuestions()->sync([]);
        $lesson->delete();

        if (request()->ajax()) {
            return response()->json([
                'message'  => '',
                'redirect' => true,
            ]);
        }

        return redirect()->route('admin.tests.index');
    }

    /**
     * Create or edit a lesson.
     *
     * @param  \App\Models\Lesson|null $lesson
     * @return array
     */
    protected function createOrEdit(Lesson $lesson = null)
    {
        $lesson = $lesson ?: new Lesson;
        $url    = $lesson->id
        ? route('admin.tests.update', [$lesson->id])
        : route('admin.tests.store');

        $knowledgeareas   = KnowledgeArea::all()->pluck('name', 'id');
        $knowledgetypes   = KnowledgeType::all()->pluck('name', 'id');
        $levels           = collect(Config::get('constant.LEVEL'));
        $questioncates    = collect(Config::get('constant.QUESTION_CATE'));
        $questiontypes    = collect(Config::get('constant.QUESTION_TYPE'));
        $questionapproves = collect(Config::get('constant.QUESTION_APPROVE'));
        $questionapplys   = collect(Config::get('constant.QUESTION_APPLY'));

        return compact('lesson', 'url', 'roles', 'knowledgeareas', 'knowledgetypes', 'levels', 'questioncates', 'questiontypes', 'questionapproves', 'questionapplys');
    }

    /**
     * Store or update a lesson.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Lesson|null $lesson
     * @return \App\Models\Lesson
     */
    protected function storeOrUpdate(Request $request, Lesson $lesson = null)
    {
        $lesson = $lesson ?: new Lesson;

        $lesson->fill($request->all());
        $lesson->addendum_id = 0;
        $lesson->knowledge_area_id = 0;
        $lesson->knowledge_type = 0;
        $lesson->create_by = user()->id;
        $lesson->note = '';
        $lesson->is_test = 1;

        $lesson->save();
        if ($questions = $request->input('questionHidden', [])) {
            $questionIds = json_decode($questions);
            $newQuestionsA = [];
            foreach ($questionIds as $key => $id) {
                $item = ['order_display' => $key];
                $newQuestionsA[$id] = $item;
            }
            $lesson->testQuestions()->sync($newQuestionsA);
        }

        return $lesson;
    }
}
