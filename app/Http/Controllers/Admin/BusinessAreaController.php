<?php

namespace App\Http\Controllers\Admin;

use App\Models\BusinessArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\BusinessAreaRepository;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\BusinessAreaRequest;

class BusinessAreaController extends Controller
{

    private $businessArea;
    function __construct(BusinessAreaRepository $businessArea)
    {
        $this->businessArea = $businessArea;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Input::get();
        $businessAreas = $this->businessArea->findFeilds($input);
        return view('admin.business_area.index', compact('businessAreas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.business_area.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BusinessAreaRequest $request)
    {
        $info = $request->all();
        $this->businessArea->insert($info);
        return redirect(route('admin.business.index'))->with('status', 'Thêm lĩnh vực kinh doanh thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
        $businessAreaId = $request->business;
        $businessArea = $this->businessArea->findOrFail($businessAreaId);
        return view('admin.business_area.edit', compact('businessArea'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BusinessAreaRequest $request, $id)
    {
        $businessArea = $this->businessArea->findOrFail($id);
        $this->businessArea->save($request, $businessArea);
        return redirect(route('admin.business.index'))->with('status', 'Sửa thông tin thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $businessAreaId = $request->business;
        $this->businessArea->delete($businessAreaId);
        return redirect(route('admin.business.index'))->with('status', 'Xóa thành công');
    }
}
