<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::with('role');
        $users = $users->when($request->name, function($q) use ($request){
            $q->where('name', 'like', '%' . $request->name . '%');
        });
        $users = $users->when($request->role, function($q) use ($request){
            $q->where('role_id',$request->role);
        });
        if($request->status && $request->status >= 0){
            $users = $users->where('status', $request->status);
        }
        $users = $users->paginate(10);
        $roles = Role::all()->pluck('name','id')->prepend(sprintf('-- %s --', 'Choose role'), 0);
        return view('admin.users.index', compact('users','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.users.edit', $this->createOrEdit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, User::$rules);

        $this->storeOrUpdate($request);

        return redirect()->route('admin.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        return view('admin.users.edit', $this->createOrEdit($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $rules = User::$rules;
        $rules['email'] = [
            'email',
            Rule::unique('users')->ignore($user->id),
        ];
        $rules['phone_number'] = [
            Rule::unique('users')->ignore($user->id),
        ];
        if (!$request->password) {
            $rules['password'] = [];
        }
        $this->validate($request, $rules);

        $this->storeOrUpdate($request, $user);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        $user->delete();

        if (request()->ajax()) {
            return response()->json([
                'message'  => '',
                'redirect' => true,
            ]);
        }

        return redirect()->route('admin.users.index');
    }

    /**
     * Create or edit a user.
     *
     * @param  \App\Models\User|null $user
     * @return array
     */
    protected function createOrEdit(User $user = null)
    {
        $user = $user ?: new User;
        $url  = $user->id
        ? route('admin.users.update', [$user->id])
        : route('admin.users.store');
        $roles = Role::all()->pluck('name','id');

        return compact('user', 'url', 'roles');
    }

    /**
     * Store or update a user.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User|null $user
     * @return \App\Models\User
     */
    protected function storeOrUpdate(Request $request, User $user = null)
    {
        $user = $user ?: new User;
        $data = $request->all();
        
        $user->fill($data);
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }
        if ($request->hasFile('avatar')) {
            $user->avatar = $request->file('avatar')->storePublicly(User::AVATAR_DIR, User::UPLOAD_DISK);
        }

        $user->save();

        return $user;
    }

}
