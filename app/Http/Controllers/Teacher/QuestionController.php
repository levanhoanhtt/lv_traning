<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionRequest;
use App\Models\Answer;
use App\Models\Lesson;
use App\Models\Question;
use App\Models\Role;
use App\Models\User;
use App\Models\WishList;
use App\Repository\AnswerRepository;
use App\Repository\LessonsRepository;
use App\Repository\QuestionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class QuestionController extends Controller
{
    private $question;
    private $answer;
    private $lesson;

    public function __construct(QuestionRepository $question,
        AnswerRepository $answer,
        LessonsRepository $lesson) {
        $this->question = $question;
        $this->answer   = $answer;
        $this->lesson   = $lesson;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lessonId = $request->lessonId;
        $lesson   = $this->lesson->findOrFail($lessonId);
        //$questionEssay = $lesson->questions(Q_ESSAY)->paginate();
        $questions        = $lesson->questions()->with('creater', 'answers')->paginate();
        $questionType     = Config::get('constant.QUESTION_TYPE');
        $questionlevels   = collect(Config::get('constant.LEVEL'));
        $questionapproves = collect(Config::get('constant.QUESTION_APPROVE'));
        $questionapplies  = collect(Config::get('constant.QUESTION_APPLY'));

        $wishList = WishList::where('user_id', user()->id)->get()->pluck('lesson_id')->toArray();
        $admins = User::where('role_id', Role::ROLE_ADMIN)->get()->pluck('name', 'id');

        $lessonLists = Lesson::with('knowledgeArea','questionsEssay','questionsChoice', 'questionsEssayUser','questionsChoiceUser')->where('is_test', 0)
            ->when($request->lesson_name, function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->lesson_name . '%');
            })
            ->paginate();
        $srcDelete = asset('teacher/img/icon24.png');
        $srcAdd = asset('teacher/img/icon21.png');

        return view('teacher.index', compact('questionType', 'lesson', 'lessonId', 'questions', 'admins', 'questionlevels', 'questionapproves', 'questionapplies', 'wishList', 'lessonLists', 'srcDelete', 'srcAdd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function createEssayQuestions(QuestionRequest $request)
    {
        $lessonId = $request->lessonId;
        $userId   = Auth::id();
        // create question
        $dataQuestion = [
            'lesson_id'     => $lessonId, // đang lấy mặc định Bài học có id 1, bổ sung khi có màn hình list bài học và giáo viên
            'code'          => 'TL' . get_unique_code(),
            'type'          => $request->get('type'),
            'preview_flg'   => $request->has('preview_flg') ? $request->get('preview_flg') : Q_NO_PREVIEW,
            'content'       => $request->get('content'),
            'image'         => $request->get('image'),
            'level'         => $request->get('level'),
            'question_type' => $request->get('question_type'),
            'created_by'    => $userId,
        ];
        $question = $this->question->insert($dataQuestion);
        $this->question->saveFile($request, $question);

        // insert answer
        $dataAnswer = [
            'question_id' => $question->id,
            'content'     => $request->get('answer'),
            'correct_flg' => A_IS_CORRECT,
            'created_by'  => $userId,
        ];
        $answer = $this->answer->insert($dataAnswer);

        $request->flush();
        return redirect(route('teacher.index', $lessonId));
    }

    public function createChoiceQuestions(QuestionRequest $request)
    {
        $lessonId = $request->lessonId;
        $userId   = Auth::id();
        // create question
        $dataQuestion = [
            'lesson_id'     => $lessonId, // đang lấy mặc định Bài học có id 1, bổ sung khi có màn hình list bài học và giáo viên
            'code'          => 'TN' . get_unique_code(),
            'type'          => $request->get('type'),
            'preview_flg'   => $request->has('preview_flg') ? $request->get('preview_flg') : Q_NO_PREVIEW,
            'number_of_ans' => $request->get('number_of_ans'),
            'answer_type'   => $request->get('answer_type'),
            'content'       => $request->get('content_choice'),
            'level'         => $request->get('level'),
            'question_type' => $request->get('question_type'),
            'created_by'    => $userId,
        ];
        $question = $this->question->insert($dataQuestion);

        // insert answer
        foreach ($request->get('answer') as $k => $answer) {
            $dataAnswer = [
                'question_id' => $question->id,
                'content'     => $answer,
                'correct_flg' => $request->get('correct_flg')[$k],
                'created_by'  => $userId,
            ];

            $answer = $this->answer->insert($dataAnswer);
            if (!empty($request->image_ans[$k])) {
                $this->answer->saveFile($request->image_ans[$k], $answer);
            }
        }

        $request->flush();
        return redirect(route('teacher.index', $lessonId));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * get list question
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $lessonId
     * @return \Illuminate\Http\Response
     */

    public function listQuestions(Request $request, $lessonId)
    {

        $lesson    = $this->lesson->findOrFail($lessonId);
        $questions = Question::where('lesson_id', $lessonId);
        if ($request->admin > 0) {
            $questions = $questions->where('created_by', $request->admin);
        }
        if ($request->questionLevel > -1) {
            $questions = $questions->where('level', $request->questionLevel);
        }
        if ($request->questionapprove > -1) {
            $questions = $questions->where('approve_flg', $request->questionapprove);
        }
        if ($request->questionapply > -1) {
            $questions = $questions->where('apply_flg', $request->questionapply);
        }
        $questions = $questions->with('creater', 'answers')->paginate();

        return $questions->toJson();
    }

    /**
     * get list lesson
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $lessonId
     * @return \Illuminate\Http\Response
     */

    public function listLessons(Request $request, $lessonId)
    {

        $lessons = Lesson::with('knowledgeArea')
            ->when($request->name, function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->name . '%');
            })
            ->paginate();

        return $lessons->toJson();
    }

    public function addWishList(Request $request){
        if (!isset($request->lessonAddId)) {
            abort(500);
        }
        $lessonAddId = $request->lessonAddId;
        $wishList = WishList::where('user_id', user()->id)->where('lesson_id',$lessonAddId)->first();

        if($wishList){
            $wishList->delete();
            return [
                'success' => true,
                'action' => 'delete'
            ];
        } else{
            $newWishList = new WishList();
            $newWishList->user_id = user()->id;
            $newWishList->lesson_id = $lessonAddId;
            $newWishList->save();
            return [
                'success' => true,
                'action' => 'add'
            ];
        }
    }
}
