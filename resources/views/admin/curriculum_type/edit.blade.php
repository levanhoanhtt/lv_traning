@extends('adminlte::page')

@section('title', $curriculumType->id ? 'Sửa thông tin loại chương trình học' : 'Thêm mới loại chương trình học')

@section('content_header')
    <h1>Loại chương trình học</h1>

@stop

@section('content')

    <div class="box">
        <div class="box-title">
            @if ($curriculumType->id)
                <h4>Sửa thông tin loại chương trình học {{ $curriculumType->name }}</h4>
            @else
                <h4>Thêm mới loại chương trình học {{ $curriculumType->name }}</h4>
            @endif
        </div>
        <div class="box-body">
            @include('partials.errors')
            {!! Form::model($curriculumType, [
                    'url' => $url,
                    'role' => 'form',
                    'method' => $curriculumType->id ? 'PUT' : 'POST',
                    'files' => true,
                ])
            !!}
            <div class="form-group col-md-6">
                {!!  Form::label('name', 'Tên: ') !!}
                {!!  Form::text('name', null, ['class' => 'form-control']) !!}
                <br>
                {!!  Form::checkbox('status') !!}
                {!!  Form::label('status', 'Kích hoạt') !!}
            </div>
            <div class="form-group col-md-12">
                {!! Form::submit('Lưu', ['class'=>'btn btn-success clear-both']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('errors.error')
@stop