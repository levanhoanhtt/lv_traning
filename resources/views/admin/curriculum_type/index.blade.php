@extends('adminlte::page')

@section('title', 'Quản lý loại chương trình học')

@section('css')
    {!! Html::style('css/style.css') !!}
@stop

@section('content_header')
    <h1>Quản lý loại chương trình học</h1>

    <ul class="list-inline">
        <a href="{{ route('admin.curriculum-type.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
    </ul>

    @if(session('status'))
        <div class="alert alert-success">{{session('status')}}</div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-tools">
                        {!! Form::open(['route' => 'admin.curriculum-type.index', 'method'=>'GET']) !!}
                        <div class="input-group input-group-sm" style="width: 150px;">

                            <input type="text" name="name" class="form-control pull-right" placeholder="Tìm kiếm">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>

                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="box-body">
                    @if($curriculumTypes->count() > 0)
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Trạng thái</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($curriculumTypes as $curriculumType)
                            <tr>
                                <td>{{$curriculumType->name}}</td>
                                <td>{!! $curriculumType->present()->status !!}</td>
                                <td style="min-width: 100px">
                                    <a data-toggle="tooltip" class="btn btn-sm btn-primary" title="Edit" href="{{ route('admin.curriculum-type.edit', $curriculumType->id) }}"><i class="fa fa-pencil"></i></a>
                                    <a href="{{ route('admin.curriculum-type.destroy', $curriculumType->id) }}"
                                        class="btn btn-sm btn-danger" title="Delete"
                                        data-swal="{{ json_encode(swal([
                                            'title' => 'Are you sure',
                                            'type' => 'question',
                                            'showCancelButton' => true,
                                        ])) }}"
                                        data-swal-method="DELETE"><i class="fa fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $curriculumTypes->appends(request()->query())->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop