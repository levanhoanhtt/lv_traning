@extends('layouts.admin')

@section('title', $curriculum->id ? 'Sửa thông tin chương trình học' : 'Thêm mới chương trình học')

@section('content_header')
<h1>Chương trình học</h1>

@stop

@section('content')

<div class="box" id="admin">
        <div class="box-title">
            @if ($curriculum->id)
                <h4>Sửa thông tin chương trình học {{ $curriculum->name }}</h4>
            @else
                <h4>Thêm mới chương trình học {{ $curriculum->name }}</h4>
            @endif
        </div>
        @include('partials.errors')
        <div class="box-body" id="curriculum">
            {!! Form::model($curriculum, [
                    'url' => $url,
                    'role' => 'form',
                    'method' => $curriculum->id ? 'PUT' : 'POST',
                    'files' => true,
                ])
            !!}
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group col-md-6">
                        {!!  Form::label('name', 'Tên chương trình: ') !!}
                        {!!  Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::label('code', 'Mã chương trình: ') !!}
                        {!!  Form::text('code', null, ['class' => 'form-control']) !!}
                    </div>
                    <curriculum-content></curriculum-content>
                </div>
                <div class="col-md-4">
                    <div class="form-group col-md-12">
                        {!! Form::label('admin_id', 'Người tạo:') !!}
                        {!! Form::select('admin_id', $admins, null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!!  Form::label('image', 'Ảnh bìa: ') !!}
                        <img src="{{ $curriculum->present()->image('blog') }}" class="img-responsive" />

                        {!! Form::file('image', [
                            'class' => 'form-control',
                        ]) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!! Form::label('curriculum_type_id', 'Loại chương trình học:') !!}
                        {!! Form::select('curriculum_type_id', $curriculum_types, null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!!  Form::label('code', 'Cấp độ bài học: ') !!}
                        <div class="level-lesson">
                            <i class="fa fa-star  active" style="font-size:25px" aria-hidden="true"></i>
                            <i class="fa fa-star  active" style="font-size:25px" aria-hidden="true"></i>
                            <i class="fa fa-star  active" style="font-size:25px" aria-hidden="true"></i>
                        </div>
                        {{ Form::hidden('level', 3) }}
                    </div>
                    <div class="form-group col-md-12">
                        {!!  Form::label('', 'Trạng thái : ') !!}
                        {!!  Form::radio('status', '0', true) !!}
                        {!!  Form::label('status', 'Không Hoạt dộng') !!}

                        {!!  Form::radio('status', '1') !!}
                        {!!  Form::label('status', 'Hoạt động') !!}

                    </div>
                    <div class="col-md-12 form-group">
                        {!!  Form::label('', 'Giá : ') !!}
                        {!! Form::number('price', old('price'), [
                            'class' => 'form-control'
                        ]) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!!  Form::label('', 'Lưu hành : ') !!}
                        {!!  Form::radio('published', '0', true) !!}
                        {!!  Form::label('published', 'Nội bộ') !!}

                        {!!  Form::radio('published', '1') !!}
                        {!!  Form::label('published', 'Thương mại') !!}

                    </div>
                </div>
                <div class="form-group col-md-12">
                    {!! Form::submit('Lưu', ['class'=>'btn btn-success clear-both']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
</div>
    
@stop