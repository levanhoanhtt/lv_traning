@extends('layouts.admin')

@section('title', 'Quản lý chương trình học')
@section('css')
    {!! Html::style('css/style.css') !!}
@stop
@section('content_header')
    <h1>Quản lý chương trình học</h1>
    <ul class="list-inline">
        <a href="{{ route('admin.curriculums.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
    </ul>
    @if(session('status'))
        <div class="alert alert-success">{{session('status')}}</div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Chương trình học</h3>
                    <div class="box-tools">
                        {!! Form::open(['route' => 'admin.curriculums.index', 'method'=>'GET']) !!}
                        <div class="input-group input-group-sm" style="width: 150px;">

                            <input type="text" name="name" class="form-control pull-right" placeholder="Tìm kiếm">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>

                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($curriculums as $curriculum)
                            <tr id="brand-{{$curriculum->id}}">
                                <td class="brand-id">{{$curriculum->id}}</td>
                                <td>{!! $curriculum->present()->status !!}</td>
                                <td>
                                    <img src="{{ $curriculum->present()->image('square64') }}">
                                </td>
                                <td>{{ $curriculum->name }}</td>
                                <td style="min-width: 100px">
                                    <a data-toggle="tooltip" class="btn btn-sm btn-primary" title="Edit" href="{{ route('admin.curriculums.edit', $curriculum->id) }}"><i class="fa fa-pencil"></i></a>
                                    {{-- <a href="{{ route('admin.curriculums.destroy', $curriculum->id) }}"
                                        class="btn btn-sm btn-danger" title="Delete"
                                        data-swal="{{ json_encode(swal([
                                            'title' => 'Are you sure',
                                            'type' => 'question',
                                            'showCancelButton' => true,
                                        ])) }}"
                                        data-swal-method="DELETE"><i class="fa fa-times"></i>
                                    </a> --}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-right">
                    {{ $curriculums->appends(request()->query())->links() }}
                </div>
                </div>
            </div>
        </div>
    </div>

@stop