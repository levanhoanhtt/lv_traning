@extends('layouts.admin')

@section('title', 'Quản lý người dùng')
@section('css')
    @parent
    {!! Html::style('css/style.css') !!}
@stop
@section('content_header')
    <h1>Quản lý người dùng</h1>
    <ul class="list-inline">
        <a href="{{ route('admin.users.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
    </ul>
    @if(session('status'))
        <div class="alert alert-success">{{session('status')}}</div>
    @endif
@stop

@section('content')
    <div class="row" id="admin">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Người dùng</h3>
                    <div class="box-tools">
                        {!! Form::open(['route' => 'admin.users.index', 'method'=>'GET','class' => 'form-inline']) !!}
                        <div >
                            <input type="text" name="name" class="form-control" placeholder="Tên" value="{{ request('name') }}">
                            {!!  Form::select('role', $roles, request('role') , ['class' => 'form-control']) !!}
                            {!! Form::select(
                                'status', 
                                ['-1' => '-- Status --', '0' => 'Inactive', '1' => 'Active' ], 
                                request('status'),
                                ['class' => 'form-control']
                           ) !!}
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Status</th>
                            <th>Avatar</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr id="brand-{{$user->id}}">
                                <td class="brand-id">{{$user->id}}</td>
                                <td>{!! $user->present()->status !!}</td>
                                <td>
                                    <img src="{{ $user->present()->avatar('square64') }}">
                                </td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->phone_number }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role->name }}</td>
                                <td style="min-width: 100px">
                                    <a data-toggle="tooltip" class="btn btn-sm btn-primary" title="Edit" href="{{ route('admin.users.edit', $user->id) }}"><i class="fa fa-pencil"></i></a>
                                    <a href="{{ route('admin.users.destroy', $user->id) }}"
                                        class="btn btn-sm btn-danger" title="Delete"
                                        data-swal="{{ json_encode(swal([
                                            'title' => 'Are you sure',
                                            'type' => 'question',
                                            'showCancelButton' => true,
                                        ])) }}"
                                        data-swal-method="DELETE"><i class="fa fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-right">
                    {{ $users->appends(request()->query())->links() }}
                </div>
                </div>
            </div>
        </div>
    </div>

@stop