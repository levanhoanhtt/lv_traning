@extends('layouts.admin')

@section('title', $user->id ? 'Sửa thông tin người dùng' : 'Thêm mới người dùng')

@section('content_header')
<h1>{{ $user->id ? 'Sửa thông tin' : 'Thêm mới' }} người dùng</h1>

@stop

@section('content')

<div class="box">
        {{-- <div class="box-title">
            <h4>Sửa thông tin học sinh {{$student->name}}</h4>
        </div> --}}
        @include('partials.errors')
        <div class="box-body">
            {!! Form::model($user, [
                'url' => $url,
                'role' => 'form',
                'method' => $user->id ? 'PUT' : 'POST',
                'files' => true,
            ])
            !!}
            <div class="row">
                <div class="col-md-3 text-center">

                        <img src="{{ $user->present()->avatar('square256') }}" class="img-responsive" />

                        {!! Form::file('avatar', [
                            'class' => 'form-control',
                        ]) !!}
                        <br>
                        <div class="form-group m-t-lg">
                            {!! Form::submit('Lưu', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                <div class="col-md-9">
                    <div class="form-group col-md-6">
                        {!!  Form::label('name', 'Họ tên: ') !!}
                        {!!  Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::label('password', 'Mật khẩu: ') !!}
                        {!!  Form::password('password', ['class' => 'form-control', 'placeholder' => 'Để trống nếu không thay đổi']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::label('phone_number', 'SDT: ') !!}
                        {!!  Form::text('phone_number', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::label('gender', 'Giới tính: ') !!}
                        {!!  Form::select('gender', ['male' => 'Nam', 'female' => 'Nữ'], 'male', ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::label('birthday', 'Ngày sinh: ') !!}
                        {!!  Form::date('birthday', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::label('email', 'Email: ') !!}
                        {!!  Form::text('email', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::label('address', 'Địa chỉ: ') !!}
                        {!!  Form::text('address', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::label('department', 'Bộ phận làm việc: ') !!}
                        {!!  Form::text('department', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::label('company', 'Công ty: ') !!}
                        {!!  Form::text('company', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::label('role', 'Loại người dùng') !!}
                        {!!  Form::select('role_id', $roles, \App\Models\Role::ROLE_STUDENT , ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!!  Form::checkbox('status') !!}
                        {!!  Form::label('status', 'Kích hoạt') !!}
                    </div>
                </div>
            </div>

            {{-- {!! Form::submit('Lưu', ['class'=>'btn btn-success clear-both']) !!} --}}
            {!! Form::close() !!}
        </div>
    </div>
    
    @stop