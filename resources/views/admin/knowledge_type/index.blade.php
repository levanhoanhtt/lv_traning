@extends('adminlte::page')

@section('title', 'Quản lý loại kiến thức')

@section('css')
    {!! Html::style('css/style.css') !!}
@stop

@section('content_header')
    <h1>Quản lý loại kiến thức</h1>

    <ul class="list-inline">
        <a href="{{url(route('admin.knowledge-type.create'))}}" class="btn btn-primary">Thêm loại kiến thức</a>
    </ul>

    @if(session('status'))
        <div class="alert alert-success">{{session('status')}}</div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-tools">
                        {!! Form::open(['route' => 'admin.knowledge-type.index', 'method'=>'GET']) !!}
                        <div class="input-group input-group-sm" style="width: 150px;">

                            <input type="text" name="name" class="form-control pull-right" placeholder="Tìm kiếm">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>

                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="box-body">
                    @if($knowledgeTypes->count() > 0)
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Mô tả</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($knowledgeTypes as $kt)
                            <tr id="brand-{{$kt->id}}">
                                <td><a href="{{url(route('admin.knowledge-type.edit', $kt->id))}}">{{$kt->name}}</a></td>
                                <td>{{ $kt->description }}</td>
                                <td>
                                    {!! Form::open(['route' => ['admin.knowledge-type.destroy', $kt->id], 'method' => 'delete']) !!}
                                    {!! Form::submit('Xóa', ['class'=>'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $knowledgeTypes->appends(Request::only('name'))->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop