@extends('adminlte::page')

@section('title', 'Thêm loại kiến thức')

@section('content_header')
    <h1>Thêm loại kiến thức</h1>

@stop

@section('content')

    <div class="box">
        <div class="box-title">
            <h4>Thông tin loại kiến thức</h4>
        </div>
        <div class="box-body">
            {!! Form::open(['route' => 'admin.knowledge-type.store']) !!}
            <div class="form-group col-md-6">
                {!!  Form::label('name', 'Tên: ') !!}
                {!!  Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('description', 'Mô tả: ') !!}
                {!!  Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
            </div>
            <div class="form-group col-md-12">
            {!! Form::submit('Thêm', ['class'=>'btn btn-success clear-both']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('errors.error')
@stop