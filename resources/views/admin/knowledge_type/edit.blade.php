@extends('adminlte::page')

@section('title', 'Sửa loại kiến thức')

@section('content_header')
    <h1>Sửa loại kiến thức</h1>

@stop

@section('content')

    <div class="box">
        <div class="box-title">
            <h4>Sửa thông tin loại kiến thức {{ $knowledgeType->name }}</h4>
        </div>
        <div class="box-body">
            {!! Form::model($knowledgeType, [ 'method'=>'PATCH','action' => ['Admin\KnowledgeTypeController@update', $knowledgeType->id]]) !!}
            <div class="form-group col-md-6">
                {!!  Form::label('name', 'Tên: ') !!}
                {!!  Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('description', 'Mô tả: ') !!}
                {!!  Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
            </div>
            <div class="form-group col-md-12">
                {!! Form::submit('Sửa', ['class'=>'btn btn-success clear-both']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('errors.error')
@stop