@extends('adminlte::page')

@section('title', 'Quản lý lĩnh vực kinh doanh')

@section('css')
    {!! Html::style('css/style.css') !!}
@stop

@section('content_header')
    <h1>Quản lý lĩnh vực kinh doanh</h1>
    <ul class="list-inline">
        <a href="{{url(route('admin.business.create'))}}" class="btn btn-primary">Thêm lĩnh vực</a>
    </ul>
    @if(session('status'))
        <div class="alert alert-success">{{session('status')}}</div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-tools">
                        {!! Form::open(['route' => 'admin.business.index', 'method'=>'GET']) !!}
                        <div class="input-group input-group-sm" style="width: 150px;">

                            <input type="text" name="name" class="form-control pull-right" placeholder="Tìm kiếm">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>

                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="box-body">
                    @if($businessAreas->count() > 0)
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Mã</th>
                            <th>Mô tả</th>
                            <th>Trạng thái</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($businessAreas as $bs)
                            <tr id="brand-{{$bs->id}}">
                                <td><a href="{{url(route('admin.business.edit', $bs->id))}}">{{$bs->name}}</a></td>
                                <td>{{ $bs->code }}</td>
                                <td>{{ $bs->description }}</td>
                                <td>{!! $bs->present()->status !!}</td>
                                <td>
                                    {!! Form::open(['route' => ['admin.business.destroy', $bs->id], 'method' => 'delete']) !!}
                                    {!! Form::submit('Xóa', ['class'=>'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $businessAreas->appends(Request::only('name'))->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop