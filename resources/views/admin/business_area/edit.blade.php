@extends('adminlte::page')

@section('title', 'Sửa lĩnh vực kinh doanh')

@section('content_header')
    <h1>Sửa lĩnh vực kinh doanh</h1>

@stop

@section('content')

    <div class="box">
        <div class="box-title">
            <h4>Sửa thông tin lĩnh vực kinh doanh {{ $businessArea->name }}</h4>
        </div>
        <div class="box-body">
            {!! Form::model($businessArea, [ 'method'=>'PATCH','action' => ['Admin\BusinessAreaController@update', $businessArea->id]]) !!}
            <div class="form-group col-md-6">
                {!!  Form::label('name', 'Tên: ') !!}
                {!!  Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('code', 'Mã: ') !!}
                {!!  Form::text('code', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('description', 'Mô tả: ') !!}
                {!!  Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('status', 'Trạng thái ') !!}
                {!!  Form::select('status', \App\Models\BusinessArea::BUSINESSAREA_STATUS, null, ['class' =>
                'form-control']) !!}
            </div>
            <div class="form-group col-md-12">
                {!! Form::submit('Sửa', ['class'=>'btn btn-success clear-both']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('errors.error')
@stop