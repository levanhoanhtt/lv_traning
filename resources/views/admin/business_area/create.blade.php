@extends('adminlte::page')

@section('title', 'Thêm lĩnh vực kinh doanh')

@section('content_header')
    <h1>Thêm lĩnh vực kinh doanh</h1>

@stop

@section('content')

    <div class="box">
        <div class="box-title">
            <h4>Thông tin lĩnh vực kinh doanh</h4>
        </div>
        <div class="box-body">
            {!! Form::open(['route' => 'admin.business.store']) !!}
            <div class="form-group col-md-6">
                {!!  Form::label('name', 'Tên: ') !!}
                {!!  Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('code', 'Mã: ') !!}
                {!!  Form::text('code', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('description', 'Mô tả: ') !!}
                {!!  Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('status', 'Trạng thái ') !!}
                {!!  Form::select('status', \App\Models\BusinessArea::BUSINESSAREA_STATUS, '', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-12">
            {!! Form::submit('Thêm', ['class'=>'btn btn-success clear-both']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('errors.error')
@stop