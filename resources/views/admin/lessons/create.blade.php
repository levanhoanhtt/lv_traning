@extends('adminlte::page')



@section('title', 'Thêm bài học mới')
@section('css')
    <style>
        .ck-editor__editable {
            min-height: 300px;
        }
    </style>
@stop
@section('content_header')
    <h1>Tạo bài học mới</h1>
    <div id="validate"></div>
@stop


@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="box">
                {!! Form::open(['route' => 'admin.lessons.store', 'method' => 'POST', 'files' => true]) !!}
                <div class="box-title">
                    <h3>Thêm bài học</h3>
                </div>
                <div class="box-body">

                            <div class="form-group">
                                {!!  Form::label('name', 'Tiêu đề: ') !!}
                                {!!  Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!!  Form::label('', 'Loại bài học : ') !!}
                                {!!  Form::radio('type', 'audio', true) !!}
                                {!!  Form::label('type', 'Audio') !!}

                                {!!  Form::radio('type', 'blog') !!}
                                {!!  Form::label('type', 'Blog') !!}

                                {!!  Form::radio('type', 'video') !!}
                                {!!  Form::label('type', 'Video') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::file('file') !!}
                            </div>
                            <div class="form-group progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                                <div id="status"></div>
                            </div>
                            <div class="form-group" id="content-group">
                                {!!  Form::label('content', 'Nội dung: ') !!}
                                {!!  Form::textarea('content', null, ['class' => 'form-control', 'id' =>'content']) !!}
                            </div>
                            <div class="form-group">
                                {!!  Form::label('note', 'Ghi chú: ') !!}
                                {!!  Form::textarea('note', null, ['class' => 'form-control', 'id' =>'note']) !!}
                            </div>
                        </div>


            </div>
        </div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-body">
                    <div class="form-group">
                        {!!  Form::label('', 'Trạng thái : ') !!}
                        {!!  Form::radio('status', '0', true) !!}
                        {!!  Form::label('status', 'Không Hoạt dộng') !!}

                        {!!  Form::radio('status', '1') !!}
                        {!!  Form::label('status', 'Hoạt động') !!}

                    </div>
                    <div class="form-group">
                        {!! Form::label('create_by', 'Người soạn') !!}
                        {!! Form::select('create_by', $users, null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('knowledge_area_id', 'Lĩnh vực kiến thức: ') !!}
                        {!! Form::select('knowledge_area_id', $knowledgeAreas, null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('knowledge_type', 'Loại kiến thức:') !!}
                        {!! Form::select('knowledge_type', $knowledgeTypes, null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('addendum_id', 'Phụ lục') !!}
                        {!! Form::select('addendum_id', $addendum, null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('thumbnail', 'Ảnh bìa: ') !!}
                        {!! Form::file('thumbnail') !!}
                    </div>
                    <img id="thumbnail_picture" width="300px" height="150px" src="" alt="">
                    {{Form::submit('Lưu',  ['class'=>'btn btn-success', 'id' => 'saveButton'])}}
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>


@stop

@section('js')
    <script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
    <script src="{{asset('js/bootstrap-tagsinput.js')}}"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    <script>
        $(document).ready(function() {
            $('#content-group').hide();
            $('input[type=radio][name=type]').change(function() {

                if (this.value == 'blog') {
                    CKEDITOR.replace( 'content' );
                    $('#content-group').show();
                    $('input[type=file][name=file]').hide();
                    $('div.progress').hide();
                }
                else {
                    if(CKEDITOR.instances.content){
                        CKEDITOR.instances.content.destroy();
                    }

                    $('#content-group').hide();
                    $('input[type=file][name=file]').show();
                    $('div.progress').show();
                }
            });
            $('select').select2();
            $('input[type=file][name=thumbnail]').on('change', function (evt) {
                var file = evt.target.files[0];
               readFile(file);
            });
            // upload file progress bar
            var bar = $('.progress-bar');
            $("#saveButton").click(function() {
                var file = $('input[name="file"]').val();
                $('form').ajaxForm({
                    beforeSend: function() {
                        var percentVal = '0%';
                        bar.width(percentVal);
                        bar.html(percentVal);
                    },
                    uploadProgress: function(event, position, total, percentComplete) {
                        var percentVal = percentComplete + '%';
                        console.log(file);
                        if (file != '') {
                            bar.width(percentVal);
                            bar.html(percentVal);
                        }
                    },
                    complete: function(xhr, data) {
                        var data = jQuery.parseJSON(xhr.responseText);
                        if (data.status == 'success') {
                            $('div#validate').html('');
                            window.location.href = data.url;
                        } else if(data.status == 'error') {
                            $('div#validate').html(data.html);
                        }
                    }
                });
            });

            function readFile(file) {
                var preview = $('#thumbnail_picture');
                var reader = new FileReader();
                reader.onload = function (e) {
                    preview.attr('src', e.target.result);
                }
                if(file){
                    reader.readAsDataURL(file);
                }else{
                    preview.src = '';
                }
            }

        });

    </script>
    <script>CKEDITOR.replace( 'note', {height: 100} );</script>
@stop
