@extends('adminlte::page')

@section('title', 'Quản lý bài học')

@section('css')
    {!! Html::style('css/style.css') !!}
@stop

@section('content_header')
    <h1>Danh sách bài học</h1>
    <ul class="list-inline">
        <a href="{{url(route('admin.lessons.create'))}}" class="btn btn-primary">Thêm bài học</a>
    </ul>
    @if(session('status'))
        <div class="alert alert-success">{{session('status')}}</div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    {!! Form::open(['route' => 'admin.lessons.index', 'method'=>'GET']) !!}
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="input-group-sm">
                                {!! Form::select('knowledge_area_id', $knowledgeAreas, null, ['class' => 'form-control',
                                'placeholder' => '--Lĩnh vực kiến thức--'
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group-sm">
                                {!! Form::select('knowledge_type', $knowledgeTypes, null, ['class' => 'form-control',
                                'placeholder' => '--Loại kiến thức--'
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group-sm">
                                {!! Form::select('level', $level, null, ['class' => 'form-control',
                                'placeholder' => '--Cấp độ--']) !!}
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group-sm">
                                {!! Form::select('type', $lessonType, null, ['class' => 'form-control',
                                'placeholder' => '--Định dạng--']) !!}
                            </div>
                        </div>
                        <div class="col-sm-2 pull-right">
                            <div class="input-group-sm">
                                {!! Form::select('status', $status, null, ['class' => 'form-control',
                                'placeholder' => '--Trạng thái duyệt--']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 15px;">
                        <div class="col-sm-12">
                            <div class="input-group input-group-sm">

                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Tìm kiếm']) !!}
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="box-body">
                    @if($lessons->count() > 0)
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Mã bài học</th>
                                <th>Tên bài học</th>
                                <th>Định dạng</th>
                                <th>Loại kiến thức</th>
                                <th>Lĩnh vực kiến thức</th>
                                <th>Cấp độ</th>
                                <th>Trạng thái duyệt</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lessons as $lesson)
                                <tr id="brand-{{$lesson->id}}">
                                    <td><input type="checkbox" class="iCheckTable iCheckItem"
                                               value="{{ $lesson->id }}">
                                    </td>
                                    <td>{{ $lesson->code }}</td>
                                    <td>
                                        <a href="{{url(route('admin.lessons.edit', $lesson->id)) }}">{{$lesson->name}}</a>
                                    </td>
                                    <td>{{ $lesson->type }}</td>
                                    <td>{{ $lesson->knowledgeType->name }}</td>
                                    <td>{{ $lesson->knowledgeArea->name }}</td>
                                    <td>{!! $lesson->present()->level !!}</td>
                                    <td>{!! $lesson->present()->status !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pull-right">
                            {{ $lessons->appends(Request::all())->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop