@extends('adminlte::page')

@section('title', 'Quản lý câu hỏi')

@section('css')
    {!! Html::style('css/style.css') !!}
@stop

@section('content_header')
    <h1>Danh sách câu hỏi</h1>
    <ul class="list-inline">
        <div class="btn-group ">
            <select class="btn btn-primary" onchange="this.options[this.selectedIndex].value && (window.location = this
            .options[this.selectedIndex].value);">
                <option value="{{url(route('admin.questions.index'))}}">Lọc theo câu hỏi</option>
                <option value="{{url(route('admin.questions.lessons'))}}">Lọc theo bài học</option>
            </select>
        </div>
    </ul>
    @if(session('status'))
        <div class="alert alert-success">{{session('status')}}</div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    {!! Form::open(['route' => 'admin.questions.index', 'method'=>'GET']) !!}
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="input-group-sm">
                                {!! Form::select('k_area', $knowledgeAreas, null, ['class' => 'form-control',
                                'placeholder' => '--Lĩnh vực kiến thức--'
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-sm-2" style="margin-left: -25px;">
                            <div class="input-group-sm">
                                {!! Form::select('k_type', $knowledgeTypes, null, ['class' => 'form-control',
                                'placeholder' => '--Loại kiến thức--'
                                ]) !!}
                            </div>
                        </div>

                        <div class="col-sm-2" style="margin-left: -25px;">
                            <div class="input-group-sm">
                                {!! Form::select('type', $questionCate, null, ['class' => 'form-control',
                                'placeholder' => '--Thể loại--']) !!}
                            </div>
                        </div>
                        <div class="col-sm-2" style="margin-left: -25px;">
                            <div class="input-group-sm" style="width: 65%;">
                                {!! Form::select('level', $level, null, ['class' => 'form-control',
                                'placeholder' => '--Cấp độ--']) !!}
                            </div>
                        </div>
                        <div class="col-sm-2" style="margin-left: -77px;">
                            <div class="input-group-sm" style="width: 65%;">
                                {!! Form::select('q_type', $questionType, null, ['class' => 'form-control',
                                'placeholder' => '--Kĩ năng--']) !!}
                            </div>
                        </div>
                        <div class="col-sm-2 pull-right">
                            <div class="input-group-sm">
                                {!! Form::select('apply', $questionApply, null, ['class' => 'form-control',
                                'placeholder' => '--Trạng thái áp dụng--']) !!}
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="input-group-sm">
                                {!! Form::select('approve', $questionApprove, null, ['class' => 'form-control',
                                'placeholder' => '--Trạng thái duyệt--']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 15px;">
                        <div class="col-sm-12">
                            <div class="input-group input-group-sm">

                                {!! Form::text('content', null, ['class' => 'form-control', 'placeholder' => 'Tìm kiếm']) !!}
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="box-body">
                    @if($questions->count() > 0)
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Mã câu hỏi</th>
                                <th>Nội dung</th>
                                <th>Thể loại</th>
                                <th>Độ khó</th>
                                <th>Kĩ năng</th>
                                <th>Thuộc bài học</th>
                                <th>Trạng thái duyệt</th>
                                <th>Trạng thái áp dụng</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($questions as $question)
                                <tr id="brand-{{$question->id}}">
                                    <td><input type="checkbox" class="iCheckTable iCheckItem"
                                               value="{{ $question->id }}">
                                    </td>
                                    <td><a href="{{url(route('admin.lessons.edit', $question->id)) }}">{{ $question->code }}</a></td>
                                    <td>{!! $question->content !!}</td>
                                    <td>{{ $questionCate[$question->type] }}</td>
                                    <td>{!! $question->present()->level !!}</td>
                                    <td>{{ $questionType[$question->question_type] }} </td>
                                    <td>{{ $question->lesson->code }} </td>
                                    <td>{!! $question->present()->approve !!}</td>
                                    <td>{!! $question->present()->apply !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pull-right">
                            {{ $questions->appends(Request::all())->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop