@extends('layouts.admin')

@section('title', $lesson->id ? 'Sửa bài thi' : 'Thêm mới bài thi')

@section('content_header')
<h1>{{ $lesson->id ? 'Sửa thông tin' : 'Thêm mới' }} bài thi</h1>

@stop

@section('content')

<div class="box" id="admin">
        <div class="box-title">
            <h4>Thông tin bài thi</h4>
        </div>
        @include('partials.errors')
        <div class="box-body">
            {!! Form::model($lesson, [
                'url' => $url,
                'role' => 'form',
                'method' => $lesson->id ? 'PUT' : 'POST',
                'files' => true,
            ])
            !!}
            <div class="row">
                <div class="col-md-6">
                    <label>Tên:</label>
                    {!!  Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!!  Form::label('', 'Trạng thái : ') !!}
                        {!!  Form::radio('status', '0', true) !!}
                        {!!  Form::label('status', 'Không Hoạt dộng') !!}

                        {!!  Form::radio('status', '1') !!}
                        {!!  Form::label('status', 'Hoạt động') !!}

                    </div>
                </div>
                <hr>
                <div class="col-md-12">
                    <test-content
                        :questions="{{ $lesson->testQuestions->toJson() }}"
                        :knowledgeareas="{{ $knowledgeareas->toJson() }}"
                        :knowledgetypes="{{ $knowledgetypes->toJson() }}"
                        :levels="{{ $levels->toJson() }}"
                        :questioncates="{{ $questioncates->toJson() }}"
                        :questiontypes="{{ $questiontypes->toJson() }}"
                        :questionapproves="{{ $questionapproves->toJson() }}"
                        :questionapplys="{{ $questionapplys->toJson() }}"
                    >
                    </test-content>
                </div>
                <div class="col-md-12">
                    {{Form::submit('Lưu',  ['class'=>'btn btn-success'])}}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
</div>
    
@stop