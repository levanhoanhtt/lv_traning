@extends('layouts.admin')

@section('title', 'Quản lý bài thi')
@section('css')
    @parent
    {!! Html::style('css/style.css') !!}
@stop
@section('content_header')
    <h1>Quản lý bài thi</h1>
    <ul class="list-inline">
        <a href="{{ route('admin.tests.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
    </ul>
    @if(session('status'))
        <div class="alert alert-success">{{session('status')}}</div>
    @endif
@stop

@section('content')
    <div class="row" id="admin">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Danh sách bài thi</h3>
                </div>
                <div class="box-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Trạng thái</th>
                            <th>Name</th>
                            <th>Số câu hỏi</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lessons as $lesson)
                            <tr id="brand-{{$lesson->id}}">
                                <td class="brand-id">{{$lesson->id}}</td>
                                <td>{!! $lesson->present()->status !!}</td>
                                <td>{{ $lesson->name }}</td>
                                <td>{{ $lesson->testQuestions->count() }}</td>
                                <td style="min-width: 100px">
                                    <a data-toggle="tooltip" class="btn btn-sm btn-primary" title="Edit" href="{{ route('admin.tests.edit', $lesson->id) }}"><i class="fa fa-pencil"></i></a>
                                    <a href="{{ route('admin.tests.destroy', $lesson->id) }}"
                                        class="btn btn-sm btn-danger" title="Delete"
                                        data-swal="{{ json_encode(swal([
                                            'title' => 'Are you sure',
                                            'type' => 'question',
                                            'showCancelButton' => true,
                                        ])) }}"
                                        data-swal-method="DELETE"><i class="fa fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-right">
                    {{ $lessons->appends(request()->query())->links() }}
                </div>
                </div>
            </div>
        </div>
    </div>

@stop