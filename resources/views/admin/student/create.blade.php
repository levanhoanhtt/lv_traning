@extends('adminlte::page')

@section('title', 'Thêm học sinh')

@section('content_header')
    <h1>Thêm học sinh</h1>

@stop

@section('content')

    <div class="box">
        <div class="box-title">
            <h4>Thông tin học sinh</h4>
        </div>
        <div class="box-body">
            {!! Form::open(['route' => 'admin.students.store']) !!}
            <div class="form-group col-md-6">
                {!!  Form::label('name', 'Họ tên: ') !!}
                {!!  Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('password', 'Mật khẩu: ') !!}
                {!!  Form::password('password', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('phone_number', 'SDT: ') !!}
                {!!  Form::text('phone_number', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('gender', 'Giới tính: ') !!}
                {!!  Form::select('gender', ['male' => 'Nam', 'female' => 'Nữ'], 'male', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('birthday', 'Ngày sinh: ') !!}
                {!!  Form::date('birthday', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('email', 'Email: ') !!}
                {!!  Form::text('email', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('address', 'Địa chỉ: ') !!}
                {!!  Form::text('address', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('department', 'Bộ phận làm việc: ') !!}
                {!!  Form::text('department', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('company', 'Công ty: ') !!}
                {!!  Form::text('company', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('status', 'Trạng thái ') !!}
                {!!  Form::select('status', \App\Models\User::USER_STATUS, 'male', ['class' => 'form-control']) !!}
            </div>
            {!! Form::submit('Thêm giáo viên', ['class'=>'btn btn-success clear-both']) !!}
            {!! Form::close() !!}
        </div>
    </div>
    @include('errors.error')
@stop