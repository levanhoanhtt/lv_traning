@extends('adminlte::page')

@section('title', 'Học sinh')

@section('content_header')
    <div class="row">
        <div class="col-md-10">
            <h3 style="margin-top: 0">Quản lý học sinh</h3>
        </div>
        <div class="col-md-2">
            <a href="{{url(route('admin.students.create'))}}" class="btn btn-success">Thêm học sinh</a
        </div>
    </div>


    @if(session('status'))
        <div class="alert alert-success">{{session('status')}}</div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Giáo viên</h3>
                    <div class="box-tools">
                        {!! Form::open(['route' => 'admin.students.index', 'method'=>'GET']) !!}
                        <div class="input-group input-group-sm" style="width: 150px;">

                            <input type="text" name="name" class="form-control pull-right" placeholder="Tìm kiếm">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>

                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="box-body">
                    @if($students->count() > 0)
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Họ tên</th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                                <th>Bộ phận</th>
                                <th>Vai trò</th>
                                <th>Ngày tham gia</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr id="brand-{{$student->id}}">
                                    <td><a href="{{url(route('admin.students.edit', $student->id))}}">{{$student->name}}</a></td>
                                    <td>{{$student->phone_number}}</td>
                                    <td>{{$student->email}}</td>
                                    <td>{{$student->department}}</td>
                                    <td>{{$student->role->name}}</td>
                                    <td>{{$student->created_at}}</td>
                                    <td>
                                        {!! Form::open(['action' => ['Admin\StudentController@destroy', $student->id], 'method' => 'delete']) !!}
                                        {!! Form::submit('Xóa', ['class'=>'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$students->appends(Request::only('name'))->links()}}
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop