@extends('adminlte::page')

@section('title', 'Sửa lĩnh vực kinh doanh')

@section('content_header')
    <h1>Sửa lĩnh vực kinh doanh</h1>

@stop

@section('content')

    <div class="box">
        <div class="box-title">
            <h4>Sửa thông tin lĩnh vực kinh doanh {{ $knowledgeArea->name }}</h4>
        </div>
        <div class="box-body">
            {!! Form::model($knowledgeArea, [ 'method'=>'PATCH','action' => ['Admin\KnowledgeAreaController@update',
            $knowledgeArea->id]]) !!}
            <div class="form-group col-md-6">
                {!!  Form::label('name', 'Tên: ') !!}
                {!!  Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('code', 'Mã: ') !!}
                {!!  Form::text('code', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('business', 'Lĩnh vực kinh doanh doanh: ') !!}
                {!!  Form::select('business[]',$businessAreaList, null, ['class' => 'form-control
                select2',
                'multiple' => true]) !!}
            </div>
            <div class="form-group col-md-6">
                {!!  Form::label('description', 'Mô tả: ') !!}
                {!!  Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
            </div>
            <div class="form-group col-md-12">
                {!! Form::submit('Sửa', ['class'=>'btn btn-success clear-both']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('errors.error')
@stop

@section('js')

    <script>
        $(document).ready(function() {
            if($('select.select2').length > 0) $('select.select2').select2();
        });
    </script>
@stop