@extends('adminlte::page')

@section('title', 'Quản lý lĩnh vực kiến thức')

@section('css')
    {!! Html::style('css/style.css') !!}
@stop

@section('content_header')
    <h1>Quản lý lĩnh vực kiến thức</h1>

    <ul class="list-inline">
        <a href="{{url(route('admin.knowledge.create'))}}" class="btn btn-primary">Thêm lĩnh vực</a>
    </ul>

    @if(session('status'))
        <div class="alert alert-success">{{session('status')}}</div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-tools">
                        {!! Form::open(['route' => 'admin.knowledge.index', 'method'=>'GET']) !!}
                        <div class="input-group input-group-sm" style="width: 150px;">

                            <input type="text" name="name" class="form-control pull-right" placeholder="Tìm kiếm">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>

                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="box-body">
                    @if($knowledgeAreas->count() > 0)
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Mã</th>
                            <th>Mô tả</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($knowledgeAreas as $ka)
                            <tr id="brand-{{$ka->id}}">
                                <td><a href="{{url(route('admin.knowledge.edit', $ka->id))}}">{{$ka->name}}</a></td>
                                <td>{{ $ka->code }}</td>
                                <td>{{ $ka->description }}</td>
                                <td>
                                    {!! Form::open(['route' => ['admin.knowledge.destroy', $ka->id], 'method' => 'delete']) !!}
                                    {!! Form::submit('Xóa', ['class'=>'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $knowledgeAreas->appends(Request::only('name'))->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop