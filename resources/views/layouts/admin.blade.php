@extends('adminlte::page')
@section('js')
@parent
{!! Html::script(mix('js/manifest.js')) !!}
{!! Html::script(mix('js/vendor.js')) !!}
{!! Html::script(mix('js/admin.js')) !!}
@stop
@section('css')
@parent
<meta name="csrf-token" content="{{ csrf_token() }}">
{!! Html::style(mix('css/vendor.css')) !!}
{!! Html::style(mix('css/admin.css')) !!}

@stop
