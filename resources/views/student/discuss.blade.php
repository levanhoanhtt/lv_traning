<div class="box-comment">
  <div class="avatar"><i class="fa fa-user" aria-hidden="true"></i></div>
  <div class="right-comment comment{{$discuss->id}}">
    <p><a class="name" href="#">{{$discuss->users->name}} <span>{{$discuss->users->role->name}}</span></a>{{$discuss->content}}</p>
    @if($discuss->image != null)
    <div class="get-img">
      <a href="#" class="send trans"><i class="fa fa-send" aria-hidden="true"></i></a>
      <img src="{{ asset('student/uploads/discuss/' . $discuss->image) }}" alt=" " style="height: 100px; width: 100px;" />
    </div>
    @endif
    <div class="answer clearfix">
      <div class="left">
        <a href="#" class="trans text reply" data-user-lesson-id="{{$discuss->id}}">Trả lời</a><span><img src="{{ asset('student/img/icon12.png') }}" alt=" " /></span>
        <a href="#" class="trans like @if($discuss->userLessonLike()->where('type', App\Models\UserLessonLike::TYPE_LIKE)->where('user_id', $user->id)->first()) active @endif" data-like="{{$discuss->id}}"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span>{{$discuss->userLessonLike()->where('type', App\Models\UserLessonLike::TYPE_LIKE)->get()->count()}}</span></a><span><img src="{{ asset('student/img/icon12.png') }}" alt=" " /></span>
        <a href="#" class="trans dislike @if($discuss->userLessonLike()->where('type', App\Models\UserLessonLike::TYPE_DISLIKE)->where('user_id', $user->id)->first()) active @endif" data-dislike="{{$discuss->id}}"><i class="fa fa-thumbs-down  fa-flip-horizontal" aria-hidden="true"></i>
        <span>{{$discuss->userLessonLike()->where('type', App\Models\UserLessonLike::TYPE_DISLIKE)->get()->count()}}</span></a>
      </div>
      <div class="right">{{Carbon\Carbon::parse($discuss->created_at)->format('Y-m-d | H:i')}}</div>
    </div>
    @if($discuss->userLessonSub->count() > 0)
      @foreach($discuss->userLessonSub as $userLessonSub)
        @include('/student/discussSub')
      @endforeach
    @endif
    <div id="userLessonId{{$discuss->id}}"></div>
  </div>
</div>