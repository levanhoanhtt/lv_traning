@if($userLessonSub != null)
<div class="box-comment sub">
  <div class="avatar"><i class="fa fa-user" aria-hidden="true"></i></div>
  <div class="right-comment">
    <p><a class="name" href="#">{{$userLessonSub->users->name}} <span>{{$userLessonSub->users->role->name}}</span></a>{{$userLessonSub->content}}</p>
    @if($userLessonSub->image != null)
      <div class="multy-image clearfix" style="border: none !important;">
        <img src="{{ asset('student/uploads/subDiscuss/' . $userLessonSub->image) }}" alt="" />
      </div>
    @endif
  </div>
</div>
@endif
