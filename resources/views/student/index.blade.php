<!DOCTYPE html>
<html lang="vi" class="no-js">
  <head>
    <meta charset="utf-8">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Ricky Training</title>
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:300,400,500,700,900&amp;subset=cyrillic,greek,vietnamese" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300i,400,500,700,900&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('student/css/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('student/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('student/js/fancy/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('student/css/style.css') }}">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cleartype" content="on">
    <![endif]-->
  </head>
  <body>
    <input type="hidden" name="lessonId" value="{{$lessons->id}}">
    <header class="clearfix">
      <div class="left">
        <div class="btn-menu"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <h1 class="logo"><a href="#" class="trans"><img src="{{ asset('student/img/logo.png') }}" alt="Ricky studio"></a></h1>
      </div>
      <div class="right">
        <a href="#" class="icon notification trans"><i class="fa fa-bell-o" aria-hidden="true"></i></a>
        <a href="#" class="icon message trans"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        <div class="infor-user">
          <div class="avatar"><i class="fa fa-user" aria-hidden="true"></i></div>
          <div class="name"><span>Hoàng Đình Thái<i class="fa fa-caret-down arrow" aria-hidden="true"></i></span></div>
          <div class="position">Bộ phận sale</div>
          <div class="drop-down">
          <div class="cover">
            <ul>
              <li><a href="#"><img src="{{ asset('student/img/icon-menu01.png') }}" alt=" " />Tài khoản cá nhân</a></li>
              <li><a href="#"><img src="{{ asset('student/img/icon-menu02.pn') }}g" alt=" " />Học bạ</a></li>
              <li class="active"><a href="#"><img src="{{ asset('student/img/icon-menu03.png') }}" alt=" " />Khóa học của tôi</a></li>
              <li><a href="#"><img src="{{ asset('student/img/icon-menu04.png') }}" alt=" " />Lịch sử giao dịch</a></li>
              <li><a href="#"><img src="{{ asset('student/img/icon-menu05.png') }}" alt=" " />Thoát</a></li>
            </ul>
          </div>
          </div>
        </div>
      </div>
    </header>
    <div class="main">
      <div class="container dis-tb clearfix" cellpadding="10">

        <div class="cover-col col01 dis-cell">
          <div class=" col-left get-height2">
            <div id="tabs-container">
              <ul class="tabs-menu tabs-menu-js01 clearfix">
                <li class="current"><a href="#tab-1"><i class="fa fa-list" aria-hidden="true"></i>Nội dung</a></li>
                <li><a href="#tab-2"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>Nhiệm vụ</a></li>
                <li><a href="#tab-3"><i class="fa fa-download" aria-hidden="true"></i>Tài liệu</a></li>
              </ul>
              <div class="tab">
                <div id="tab-1" class="tab-content tab-content-js01">
                  <div class="custom-bar get-height">
                    @foreach ($lessons->addendums->curriculums->addendums as $addendum)
                      @if(count($addendum->lessons) > 0)
                        <div class="head-accor head-accor-js active">{{$addendum->name}}<i class="fa fa-caret-down arrow" aria-hidden="true"></i></div>
                        <div class="content-accor body-accor-js" style="display: block;">
                          <ul>
                            @foreach ($addendum->lessons as $key => $lesson)
                              <li class="@if($lessons->id == $lesson->id) watching @endif" data-url="{{ url(route('student.index', ['lessonId' => $lesson->id])) }}">
                                <a href="{{ url(route('student.index', ['lessonId' => $lesson->id])) }}"><p class="ttl-lesson">{{$lesson->name}}</p></a>
                                <p>
                                  <a href="#" class="trans time"><i class="fa fa-play-circle" aria-hidden="true"></i>03:45</a>
                                  <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                                  <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                                </p>
                              </li>
                            @endforeach
                          </ul>
                        </div>
                      @endif
                    @endforeach
                    <!-- <div class="head-accor head-accor-js active">PHẦN 1: GIỚI THIỆU CHUNG<i class="fa fa-caret-down arrow" aria-hidden="true"></i></div>
                    <div class="content-accor body-accor-js" style="display: block;">
                      <ul>
                        <li class="watched">
                          <p class="ttl-lesson"><a href="{{ url(route('student.detail')) }}">Bài giảng 1: Giới thiệu chung về Ukulele</a></p>
                          <p>
                            <a href="#" class="trans time"><i class="fa fa-play-circle" aria-hidden="true"></i>03:45</a>
                            <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                            <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                          </p>
                        </li>
                        <li class="watching">
                          <p class="ttl-lesson"><a href="{{url(route('student.detail'))}}">Bài giảng 2: Phân loại đàn Ukulele</a></p>
                          <p>
                            <a href="#" class="trans time"><i class="fa fa-play-circle" aria-hidden="true"></i>03:45</a>
                            <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                            <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                          </p>
                        </li>
                        <li>
                          <p class="ttl-lesson"><a href="{{url(route('student.detail'))}}">Bài giảng 3: Cách chọn mua Ukulele</a></p>
                          <p>
                            <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                            <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                          </p>
                        </li>
                        <li>
                          <p class="ttl-lesson"><a href="{{url(route('student.detail'))}}">Bài giảng 4: Cách bảo quản đàn Ukulele</a></p>
                          <p>
                            <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                            <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                          </p>
                        </li>
                      </ul>
                    </div> -->
                    <!-- <div class="head-accor head-accor-js active">PHẦN 2: HƯỚNG DẪN CƠ BẢN<i class="fa fa-caret-down arrow" aria-hidden="true"></i></div>
                    <div class="content-accor body-accor-js" style="display: block;">
                      <ul>
                        <li>
                          <p class="ttl-lesson"><a href="{{url(route('student.detail'))}}">Bài giảng 5: Cách cầm đàn Ukulele</a></p>
                          <p>
                            <a href="#" class="trans time"><i class="fa fa-play-circle" aria-hidden="true"></i>03:45</a>
                            <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                            <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                          </p>
                        </li>
                        <li>
                          <p class="ttl-lesson"><a href="{{url(route('student.detail'))}}">Bài giảng 6: Khởi động tay khi chơi Ukulele</a></p>
                          <p>
                            <a href="#" class="trans time"><i class="fa fa-play-circle" aria-hidden="true"></i>03:45</a>
                            <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                            <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                          </p>
                        </li>
                        <li>
                          <p class="ttl-lesson">Bài giảng 7: Thực hành: Khởi động tay trái 1</p>
                          <p>
                            <a href="#" class="trans time"><i class="fa fa-play-circle" aria-hidden="true"></i>03:45</a>
                            <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                            <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                          </p>
                        </li>
                        <li>
                          <p class="ttl-lesson">Bài giảng 9: Thực hành: Khởi động tay trái 2</p>
                          <p>
                            <a href="#" class="trans time"><i class="fa fa-play-circle" aria-hidden="true"></i>03:45</a>
                            <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                            <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                          </p>
                        </li>
                        <li>
                          <p class="ttl-lesson">Bài giảng 10: Thực hành: Khởi động tay trái 2</p>
                          <p>
                            <a href="#" class="trans time"><i class="fa fa-play-circle" aria-hidden="true"></i>03:45</a>
                            <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                            <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                          </p>
                        </li>
                        <li>
                          <p class="ttl-lesson">Bài giảng 11: Thực hành: Khởi động tay trái 2</p>
                          <p>
                            <a href="#" class="trans time"><i class="fa fa-play-circle" aria-hidden="true"></i>03:45</a>
                            <a href="#" class="trans"><i class="fa fa-download" aria-hidden="true"></i></a>
                            <a href="#" class="trans"><i class="fa fa-comments" aria-hidden="true"></i></a>
                          </p>
                        </li>
                      </ul>
                    </div> -->

                  </div>
                </div>
                <div id="tab-2" class="tab-content tab-content-js01">
                  <p>Donec semper dictum sem, quis pretium sem malesuada non. Proin venenatis orci vel nisl porta sollicitudin. Pellentesque sit amet massa et orci malesuada facilisis vel vel lectus. Etiam tristique volutpat auctor. Morbi nec massa eget sem ultricies fermentum id ut ligula. Praesent aliquet adipiscing dictum. Suspendisse dignissim dui tortor. Integer faucibus interdum justo, mattis commodo elit tempor id. Quisque ut orci orci, sit amet mattis nulla. Suspendisse quam diam, feugiat at ullamcorper eget, sagittis sed eros. Proin tortor tellus, pulvinar at imperdiet in, egestas sed nisl. Aenean tempor neque ut felis dignissim ac congue felis viverra. </p>

                </div>
                <div id="tab-3" class="tab-content tab-content-js01">
                  <p>Duis egestas fermentum ipsum et commodo. Proin bibendum consectetur elit, hendrerit porta mi dictum eu. Vestibulum adipiscing euismod laoreet. Vivamus lobortis tortor a odio consectetur pulvinar. Proin blandit ornare eros dictum fermentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur laoreet, ante aliquet molestie laoreet, lectus odio fringilla purus, id porttitor erat velit vitae mi. Nullam posuere nunc ut justo sollicitudin interdum. Donec suscipit eros nec leo condimentum fermentum. Nunc quis libero massa. Integer tempus laoreet lectus id interdum. Integer facilisis egestas dui at convallis. Praesent elementum nisl et erat iaculis a blandit ligula mollis. Vestibulum vitae risus dui, nec sagittis arcu. Nullam tortor enim, placerat quis eleifend in, viverra ac lacus. Ut aliquam sapien ut metus hendrerit auctor dapibus justo porta. </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="cover-col col02 dis-cell">
          <div class=" col-mid main-content get-height2">
            <div class="top-main clearfix">
              <div class="right">
                <div class="view-style">Xem kiểu <a href="" class="has-sidebar active"></a><a href="" class="no-sidebar"></a></div>
                <div class="rating ">Đánh giá
                  <a data-fancybox="modal" data-src="#modal" class="trans click-pop" href="javascript:;">
                    <div class="box-rating clearfix star2">
                      <label for="rd01"></label><input id="rd01" class="change-value" type="radio" data-class="star1" value="1" name="rating01" />
                      <label for="rd02"></label><input id="rd02" class="change-value" type="radio" data-class="star2" value="2" name="rating01" />
                      <label for="rd03"></label><input id="rd03" class="change-value" type="radio" data-class="star3" value="3" name="rating01" />
                      <label for="rd04"></label><input id="rd04" class="change-value" type="radio" data-class="star4" value="4" name="rating01" />
                      <label for="rd05"></label><input id="rd05" class="change-value" type="radio" data-class="star5" value="5" name="rating01" />
                    </div>
                  </a>
                </div>
                <a class="giude trans" href="#"><i class="fa fa-info-circle" aria-hidden="true"></i>Xem hướng dẫn</a>
              </div>
            </div>
            <!-- if type = video -->
            @if($lessons->type == App\Models\Lesson::LESSON_TYPE_VIDEO)
            <input type="hidden" name="video" id="inputVideo" value="{{asset('student/file/' . $lessons->file)}}">
            <div class="video" id="video">
              <img src="{{ asset('student/img/video.jpg') }}" alt="video" />
            </div>
            <div class="control-video clearfix">
              <div class="left">
                Tự động chạy
                <div class="flatRoundedCheckbox">
                  <input type="checkbox" name="check" id="flatOneRoundedCheckbox" value="1">
                  <label for="flatOneRoundedCheckbox"></label>
                  <div></div>
                </div>
              </div>
              <div class="right">
                <a href="#" class="prev trans"><i class="fa fa-step-backward" aria-hidden="true"></i></a>
                <a href="#" class="next trans"><i class="fa fa-step-forward" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
          @endif
          <!-- if type = text -->
          @if($lessons->type == App\Models\Lesson::LESSON_TYPE_TEXT)
          <div class="content-post height-content">
              <div class="inner custom-bar get-height3" id="content-l3">
                {{$lessons->content}}
                <!-- <h1>Title page h1</h1>
                <h2>Title page h2</h2>
                <h3>Title page h3</h3>
                <h4>Title page h4</h4>
                <h5>Title page h5</h5>
                <ul>
                  <li>Delectus consectetur quos laboriosam, accusamus, ipsam mollitia labore voluptatibus totam fugi</li>
                  <li>Aspernatur recusandae, in blanditiis sapiente reiciendis eius delectus, eos,</li>
                  <li>Atque non culpa quidem reprehenderit molestiae nisi corrupti esse similique ipsa, at max</li>
                  <li>Modi sequi voluptatibus optio, eius corrupti ipsam facere quisquam ipsa reprehenderit, tenetur m</li>
                  <li>Maxime, recusandae, repudiandae. Asperiores laudantium aperiam, odio consequatur corporis qui</li>
                </ul>
                <ol>
                  <li>Soluta nemo, dolorem maxime in! Culpa, ipsa ducimus corrupti! Odio, sed! Ad eum qui</li>
                  <li>Aut rerum eligendi modi error, magni! Cum eligendi commodi similique quibusdam in aliquam min</li>
                  <li>Eaque reiciendis, nesciunt aperiam quisquam facere odio, molestias voluptate, fuga cupiditate dolor minus a</li>
                  <li>Totam incidunt nihil tenetur soluta quo et laudantium, aut ullam, doloremque debitis reiciendis</li>
                  <li>Impedit mollitia eveniet inventore harum magni adipisci, nihil ipsa aliquid ratione animi beatae. Porro </li>
                </ol>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit laboriosam cumque natus illo, voluptates sint quidem nemo iusto atque facere optio unde autem, reprehenderit totam, quaerat impedit. Quae molestias, maiores.</p>
                <p>Id dolorem temporibus ipsa nemo suscipit accusantium inventore vero similique at maxime quia molestias ex illo rerum, totam delectus, necessitatibus aspernatur architecto cupiditate voluptatum quo nobis quis enim quasi exercitationem!</p>
                <p><strong>Lorem ipsum dolor sit amet</strong></p>
                <p>Quibusdam harum, in maxime tempore temporibus fuga illum laudantium quisquam alias et? Excepturi laudantium, vel repellat cum! Veritatis, recusandae perspiciatis quasi quibusdam, molestiae eveniet consectetur consequatur fugit voluptates laudantium, atque.</p>
                <p>Ullam beatae magni sunt quasi repudiandae, quaerat excepturi voluptas optio deleniti sed ducimus eveniet rerum similique necessitatibus laudantium cupiditate. Pariatur dicta eligendi tempora, culpa temporibus, aperiam ipsum error quidem consectetur?</p>
                <p>Autem fugit, praesentium. Officia vitae pariatur sequi quae porro saepe aliquam ex a perferendis quis, modi assumenda culpa voluptas, eveniet earum alias natus commodi tenetur dolorum nobis. Consequuntur, dolore, illum.</p>
                <p>Totam earum officiis omnis magni ipsa at nemo recusandae perspiciatis, esse reiciendis, cumque provident, quia. Sed fugiat iusto a enim, animi cumque quasi, voluptatem, non pariatur iste dignissimos totam mollitia?</p>
                <p>Tempora voluptate sed amet, enim possimus necessitatibus incidunt soluta provident, est consectetur, iusto! Reiciendis at nisi porro quasi expedita explicabo ab voluptatibus dolorem velit eum deleniti, a quidem, ad debitis?</p>
                <p>Repellat non ut natus quos dolor sint quasi et modi id officiis culpa, vero illum, veritatis sed commodi consectetur iure, labore similique! Quam voluptates asperiores, maxime voluptatum! Facilis, maiores, impedit.</p>
                <p>Nisi, consequuntur architecto? Odit nemo laudantium sapiente delectus, assumenda ex eos eum maiores expedita est non culpa, laborum! Natus provident error officiis enim impedit, blanditiis tenetur mollitia iste doloribus nulla.</p>
                <p>Iusto voluptates, eaque ad dolores consectetur suscipit exercitationem tempora nemo a, amet porro quasi doloremque nihil voluptas adipisci, asperiores eveniet maiores reprehenderit laborum ipsum, harum fugiat enim veritatis animi. Qui!</p>
                <p>Quibusdam, ut voluptate! Magnam ratione, accusantium impedit odio ducimus, enim ab commodi voluptates doloribus dicta voluptatibus modi eveniet doloremque omnis velit. Id consequuntur, voluptatibus asperiores? Quia dolore odio quisquam culpa!</p>
                <p>Placeat ipsum quam illo reprehenderit natus praesentium dolores magnam, temporibus, quis iure repellendus sapiente enim hic modi commodi facilis pariatur dignissimos eius quae repudiandae qui. Tempore, in nostrum id error?</p>
                <p>Explicabo nisi modi nam nihil ullam commodi quam necessitatibus culpa molestiae doloribus reprehenderit alias aliquam, voluptates iusto dolorum ipsa. Ex laborum fugit deserunt, excepturi, ratione odio. Aliquid quod, facilis dicta.</p>
                <p>Odit eos aperiam dignissimos illum nam laborum, ullam commodi, tempora eaque unde temporibus totam eveniet error obcaecati ducimus hic. Delectus dolores quia nulla cupiditate consequatur aspernatur magnam non saepe expedita.</p>
                <p>Consequuntur optio dicta, deleniti? Quam possimus, tenetur ad reiciendis dignissimos consequuntur et, deleniti vel reprehenderit laborum nobis rem, corporis velit iusto ducimus nihil libero voluptatibus, at magnam! Facilis, aliquam, repellat?</p>
                <p>Quod beatae dolorum quasi excepturi perspiciatis tempore, debitis deserunt et eos omnis. Ducimus ex odit earum modi architecto minus perspiciatis voluptatibus fuga laborum ratione, facilis omnis iste atque praesentium odio.</p>
                <p>Asperiores obcaecati reprehenderit, eveniet saepe debitis quia cumque harum velit cum consectetur ducimus, impedit eius at. Obcaecati, molestias similique iusto ad explicabo soluta modi nihil. Exercitationem, reprehenderit animi natus fugiat.</p>
                <p>Inventore consectetur quae facere dolore odit ducimus, quas veniam sint corporis accusantium! Ut perspiciatis, quibusdam! Facere ducimus iure ex neque molestias necessitatibus odio quibusdam, dolore ad, omnis, quae quis quaerat!</p>
                <p>Obcaecati veniam adipisci modi provident sunt quis ullam, explicabo vitae natus omnis id accusamus autem, officiis, sit? Minima rem obcaecati, perferendis cupiditate officiis tenetur commodi. Harum veritatis quos eius, nobis.</p>
                <p>Neque consectetur ea sit accusantium eos, ullam, cumque eaque ipsam. Sint laborum soluta adipisci totam hic similique ullam deserunt, explicabo qui eligendi, molestias, doloribus dolore enim temporibus, possimus ut consequuntur!</p>
                <ul>
                  <li>Delectus consectetur quos laboriosam, accusamus, ipsam mollitia labore voluptatibus totam fugi</li>
                  <li>Aspernatur recusandae, in blanditiis sapiente reiciendis eius delectus, eos,</li>
                  <li>Atque non culpa quidem reprehenderit molestiae nisi corrupti esse similique ipsa, at max</li>
                  <li>Modi sequi voluptatibus optio, eius corrupti ipsam facere quisquam ipsa reprehenderit, tenetur m</li>
                  <li>Maxime, recusandae, repudiandae. Asperiores laudantium aperiam, odio consequatur corporis qui</li>
                </ul>
                <ol>
                  <li>Soluta nemo, dolorem maxime in! Culpa, ipsa ducimus corrupti! Odio, sed! Ad eum qui</li>
                  <li>Aut rerum eligendi modi error, magni! Cum eligendi commodi similique quibusdam in aliquam min</li>
                  <li>Eaque reiciendis, nesciunt aperiam quisquam facere odio, molestias voluptate, fuga cupiditate dolor minus a</li>
                  <li>Totam incidunt nihil tenetur soluta quo et laudantium, aut ullam, doloremque debitis reiciendis</li>
                  <li>Impedit mollitia eveniet inventore harum magni adipisci, nihil ipsa aliquid ratione animi beatae. Porro </li>
                </ol>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit laboriosam cumque natus illo, voluptates sint quidem nemo iusto atque facere optio unde autem, reprehenderit totam, quaerat impedit. Quae molestias, maiores.</p>
                <p>Id dolorem temporibus ipsa nemo suscipit accusantium inventore vero similique at maxime quia molestias ex illo rerum, totam delectus, necessitatibus aspernatur architecto cupiditate voluptatum quo nobis quis enim quasi exercitationem!</p>
                <p><strong>Lorem ipsum dolor sit amet</strong></p>
                <p>Quibusdam harum, in maxime tempore temporibus fuga illum laudantium quisquam alias et? Excepturi laudantium, vel repellat cum! Veritatis, recusandae perspiciatis quasi quibusdam, molestiae eveniet consectetur consequatur fugit voluptates laudantium, atque.</p>
                <p>Ullam beatae magni sunt quasi repudiandae, quaerat excepturi voluptas optio deleniti sed ducimus eveniet rerum similique necessitatibus laudantium cupiditate. Pariatur dicta eligendi tempora, culpa temporibus, aperiam ipsum error quidem consectetur?</p>
                <p>Autem fugit, praesentium. Officia vitae pariatur sequi quae porro saepe aliquam ex a perferendis quis, modi assumenda culpa voluptas, eveniet earum alias natus commodi tenetur dolorum nobis. Consequuntur, dolore, illum.</p>
                <p>Totam earum officiis omnis magni ipsa at nemo recusandae perspiciatis, esse reiciendis, cumque provident, quia. Sed fugiat iusto a enim, animi cumque quasi, voluptatem, non pariatur iste dignissimos totam mollitia?</p>
                <p>Tempora voluptate sed amet, enim possimus necessitatibus incidunt soluta provident, est consectetur, iusto! Reiciendis at nisi porro quasi expedita explicabo ab voluptatibus dolorem velit eum deleniti, a quidem, ad debitis?</p>
                <p>Repellat non ut natus quos dolor sint quasi et modi id officiis culpa, vero illum, veritatis sed commodi consectetur iure, labore similique! Quam voluptates asperiores, maxime voluptatum! Facilis, maiores, impedit.</p>
                <p>Nisi, consequuntur architecto? Odit nemo laudantium sapiente delectus, assumenda ex eos eum maiores expedita est non culpa, laborum! Natus provident error officiis enim impedit, blanditiis tenetur mollitia iste doloribus nulla.</p>
                <p>Iusto voluptates, eaque ad dolores consectetur suscipit exercitationem tempora nemo a, amet porro quasi doloremque nihil voluptas adipisci, asperiores eveniet maiores reprehenderit laborum ipsum, harum fugiat enim veritatis animi. Qui!</p>
                <p>Quibusdam, ut voluptate! Magnam ratione, accusantium impedit odio ducimus, enim ab commodi voluptates doloribus dicta voluptatibus modi eveniet doloremque omnis velit. Id consequuntur, voluptatibus asperiores? Quia dolore odio quisquam culpa!</p>
                <p>Placeat ipsum quam illo reprehenderit natus praesentium dolores magnam, temporibus, quis iure repellendus sapiente enim hic modi commodi facilis pariatur dignissimos eius quae repudiandae qui. Tempore, in nostrum id error?</p>
                <p>Explicabo nisi modi nam nihil ullam commodi quam necessitatibus culpa molestiae doloribus reprehenderit alias aliquam, voluptates iusto dolorum ipsa. Ex laborum fugit deserunt, excepturi, ratione odio. Aliquid quod, facilis dicta.</p>
                <p>Odit eos aperiam dignissimos illum nam laborum, ullam commodi, tempora eaque unde temporibus totam eveniet error obcaecati ducimus hic. Delectus dolores quia nulla cupiditate consequatur aspernatur magnam non saepe expedita.</p>
                <p>Consequuntur optio dicta, deleniti? Quam possimus, tenetur ad reiciendis dignissimos consequuntur et, deleniti vel reprehenderit laborum nobis rem, corporis velit iusto ducimus nihil libero voluptatibus, at magnam! Facilis, aliquam, repellat?</p>
                <p>Quod beatae dolorum quasi excepturi perspiciatis tempore, debitis deserunt et eos omnis. Ducimus ex odit earum modi architecto minus perspiciatis voluptatibus fuga laborum ratione, facilis omnis iste atque praesentium odio.</p>
                <p>Asperiores obcaecati reprehenderit, eveniet saepe debitis quia cumque harum velit cum consectetur ducimus, impedit eius at. Obcaecati, molestias similique iusto ad explicabo soluta modi nihil. Exercitationem, reprehenderit animi natus fugiat.</p>
                <p>Inventore consectetur quae facere dolore odit ducimus, quas veniam sint corporis accusantium! Ut perspiciatis, quibusdam! Facere ducimus iure ex neque molestias necessitatibus odio quibusdam, dolore ad, omnis, quae quis quaerat!</p>
                <p>Obcaecati veniam adipisci modi provident sunt quis ullam, explicabo vitae natus omnis id accusamus autem, officiis, sit? Minima rem obcaecati, perferendis cupiditate officiis tenetur commodi. Harum veritatis quos eius, nobis.</p>
                <p>Neque consectetur ea sit accusantium eos, ullam, cumque eaque ipsam. Sint laborum soluta adipisci totam hic similique ullam deserunt, explicabo qui eligendi, molestias, doloribus dolore enim temporibus, possimus ut consequuntur!</p> -->
              </div>

              <!-- <div class="up-down">
                <a class="up" href="#"><img src="img/icon14.png" alt=""></a>
                <a class="down" href="#"><img src="img/icon15.png" alt=""></a>
              </div> -->
              <div class="control-video clearfix">
                <div class="right">
                  <a href="#" class="prev trans"><i class="fa fa-step-backward" aria-hidden="true"></i></a>
                  <a href="#" class="next trans"><i class="fa fa-step-forward" aria-hidden="true"></i></a>
                </div>
            </div>
            </div>
          </div>
        @endif
        <!-- if type = audio -->
        @if($lessons->type == App\Models\Lesson::LESSON_TYPE_AUDIO)
            <div class="video">
              <img src="{{ asset('student/img/video.jpg') }}" alt="video" />
            </div>
            <div class="control-video clearfix">
              <div class="left">
                Tự động chạy
                <div class="flatRoundedCheckbox">
                  <input type="checkbox" name="check" id="flatOneRoundedCheckbox" value="1">
                  <label for="flatOneRoundedCheckbox"></label>
                  <div></div>
                </div>
              </div>
              <div class="right">
                <a href="" class="prev trans"><i class="fa fa-step-backward" aria-hidden="true"></i></a>
                <a href="" class="next trans"><i class="fa fa-step-forward" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
          @endif
        </div>
        

        <div class="cover-col col03 dis-cell">
          <div class=" col-right get-height2">
            <ul class="tabs-menu tabs-menu-js two-item clearfix">
              <li class="current"><a href="#tab-right-1"><img src="{{ asset('student/img/icon08.png') }}" alt="" >Tự tổng hợp</a></li>
              <li><a href="#tab-right-2"><i class="fa fa-comments" aria-hidden="true"></i>Thảo luận</a></li>
            </ul>
            <div class="tab">
              <div id="tab-right-1" class="tab-content tab-content-js">
                <div class="custom-bar get-height">
                  <div class="list-note" id="listnotetab1">
                    <p class="italic let">Hãy tạo sổ tay cho riêng mình</p>
                    <div class="mb20">
                      <div class="box-text" id="divCreate">
                        <input type="text" placeholder="Tiêu đề ghi chú..." name="title" />
                        <hr>
                        <textarea placeholder="Thêm nội dung ghi chú..." name="content"></textarea>
                      </div>
                      <div class="text-right"><button class="submit pointer trans" id="create"><i class="fa fa-paper-plane" aria-hidden="true"></i>Lưu</button></div>
                    </div>
                    <!-- <div class="box-note">
                      <div class="edit">
                        <a data-fancybox="modal" data-src="#modal-fixup01" class="trans click-pop" href="javascript:;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      </div>
                      <h3 class="ttl-box-note">Lưu ý khi chọn đàn Ukulele</h3>
                      <div class="date">03:15  |  08/12/2017</div>
                      <p>Giá thị trường Việt Nam từ 600k - 1500k cho 1 cây tùy vào chất liệu gỗ và nguồn đàn</p>
                    </div>
                    <div id="modal-fixup01" class="content-pop hidden">
                      <a class="close trans" href="javascript:;" data-fancybox-close ><i class="fa fa-times" aria-hidden="true"></i></a>
                      <h2 class="ttl-pop-rating">CHỈNH SỬA ĐÁNH GIÁ CỦA BẠN 1</h2>
                      <div class="content-rating-pop">
                        <input type="text" class="fix-ttl" value="Chỉnh dây đàn Ukulele" />
                        <textarea class="content-mess fixup">Có thể chỉnh bằng Tuner hoặc chỉnh tay</textarea>
                        <div class="text-center">
                          <button class="cmn-btn trans">Lưu</button>
                        </div>
                      </div>
                    </div> -->
                    @if($synthetics->count() >0)
                      @foreach($synthetics as $synthetic)
                        @include('/student/synthetics')
                      @endforeach
                    @endif
                  </div>
                </div>
              </div>
              <div id="tab-right-2" class="tab-content tab-content-js">
                <div class="custom-bar get-height">
                  <div class="list-note second">
                    <p class="italic let">Hãy đặt câu hỏi để nhận giải đáp <img class="inline" src="{{ asset('student/img/icon09.png') }}" alt="i" /></p>
                    <div class="comment">
                      <div id="errorDiscuss"></div>
                      <div class="box-comment" id="comment">
                        <div class="avatar"><i class="fa fa-user" aria-hidden="true"></i></div>
                        <div class="right-comment">
                          <div class="box-write clearfix">
                            <table>
                              <tr>
                                <th><input type="text" name="discuss"/></th>
                                <input type="file" name="image_discuss" style="display: none;" accept="image/*">
                                <td class="imoj"><a href="#" class="trans"><img src="{{ asset('student/img/icon10.png') }}" alt=" " /></a></td>
                                <td class="pic"><a href="#" class="trans" id="image_discuss"><img src="{{ asset('student/img/icon11.png') }}" alt=" " /></a></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>

                      <!-- <div class="box-comment">

                        <div class="avatar"><i class="fa fa-user" aria-hidden="true"></i></div>

                        <div class="right-comment">

                          <p><a class="name" href="#">Hoàng Văn Học</a>Có văn học là khoa học ci Văn học là khoa học cu hihi dhdhdhdh</p>

                          <div class="get-img">
                            <a href="#" class="send trans"><i class="fa fa-send" aria-hidden="true"></i></a>
                            <img src="{{ asset('student/img/image.png') }}" alt=" " />
                          </div>
                          <div class="answer clearfix">
                            <div class="left">
                              <a href="#" class="trans text">Trả lời</a><span><img src="{{ asset('student/img/icon12.png') }}" alt=" " /></span>
                              <a href="#" class="trans like active"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span>1</span></a><span><img src="{{ asset('student/img/icon12.png') }}" alt=" " /></span>
                              <a href="#" class="trans dislike"><i class="fa fa-thumbs-down  fa-flip-horizontal" aria-hidden="true"></i></a>
                            </div>
                            <div class="right">1/12/2017  19:31</div>
                          </div>


                          <div class="box-comment sub">
                            <div class="avatar"><i class="fa fa-user" aria-hidden="true"></i></div>
                            <div class="right-comment">
                              <div class="sub-anwer">
                                <input type="text" value="khi bản thân đang trả lời" name="sub_discuss">
                                <div class="right">
                                  <a href="#" class="trans"><img src="{{ asset('student/img/icon10.png') }}" alt=" " /></a>
                                  <a href="#" class="trans"><img src="{{ asset('student/img/icon11.png') }}" alt=" " /></a>
                                </div>
                              </div>
                              <div class="multy-image clearfix">
                              <a class="close trans" href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
                                <img src="{{ asset('student/img/image02.png') }}" alt="" />
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> -->
                      @if($discuss->count() > 0)
                        @foreach($discuss as $discuss)
                          @include('/student/discuss')
                        @endforeach
                      @endif

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div id="modal" class="content-pop hidden">
      <a class="close trans" href="javascript:;" data-fancybox-close ><i class="fa fa-times" aria-hidden="true"></i></a>
      <h2 class="ttl-pop-rating">CHÚNG TÔI RẤT BIẾT ƠN ĐÁNH GIÁ CỦA BẠN</h2>
      <div class="content-rating-pop">
        <div class="detail-rating">
          <table>
            <tr>
              <th>Bài giảng</th>
              <td>
                <div class="box-rating clearfix star2">
                  <label for="rd011"></label><input id="rd011" class="change-value" type="radio" data-class="star1" value="1" name="rating01" />
                  <label for="rd012"></label><input id="rd012" class="change-value" type="radio" data-class="star2" value="2" name="rating01" />
                  <label for="rd013"></label><input id="rd013" class="change-value" type="radio" data-class="star3" value="3" name="rating01" />
                  <label for="rd014"></label><input id="rd014" class="change-value" type="radio" data-class="star4" value="4" name="rating01" />
                  <label for="rd015"></label><input id="rd015" class="change-value" type="radio" data-class="star5" value="5" name="rating01" />
                </div>
              </td>
            </tr>
            <tr>
              <th>Video</th>
              <td>
                <div class="box-rating clearfix star2">
                  <label for="rd021"></label><input id="rd021" class="change-value" type="radio" data-class="star1" value="1" name="rating01" />
                  <label for="rd022"></label><input id="rd022" class="change-value" type="radio" data-class="star2" value="2" name="rating01" />
                  <label for="rd023"></label><input id="rd023" class="change-value" type="radio" data-class="star3" value="3" name="rating01" />
                  <label for="rd024"></label><input id="rd024" class="change-value" type="radio" data-class="star4" value="4" name="rating01" />
                  <label for="rd025"></label><input id="rd025" class="change-value" type="radio" data-class="star5" value="5" name="rating01" />
                </div>
              </td>
            </tr>
            <tr>
              <th>Giảng viên</th>
              <td>
                <div class="box-rating clearfix star2">
                  <label for="rd031"></label><input id="rd031" class="change-value" type="radio" data-class="star1" value="1" name="rating01" />
                  <label for="rd032"></label><input id="rd032" class="change-value" type="radio" data-class="star2" value="2" name="rating01" />
                  <label for="rd033"></label><input id="rd033" class="change-value" type="radio" data-class="star3" value="3" name="rating01" />
                  <label for="rd034"></label><input id="rd034" class="change-value" type="radio" data-class="star4" value="4" name="rating01" />
                  <label for="rd035"></label><input id="rd035" class="change-value" type="radio" data-class="star5" value="5" name="rating01" />
                </div>
              </td>
            </tr>
          </table>
        </div>
        <textarea class="content-mess" placeholder="Mô tả cảm nghĩ của bạn tại đây"></textarea>
        <div class="text-center">
          <button class="cmn-btn trans">Gửi đánh giá</button>
        </div>
      </div>
    </div>
  </div>
  <footer>
  </footer>

  <script src="{{ asset('student/js/jquery-1.12.3.min.js') }}"></script>
  <script src="{{ asset('student/js/modernizr.js') }}"></script>
  <script src="{{ asset('student/js/flexibility.min.js') }}"></script>
  <script src="{{ asset('student/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
  <script src="{{ asset('student/js/jquery.placeholder.js') }}"></script>
  <script src="{{ asset('student/js/fancy/jquery.fancybox.js') }}"></script>
  <script src="{{ asset('student/js/scripts.js') }}"></script>
  <script src="{{ asset('student/js/jwplayer-7.2.2/jwplayer.js') }}"></script>
  <script> flexibility(document.documentElement); </script>
  <script>
    $(document).ready(function() {
      var lessonId = $('input[name="lessonId"]').val();
      // jwplayer
      if ($('#video').length > 0) {
        var file = $('input#inputVideo').val();
        var countSeek = 0;
        var countPause = 0;
        jwplayer.key = "D6QFs8Rgdvib9vxFx05u3FwZmpUR0isc4k0iYA==";
        jwplayer("video").setup({
            "file": file,
            "height": 390,
            "width": 695,
            plugins: {'/student/js/countdown-master/countdown.js':{}},
            "events": {
              onPause: function(object) {
                countPause = countPause + 1;
                var start = jwplayer().getPosition();
                $.ajax({
                  url: '/student/user_lesson_log/'+lessonId,
                  type: 'post',
                  data: {_token: CSRF_TOKEN, status: 1, count: countPause, start: start},
                  success: function(data) {
                    console.log(data);
                  }
                });
              },
              onSeek: function() {
                countSeek = countSeek + 1;
                var start = jwplayer().getPosition();
                $.ajax({
                  url: '/student/user_lesson_log/'+lessonId,
                  type: 'post',
                  data: {_token: CSRF_TOKEN, status: 2, count: countSeek, start: start},
                  success: function(data) {
                    console.log(data);
                  }
                });
              },
              onComplete: function() {
                $.ajax({
                  url: '/student/user_lesson_log/'+lessonId,
                  type: 'post',
                  data: {_token: CSRF_TOKEN, status: 3, count: 0},
                  success: function(data) {
                    console.log(data);
                  }
                });
              },
            }
        });
        jwplayer().onReady(function() {
          jwplayer().pause(true);
          jwplayer().play(true);
          jwplayer().seeked(true);
        });
      }
      // next, prev page
      var nextUrl = $('li.watching').next('li').data('url');
      var prevUrl = $('li.watching').prev('li').data('url');
      $('a.next').attr('href', nextUrl);
      $('a.prev').attr('href', prevUrl);
      // insert synthetics
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $('button#create').click(function() {
        var title = $('div#divCreate input[name="title"]').val();
        var content = $('div#divCreate textarea[name="content"]').val();
        $.ajax({
            url: '/student/insert_synthetics/'+lessonId,
            type: 'post',
            data: {_token: CSRF_TOKEN, title: title, content: content},
            success: function (data) {
              $('div.alert').hide();
              if (data.status == 'error') {
                $('#listnotetab1 p.italic').before(data.html);
              } else {
                $('div#divCreate input[name="title"]').val('');
                $('div#divCreate textarea[name="content"]').val('');
                $('#listnotetab1 div.mb20').after(data.html).slideDown("fast");
              }
            }
        });
      });
      // update synthetics
      $(document).on('click', 'button.update', function() {
        var id = $(this).data('id');
        var title = $('div#modal-fixup'+id+' input[name="title"]').val();
        var content = $('div#modal-fixup'+id+' textarea[name="content"]').val();
        $.ajax({
            url: '/student/update_synthetics/'+id,
            type: 'post',
            data: {_token: CSRF_TOKEN, title: title, content: content},
            success: function (data) {
              $(this).attr('data', 'fancybox-close');
              $('div.alert').hide();
              if (data.status == 'error') {
                $('div#modal-fixup'+id+' div.content-rating-pop input').before(data.html);
              } else {
                var synthetic = data.synthetic;
                var d = new Date(synthetic.updated_at);
                var day = d.getDate();
                var month = d.getMonth() + 1;
                var year = d.getFullYear();

                var hour = d.getHours();
                var min = d.getMinutes();
                var date = min+':'+hour+' | '+day+'-'+month+'-'+year;
                $('div#modal-fixup'+id+' div.content-rating-pop input').before(data.html);
                $('div#boxnote'+id+' h3').html(title);
                $('div#boxnote'+id+' p.text-underline').html(content);
                $('div#boxnote'+id+' div.date').html(date);
              }
            }
        });
      });
      // comment 
      $('a#image_discuss').on('click', function() {
        $('input[name="image_discuss"]').click();
      });
      $('input[name="discuss"]').keyup(function(e){
        if(e.keyCode == 13)
        {
          var discuss = $(this).val();
          var image_discuss = $('input[name="image_discuss"]').prop('files')[0];
          var form_data = new FormData();
          form_data.append('discuss', discuss);
          form_data.append('image_discuss', image_discuss);
          form_data.append('_token', CSRF_TOKEN);
          if (discuss != '') {
            $.ajax({
              url: '/student/insert_discuss/'+lessonId,
              type: 'post',
              dataType: 'json',
              processData: false,
              contentType: false,
              async: false,
              data: form_data,
              success: function(data) {
                if (data.status == 'error') {
                  $('div#errorDiscuss div.alert').hide();
                  $('div#errorDiscuss').html(data.html);
                } else {
                  $('input[name="discuss"]').val('');
                  $('input[name="image_discuss"]').val('');
                  $('div#errorDiscuss div.alert').hide();
                  $('div#comment').after(data.html);
                }
              }
            });
          }
        }
      });
      // reply
      var userLessonId = '';
      $(document).on('click', 'a.reply', function() {
        var userLessonId = $(this).data('user-lesson-id');
        var divSub = '<div class="box-comment sub divSub">';
        divSub = divSub +'<div class="avatar"><i class="fa fa-user" aria-hidden="true"></i></div>';
        divSub = divSub +'<div class="right-comment">';
        divSub = divSub +'<div class="sub-anwer">';
        divSub = divSub +'<input type="text" value="Trả lời" name="sub_discuss" id="sub_discuss'+userLessonId+'" data-user-lesson-id="'+userLessonId+'">';
        divSub = divSub +'<input type="file" name="image_sub_discuss" style="display: none;" data-image-sub-discuss="'+userLessonId+'" accept="image/*">';
        divSub = divSub +'<div class="right">';
        divSub = divSub +'<a href="#" class="trans"><img src="/student/img/icon10.png" alt=" " /></a>';
        divSub = divSub +'<a href="#" class="trans image_sub_discuss" data-id='+userLessonId+'><img src="/student/img/icon11.png" alt=" " /></a>';
        divSub = divSub +'</div>';
        divSub = divSub +'</div>';
        divSub = divSub +'<div class="multy-image clearfix" data-div-image-sub-discuss="'+userLessonId+'" style="display: none;">';
        divSub = divSub +'<a class="close trans close-image-sub-discuss" data-close-image-sub-discuss="'+userLessonId+'" href="#"><i class="fa fa-times" aria-hidden="true"></i></a>';
        divSub = divSub +'<img src="{{ asset('student/img/image02.png') }}" alt="" />';
        divSub = divSub +'</div>';
        divSub = divSub +'</div>';
        divSub = divSub +'</div>';
        $('div#userLessonId'+userLessonId).show();
        $('div#userLessonId'+userLessonId).html(divSub);
        $('input#sub_discuss'+userLessonId).select();
      });

      $(document).on('click', 'a.image_sub_discuss', function() {
        var userLessonId = $(this).data('id');
        $('input[data-image-sub-discuss="'+userLessonId+'"]').click();
      });
      // select image sub discuss
      $(document).on('change', 'input[name="image_sub_discuss"]', function() {
        var userLessonId = $(this).data('image-sub-discuss');
        readURL(this, userLessonId);
      });
      // close image sub discuss
      $(document).on('click', 'a.close-image-sub-discuss', function() {
        var userLessonId = $(this).data('close-image-sub-discuss');
        $('div[data-div-image-sub-discuss="'+userLessonId+'"]').hide();
        $('input[data-image-sub-discuss="'+userLessonId+'"]').val('');
      });
      // insert sub discuss
      $(document).on('keyup', 'input[name="sub_discuss"]', function(e) {
        if (e.keyCode == 13) {
          var userLessonId = $(this).data('user-lesson-id');
          var content = $(this).val();
          var image_sub_discuss = $('input[data-image-sub-discuss="'+userLessonId+'"]').prop('files')[0];
          var image_sub_discuss_val = $('input[data-image-sub-discuss="'+userLessonId+'"]').val();
          if (content == '' && image_sub_discuss_val == '') {
            return false;
          }

          var form_data = new FormData();
          form_data.append('content', content);
          form_data.append('image_sub_discuss', image_sub_discuss);
          form_data.append('_token', CSRF_TOKEN);
          if (content != '' || image_sub_discuss != '') {
            $.ajax({
              url: '/student/insert_sub_discuss/'+userLessonId,
              type: 'post',
              dataType: 'json',
              processData: false,
              contentType: false,
              async: false,
              data: form_data,
              success: function(data) {
                $('input#sub_discuss'+userLessonId).val('');
                $('input[data-image-sub-discuss="'+userLessonId+'"]').val('');
                $('div#userLessonId'+userLessonId).hide();
                $('div#userLessonId'+userLessonId).before(data.html);
              }
            });
          }
        }
      });
      // like
      $(document).on('click', 'a.like', function() {
        var userLessonId = $(this).data('like');
        $.ajax({
          url: '/student/like_discuss/'+userLessonId,
          type: 'post',
          data: {_token: CSRF_TOKEN},
          success: function(data) {
            $('a[data-like="'+userLessonId+'"] span').html(data.count);
            if (data.status == 1) {
              $('a[data-like="'+userLessonId+'"]').addClass('active');
            } else {
              $('a[data-like="'+userLessonId+'"]').removeClass('active');
            }
          }
        });
      });
      // dislike
      $(document).on('click', 'a.dislike', function() {
        var userLessonId = $(this).data('dislike');
        $.ajax({
          url: '/student/dislike_discuss/'+userLessonId,
          type: 'post',
          data: {_token: CSRF_TOKEN},
          success: function(data) {
            $('a[data-dislike="'+userLessonId+'"] span').html(data.count);
            if (data.status == 1) {
              $('a[data-dislike="'+userLessonId+'"]').addClass('active');
            } else {
              $('a[data-dislike="'+userLessonId+'"]').removeClass('active');
            }
          }
        });
      });
      // delete synthetic
      $(document).on('click', 'a.deleteSynthetic', function() {
        var userLessonId = $(this).data('synthetic-id');
        $.ajax({
          url: '/student/delete_synthetic',
          type: 'post',
          data: {_token: CSRF_TOKEN, userLessonId: userLessonId},
          success: function(data) {
            if (data.status == 'success') {
              $('div#boxnote'+userLessonId).hide();
            } else {
              alert('Không xóa được !');
            }
          }
        });
      });
      // function add src image
      function readURL(input, userLessonId)
      {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('div[data-div-image-sub-discuss="'+userLessonId+'"]').show();
          $('div[data-div-image-sub-discuss="'+userLessonId+'"] img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }
    });
</script>
</body>
</html>