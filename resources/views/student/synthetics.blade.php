<div class="box-note" id="boxnote{{$synthetic->id}}">
  <div class="edit">
    <a data-fancybox="modal" data-src="#modal-fixup{{$synthetic->id}}" class="trans click-pop" href="javascript:;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
    <a class="trans deleteSynthetic" data-synthetic-id="{{$synthetic->id}}" href="#"><i class="fa fa-trash" aria-hidden="true"></i></a>
  </div>
  <h3 class="ttl-box-note text-underline">{{$synthetic->title}}</h3>
  <!-- <input type="text" class="ttl-box-note" /> -->
  <div class="date">
    @if($synthetic->updated_at == null)
      {{Carbon\Carbon::parse($synthetic->created_at)->format('H:i | d-m-Y')}}
    @else
      {{Carbon\Carbon::parse($synthetic->updated_at)->format('H:i | d-m-Y')}}
    @endif
  </div>
  <p class="text-underline">{{$synthetic->content}}</p>
  <!-- <textarea>Có thể chỉnh bằng Tuner hoặc chỉnh tay</textarea> -->
</div>
<div id="modal-fixup{{$synthetic->id}}" class="content-pop hidden">
  <a class="close trans" href="javascript:;" data-fancybox-close ><i class="fa fa-times" aria-hidden="true"></i></a>
  <h2 class="ttl-pop-rating">CHỈNH SỬA ĐÁNH GIÁ CỦA BẠN</h2>
  <div class="content-rating-pop">
    <input type="text" class="fix-ttl" name="title" value="{{$synthetic->title}}" />
    <textarea name="content" class="content-mess fixup">{{$synthetic->content}}</textarea>
    <div class="text-center">
      <button class="cmn-btn trans update" data-id="{{$synthetic->id}}">Lưu</button>
    </div>
  </div>
</div>