{!! Form::open(['route' => ['teacher.create-essay-questions', $lessonId], 'method' => 'POST', 'files' => true]) !!}
{!! Form::hidden('type', Q_ESSAY) !!}
<div class="">
    <div class="box-tu-luan bdt2">
        @include('errors.error')
        <div class="tool">
            {{--<img src="/teacher/img/tool-text.png" alt=" "/>--}}
            <span class="preview">
                <label>
                    {{ Form::checkbox('preview_flg', Q_PREVIEW, true) }}<span>Xem trước</span>
                </label>
            </span>
        </div>
        {!!  Form::textarea('content', null, ['class' => 'text-area-none', 'placeholder' =>
        'Soạn câu hỏi tự luận tại đây...']) !!}
    </div>
    <div class="box-tu-luan">
        {!!  Form::textarea('answer', null, ['class' => 'text-area-none', 'placeholder' => 'Soạn đán án chuẩn tại đây...']) !!}
    </div>
    <div class="box-tu-luan text">
        <table>
            <tr>
                <th style="width:250px" class="col-79 text-left">Thêm ảnh cho câu hỏi</th>
                <td>
                    <label for="question-image">
                        <i class="col-35 fa fa-picture-o" aria-hidden="true"></i>
                    </label>
                    {!! Form::file('image', ['id' => 'question-image', 'style' => "display:
                    none;"]) !!}
                    <img id="thumbnail_picture" src="" alt="">
                </td>
            </tr>
            <tr>
                <th class="col-79 text-left">Thêm video cho câu hỏi</th>
                <td><a href="#"><i class="col-35 fa fa-link fa-rotate-45"
                                   aria-hidden="true"></i></a></td>
            </tr>
        </table>
    </div>
    <div class="box-tu-luan box-sl">
        <table>
            <tr>
                <th style="width:119px" class="col-79 text-left">Độ khó</th>
                <td>
                    <ul class="list-style">
                        <li><label>
                                {{ Form::radio('level', Q_EASY, true) }}
                                <span>Dễ</span></label></li>
                        <li><label>
                                {{ Form::radio('level', Q_MEDIUM) }}
                                <span>Trung bình</span></label></li>
                        <li><label>
                                {{ Form::radio('level', Q_DIFFICUTL) }}
                                <span>Khó</span></label>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th class="col-79 text-left">Kiểu câu hỏi</th>
                <td>
                    <div class="cover-select">
                        {!!  Form::select('question_type', $questionType, null, ['class' =>
                        'form-control']) !!}
                    </div>
                </td>
            </tr>
        </table>
        <button class="submit pointer trans">
            <i class="fa fa-paper-plane" aria-hidden="true"></i>Lưu
        </button>
    </div>
</div>
{!! Form::close() !!}