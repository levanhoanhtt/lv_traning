<div id="tab-left-33" class="tab-content-js01">
    <div class="custom-bar get-height hidden-control">
      <div class="content-list-ask">
        <div class="filter-search clearfix">
          <div class="box-select">
            <p>Người soạn</p>
            <div class="cover-select2">
              <select>
                <option>Đình Thái1</option>
                <option>Đình Thái2</option>
                <option>Đình Thái3</option>
            </select>
        </div>
    </div>
    <div class="box-select">
        <p>Độ khó</p>
        <div class="cover-select2">
          <select>
            <option>Dễ</option>
            <option>Trung bình</option>
            <option>Khó</option>
        </select>
    </div>
</div>
<div class="box-select">
    <p>Trạng thái duyệt</p>
    <div class="cover-select2">
      <select>
        <option>Dã duyệt</option>
        <option>Chưa duyệt</option>
    </select>
</div>
</div>
<div class="box-select">
    <p>Trạng thái áp dụng</p>
    <div class="cover-select2">
      <select>
        <option>Đang áp dụng</option>
        <option>Chưa áp dụng</option>
    </select>
</div>
</div>
<div class="box-select long">
    <p>Lọc theo ngày</p>
    <div class="cover-select2 ">
      <select>
        <option>Trọn đời: 1 Tháng 12 2017 - 23 Tháng 12 2017</option>
        <option>Trọn đời: 1 Tháng 12 2017 - 23 Tháng 12 2017</option>
        <option>Trọn đời: 1 Tháng 12 2017 - 23 Tháng 12 2017</option>
    </select>
</div>
</div>
</div>
<div class="list-fliter">
  <div class="box-head-filter">
    <div class="col1">Mã câu hỏi</div>
    <div class="col2">Ngày soạn</div>
    <div class="col3">Người soạn</div>
    <div class="col4">Độ khó</div>
    <div class="col5">Trạng thái duyệt</div>
    <div class="col6">Trạng thái áp dụng</div>
</div>
<ul>
    <li class="active">
      <div class="body-filter">
        <div class="col1">TNV0001 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
        <div class="col2">20/12/2017 20:30</div>
        <div class="col3">Đình Thái-DT55</div>
        <div class="col4"><span class="yellow">Dễ</span></div>
        <div class="col5"><span class="green">Đã duyệt</span></div>
        <div class="col6"><span class="green">Đang áp dụng</span></div>
    </div>
    <div class="question-filter">
        <div class="ask">Có bao nhiêu con lợn trong một căn địa ốc 2 <br /> khi mà không có ai đó ở nhà ?</div>
        <div class="add-answer">
          <div class="clearfix">
            <ul class="list-style">
              <li><label><input name="ask01" type="radio"><span>1 đáp án 1 đáp án 1 đáp án 1 đáp án số mấy đây nhỉ.</span></label></li>
              <li><label><input name="ask01" type="radio"><span>1 đáp án 1 đáp án 1 đáp án 1 đáp án số mấy đây nhỉ.</span></label></li>
          </ul>
          <ul class="list-style">
              <li><label><input name="ask01" type="radio"><span>1 đáp án</span></label></li>
              <li><label><input name="ask01" type="radio"><span>1 đáp án</span></label></li>
          </ul>
      </div>
  </div>
</div>
</li>
<li>
  <div class="body-filter">
    <div class="col1">TNV0001 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
    <div class="col2">20/12/2017 20:30</div>
    <div class="col3">Đình Thái-DT55</div>
    <div class="col4"><span class="yellow">Dễ</span></div>
    <div class="col5"><span class="green">Đã duyệt</span></div>
    <div class="col6"><span class="green">Đang áp dụng</span></div>
</div>
<div class="question-filter">
    <div class="ask full">Có bao nhiêu con lợn trong một căn địa ốc 2 Có bao nhiêu con lợn trong một căn địa ốc 2 Có bao nhiêu con lợn trong một căn địa ốc 2 Có bao nhiêu con lợn trong khi mà không có ai đó ở nhà ?</div>
    
</div>
</li>
<li>
  <div class="body-filter">
    <div class="col1">TNV0001 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
    <div class="col2">20/12/2017 20:30</div>
    <div class="col3">Đình Thái-DT55</div>
    <div class="col4"><span class="yellow">Dễ</span></div>
    <div class="col5"><span class="green">Đã duyệt</span></div>
    <div class="col6"><span class="green">Đang áp dụng</span></div>
</div>
<div class="question-filter">
    <div class="ask">Có bao nhiêu con lợn trong một căn địa ốc 2 <br /> khi mà không có ai đó ở nhà ?</div>
    <div class="add-answer">
      <div class="clearfix">
        <ul class="list-style">
          <li><label><input name="ask01" type="radio"><span>1 đáp án 1 đáp án 1 đáp án 1 đáp án số mấy đây nhỉ.</span></label></li>
          <li><label><input name="ask01" type="radio"><span>1 đáp án 1 đáp án 1 đáp án 1 đáp án số mấy đây nhỉ.</span></label></li>
      </ul>
      <ul class="list-style">
          <li><label><input name="ask01" type="radio"><span>1 đáp án</span></label></li>
          <li><label><input name="ask01" type="radio"><span>1 đáp án</span></label></li>
      </ul>
  </div>
</div>
</div>
</li>
<li>
  <div class="body-filter">
    <div class="col1">TNV0001 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
    <div class="col2">20/12/2017 20:30</div>
    <div class="col3">Đình Thái-DT55</div>
    <div class="col4"><span class="yellow">Dễ</span></div>
    <div class="col5"><span class="green">Đã duyệt</span></div>
    <div class="col6"><span class="green">Đang áp dụng</span></div>
</div>
<div class="question-filter">
    <div class="ask">Có bao nhiêu con lợn trong một căn địa ốc 2 <br /> khi mà không có ai đó ở nhà ?</div>
    <div class="add-answer">
      <div class="clearfix">
        <ul class="list-style">
          <li><label><input name="ask01" type="radio"><span>1 đáp án 1 đáp án 1 đáp án 1 đáp án số mấy đây nhỉ.</span></label></li>
          <li><label><input name="ask01" type="radio"><span>1 đáp án 1 đáp án 1 đáp án 1 đáp án số mấy đây nhỉ.</span></label></li>
      </ul>
      <ul class="list-style">
          <li><label><input name="ask01" type="radio"><span>1 đáp án</span></label></li>
          <li><label><input name="ask01" type="radio"><span>1 đáp án</span></label></li>
      </ul>
  </div>
</div>
</div>
</li>
<li>
  <div class="body-filter">
    <div class="col1">TNV0001 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
    <div class="col2">20/12/2017 20:30</div>
    <div class="col3">Đình Thái-DT55</div>
    <div class="col4"><span class="yellow">Dễ</span></div>
    <div class="col5"><span class="green">Đã duyệt</span></div>
    <div class="col6"><span class="green">Đang áp dụng</span></div>
</div>
<div class="question-filter">
    <div class="ask">Có bao nhiêu con lợn trong một căn địa ốc 2 <br /> khi mà không có ai đó ở nhà ?</div>
    <div class="add-answer">
      <div class="clearfix">
        <ul class="list-style">
          <li><label><input name="ask01" type="radio"><span>1 đáp án 1 đáp án 1 đáp án 1 đáp án số mấy đây nhỉ.</span></label></li>
          <li><label><input name="ask01" type="radio"><span>1 đáp án 1 đáp án 1 đáp án 1 đáp án số mấy đây nhỉ.</span></label></li>
      </ul>
      <ul class="list-style">
          <li><label><input name="ask01" type="radio"><span>1 đáp án</span></label></li>
          <li><label><input name="ask01" type="radio"><span>1 đáp án</span></label></li>
      </ul>
  </div>
</div>
</div>
</li>
<li>
  <div class="body-filter">
    <div class="col1">TNV0001 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
    <div class="col2">20/12/2017 20:30</div>
    <div class="col3">Đình Thái-DT55</div>
    <div class="col4"><span class="yellow">Dễ</span></div>
    <div class="col5"><span class="green">Đã duyệt</span></div>
    <div class="col6"><span class="green">Đang áp dụng</span></div>
</div>
<div class="question-filter">
    <div class="ask">Có bao nhiêu con lợn trong một căn địa ốc 2 <br /> khi mà không có ai đó ở nhà ?</div>
    <div class="add-answer">
      <div class="clearfix">
        <ul class="list-style">
          <li><label><input name="ask01" type="radio"><span>1 đáp án 1 đáp án 1 đáp án 1 đáp án số mấy đây nhỉ.</span></label></li>
          <li><label><input name="ask01" type="radio"><span>1 đáp án 1 đáp án 1 đáp án 1 đáp án số mấy đây nhỉ.</span></label></li>
      </ul>
      <ul class="list-style">
          <li><label><input name="ask01" type="radio"><span>1 đáp án</span></label></li>
          <li><label><input name="ask01" type="radio"><span>1 đáp án</span></label></li>
      </ul>
  </div>
</div>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>