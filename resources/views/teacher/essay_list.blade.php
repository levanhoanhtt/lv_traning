<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-tools">
                    {!! Form::open(['route' => ['teacher.index', $lessonId], 'method'=>'GET']) !!}
                    <div class="input-group input-group-sm" style="width: 150px;">

                        <input type="text" name="name" class="form-control pull-right" placeholder="Tìm kiếm">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="box-body">
                @if($questionEssay->count() > 0)
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Mã câu hỏi</th>
                            <th>Ngày soạn</th>
                            <th>Người soạn</th>
                            <th>Độ khó</th>
                            <th>Trạng thái duyệt</th>
                            <th>Trạng thái áp dụng</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($questionEssay as $qe)
                            <tr id="brand-{{$qe->id}}">
                                <td><a href="{{url(route('teacher.index', $qe->id))}}">{{ $qe->code }}</a></td>
                                <td>{{ $qe->created_at }}</td>
                                <td>{{ $qe->creater->name }}</td>
                                <td>{!! $qe->present()->level !!}</td>
                                <td>{!! $qe->present()->approve !!}</td>
                                <td>{!! $qe->present()->apply !!}</td>
                                <td>
                                    cau tra loi
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>