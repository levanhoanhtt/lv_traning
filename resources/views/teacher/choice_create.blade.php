{!! Form::open(['route' => ['teacher.create-choice-questions', $lessonId], 'method' => 'POST', 'files' => true]) !!}
{!! Form::hidden('type', Q_CHOICE) !!}
<div class="">
    <div class="box-tu-luan box-sl stl2">
        @include('errors.error')
        <span class="preview stl2">
            <label>
                {{ Form::checkbox('preview_flg', Q_PREVIEW, true) }}<span>Xem trước</span>
            </label>
        </span>
        <table>
            <tr>
                <th style="width:119px" class="col-79 text-left">Số đáp án đúng</th>
                <td>
                    <ul class="list-style">
                        <li><label>
                            {{ Form::radio('number_of_ans', A_SELECT_ONE, true) }}
                            <span>1 đáp án</span></label>
                        </li>
                        <li><label>
                            {{ Form::radio('number_of_ans', A_SELECT_MULTI) }}
                            <span>Nhiều đáp án</span></label>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th class="col-79 text-left">Kiểu trả lời</th>
                <td>
                    <ul class="list-style">
                        <li><label>
                            {{ Form::radio('answer_type', A_SELECT_ONLY, true) }}
                            <span>Chỉ chọn</span></label>
                        </li>
                        <li><label>
                            {{ Form::radio('answer_type', A_SELECT_EXPLAIN) }}
                            <span>Chọn và giải thích phương án đã chọn</span></label>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
    <div class="box-tu-luan">
        <div class="tool">
            {{--<img src="../teacher/img/tool-text.png" alt=" "/>--}}

        </div>
        {!!  Form::textarea('content_choice', null, ['class' => 'text-area-none', 'placeholder' =>
        'Tiêu đề câu hỏi trắc nghiệm...']) !!}
    </div>
    <div class="box-tu-luan list-answer">
        <table id="list-answer">
            <thead>
            <tr class="col-79">
                <th colspan="2">List đáp án
                    <a class="btn btn-primary btn-add-answer" type="button">
                        <img src="{{ asset('teacher/img/icon21.png') }}"/>
                    </a>
                </th>
                <td style="width:138px">Ảnh ( Nếu có )</td>
                <td style="width:105px">Đáp án chuẩn</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th class="num">1</th>
                <th><input type="text" name="answer[]"
                           value="" placeholder="Nhập câu trả lời ...">
                </th>
                <td>
                    <label for="answer-image">
                        <i class="col-35 fa fa-picture-o" aria-hidden="true"></i>
                    </label>
                    <input type="file" name="image_ans[]" multiple id="answer-image" style="display: none;">
                </td>
                <td>
                    <ul class="list-style">
                        <li><label>
                                <input name="correct_flg[]" type="hidden" value="0">
                                <input name="correct_flg[]" type="radio" value="1"><span>&nbsp;</span></label>
                        </li>
                    </ul>
                </td>
                <td>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="box-tu-luan box-sl">
        <table>
            <tr>
                <th style="width:119px" class="col-79 text-left">Độ khó</th>
                <td>
                    <ul class="list-style">
                        <li><label>
                                {{ Form::radio('level', Q_EASY, true) }}
                                <span>Dễ</span></label></li>
                        <li><label>
                                {{ Form::radio('level', Q_MEDIUM) }}
                                <span>Trung bình</span></label></li>
                        <li><label>
                                {{ Form::radio('level', Q_DIFFICUTL) }}
                                <span>Khó</span></label>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th class="col-79 text-left">Kiểu câu hỏi</th>
                <td>
                    <div class="cover-select">
                        {!!  Form::select('question_type', $questionType, null, ['class' =>
                        'form-control']) !!}
                    </div>
                </td>
            </tr>
        </table>
        <button class="submit pointer trans answer-submit"><i class="fa fa-paper-plane"
                                                aria-hidden="true"></i>Lưu
        </button>
    </div>
</div>
