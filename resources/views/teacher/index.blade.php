<!DOCTYPE html>
<html lang="vi" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Ricky Training</title>
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:300,400,500,700,900&amp;subset=cyrillic,greek,vietnamese" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300i,400,500,700,900&amp;subset=vietnamese"
    rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('teacher/css/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('teacher/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('teacher/js/fancy/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('teacher/css/style.css') }}">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cleartype" content="on">
<![endif]-->
</head>
<body>

    <header class="clearfix">
        <div class="left">
            <div class="btn-menu"><i class="fa fa-bars" aria-hidden="true"></i></div>
            <h1 class="logo"><a href="#" class="trans"><img src="../teacher/img/logo.png" alt="Ricky studio"></a></h1>
        </div>
        <div class="right">
            <a href="#" class="icon notification trans"><i class="fa fa-bell-o" aria-hidden="true"></i></a>
            <a href="#" class="icon message trans"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
            <div class="infor-user">
                <div class="avatar"><i class="fa fa-user" aria-hidden="true"></i></div>
                <div class="name"><span>Hoàng Đình Thái<i class="fa fa-caret-down arrow" aria-hidden="true"></i></span>
                </div>
                <div class="position">Bộ phận sale</div>
                <div class="drop-down">
                    <div class="cover">
                        <ul>
                            <li><a href="#"><img src="../teacher/img/icon-menu01.png" alt=" "/>Tài khoản cá nhân</a></li>
                            <li><a href="#"><img src="../teacher/img/icon-menu02.png" alt=" "/>Học bạ</a></li>
                            <li class="active"><a href="#"><img src="../teacher/img/icon-menu03.png" alt=" "/>Khóa học của
                            tôi</a></li>
                            <li><a href="#"><img src="../teacher/img/icon-menu04.png" alt=" "/>Lịch sử giao dịch</a></li>
                            <li><a href="#"><img src="../teacher/img/icon-menu05.png" alt=" "/>Thoát</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="main" id="teacher">
        <div class="container dis-tb clearfix" cellpadding="10">

            <div class="cover-col col02 dis-cell">
                <div class=" col-mid main-content get-height2" style="background: #fff">
                    <div id="tabs-container">
                        <ul class="tabs-menu tabs-menu-js01 four-item stl2 clearfix">
                            <li class="current"><a href="#tab-left-11"><i class="fa fa-youtube-play" aria-hidden="true"></i>Nội
                            dung bài học</a></li>
                            <li><a href="#tab-left-22"><i class="fa fa-list" aria-hidden="true"></i>List soạn ({{ count($wishList) }})</a></li>
                            <li><a href="#tab-left-33"><i class="fa fa-list-ol" aria-hidden="true"></i>List câu hỏi ({{ $questions->count() }})</a>
                            </li>
                        </ul>
                        <div class="tab">
                            <div id="tab-left-11" class="tab-content tab-content-js01">
                                <div class="top-main has-ttl clearfix">
                                    <div class="left">
                                        <h2 class="ttl-h2-post">Bài 5 : Lorem ipsum dolor consectetur adipisicing elit?</h2>
                                    </div>
                                    <div class="right">
                                        <div class="view-style">Xem kiểu <a href="" class="has-sidebar active"></a><a href="" class="no-sidebar"></a>
                                        </div>
                                        <div class="rating ">Đánh giá
                                            <a data-fancybox="modal" data-src="#modal" class="trans click-pop" href="javascript:;">
                                                <div class="box-rating clearfix star2">
                                                    <label for="rd01"></label><input id="rd01" class="change-value" type="radio"
                                                    data-class="star1" value="1" name="rating01"/>
                                                    <label for="rd02"></label><input id="rd02" class="change-value" type="radio"
                                                    data-class="star2" value="2" name="rating01"/>
                                                    <label for="rd03"></label><input id="rd03" class="change-value" type="radio"
                                                    data-class="star3" value="3" name="rating01"/>
                                                    <label for="rd04"></label><input id="rd04" class="change-value" type="radio"
                                                    data-class="star4" value="4" name="rating01"/>
                                                    <label for="rd05"></label><input id="rd05" class="change-value" type="radio"
                                                    data-class="star5" value="5" name="rating01"/>
                                                </div>
                                            </a>
                                        </div>
                                        <a class="giude trans" href="#"><i class="fa fa-info-circle" aria-hidden="true"></i>Xem hướng
                                        dẫn</a>
                                    </div>
                                </div>
                                <div class="video">
                                    <img src="../teacher/img/video.jpg" alt="video"/>
                                </div>
                                <div class="control-video clearfix">
                                    <div class="left">
                                        Tự động chạy
                                        <div class="flatRoundedCheckbox">
                                            <input type="checkbox" name="check" id="flatOneRoundedCheckbox" value="1">
                                            <label for="flatOneRoundedCheckbox"></label>
                                            <div></div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <a href="" class="prev trans"><i class="fa fa-step-backward" aria-hidden="true"></i></a>
                                        <a href="" class="next trans"><i class="fa fa-step-forward" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            @include('teacher.wish_list')
                            </wish-list>
                            <list-question
                                :questions="{{ $questions->toJson() }}"
                                :lessonid="{{ $lessonId }}"
                                :admins="{{ $admins->toJson() }}"
                                :questionlevels="{{ $questionlevels->toJson() }}"
                                :questionapproves="{{ $questionapproves->toJson() }}"
                                :questionapplies="{{ $questionapplies->toJson() }}"
                            >
                            </list-question>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cover-col col04 dis-cell">
                <div class="col-right">
                    <ul class="tabs-menu tabs-menu-js four-item clearfix">
                        <li class="current"><a href="#tab-right-1"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Tự
                        luận</a></li>
                        <li><a href="#tab-right-2"><i class="fa fa-check-square-o" aria-hidden="true"></i>Trắc nghiệm</a>
                        </li>
                        <li><a href="#tab-right-3"><i class="fa fa-times" aria-hidden="true"></i>Ghép chéo</a></li>
                        <li><a href="#tab-right-4"><i class="fa fa-ban fa-rotate-452" aria-hidden="true"></i>Khuyết
                        thiếu</a></li>
                    </ul>
                    <div class="tab">
                        <div id="tab-right-1" class="tab-content tab-content-js">
                            @include('teacher.essay_create')
                        </div>
                        <div id="tab-right-2" class="tab-content tab-content-js">
                            @include('teacher.choice_create')
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div id="modal" class="content-pop hidden">
        <a class="close trans" href="javascript:;" data-fancybox-close><i class="fa fa-times" aria-hidden="true"></i></a>
        <h2 class="ttl-pop-rating">CHÚNG TÔI RẤT BIẾT ƠN ĐÁNH GIÁ CỦA BẠN</h2>
        <div class="content-rating-pop">
            <div class="detail-rating">
                <table>
                    <tr>
                        <th>Bài giảng</th>
                        <td>
                            <div class="box-rating clearfix star2">
                                <label for="rd011"></label><input id="rd011" class="change-value" type="radio"
                                data-class="star1" value="1" name="rating01"/>
                                <label for="rd012"></label><input id="rd012" class="change-value" type="radio"
                                data-class="star2" value="2" name="rating01"/>
                                <label for="rd013"></label><input id="rd013" class="change-value" type="radio"
                                data-class="star3" value="3" name="rating01"/>
                                <label for="rd014"></label><input id="rd014" class="change-value" type="radio"
                                data-class="star4" value="4" name="rating01"/>
                                <label for="rd015"></label><input id="rd015" class="change-value" type="radio"
                                data-class="star5" value="5" name="rating01"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Video</th>
                        <td>
                            <div class="box-rating clearfix star2">
                                <label for="rd021"></label><input id="rd021" class="change-value" type="radio"
                                data-class="star1" value="1" name="rating01"/>
                                <label for="rd022"></label><input id="rd022" class="change-value" type="radio"
                                data-class="star2" value="2" name="rating01"/>
                                <label for="rd023"></label><input id="rd023" class="change-value" type="radio"
                                data-class="star3" value="3" name="rating01"/>
                                <label for="rd024"></label><input id="rd024" class="change-value" type="radio"
                                data-class="star4" value="4" name="rating01"/>
                                <label for="rd025"></label><input id="rd025" class="change-value" type="radio"
                                data-class="star5" value="5" name="rating01"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Giảng viên</th>
                        <td>
                            <div class="box-rating clearfix star2">
                                <label for="rd031"></label><input id="rd031" class="change-value" type="radio"
                                data-class="star1" value="1" name="rating01"/>
                                <label for="rd032"></label><input id="rd032" class="change-value" type="radio"
                                data-class="star2" value="2" name="rating01"/>
                                <label for="rd033"></label><input id="rd033" class="change-value" type="radio"
                                data-class="star3" value="3" name="rating01"/>
                                <label for="rd034"></label><input id="rd034" class="change-value" type="radio"
                                data-class="star4" value="4" name="rating01"/>
                                <label for="rd035"></label><input id="rd035" class="change-value" type="radio"
                                data-class="star5" value="5" name="rating01"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <textarea class="content-mess" placeholder="Mô tả cảm nghĩ của bạn tại đây"></textarea>
            <div class="text-center">
                <button class="cmn-btn trans">Gửi đánh giá</button>
            </div>
        </div>
    </div>
</div>
<footer>
</footer>
{!! Html::script(mix('js/manifest.js')) !!}
{!! Html::script(mix('js/vendor.js')) !!}
<script src="{{ asset('teacher/js/jquery-1.12.3.min.js') }}"></script>
<script src="{{ asset('teacher/js/modernizr.js') }}"></script>
<script src="{{ asset('teacher/js/flexibility.min.js') }}"></script>
<script src="{{ asset('teacher/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('teacher/js/jquery.placeholder.js') }}"></script>
<script src="{{ asset('teacher/js/fancy/jquery.fancybox.js') }}"></script>
<script src="{{ asset('teacher/js/scripts.js') }}"></script>
{!! Html::script(mix('js/teacher.js')) !!}


<script> flexibility(document.documentElement); </script>

<script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
<script>
    jQuery(document).ready(function($) {
    $(document).ready(function () {
    $('input[type=file][name=image]').on('change', function (evt) {
    var file = evt.target.files[0];
    readFile(file);
});

function readFile(file) {
var preview = $('#thumbnail_picture');
var reader = new FileReader();
reader.onload = function (e) {
preview.attr('src', e.target.result).height(150);
}
if (file) {
reader.readAsDataURL(file);
} else {
preview.src = '';
}
}

});

$(document).on('click','.btn-add-answer',function(event) {
var tr = '<th class="num">1</th>\n' +
'                <th><input type="text" name="answer[]"\n' +
    '                           value="" placeholder="Nhập câu trả lời ...">\n' +
'                </th>\n' +
'                <td>\n' +
    '                    <label for="answer-image">\n' +
        '                        <i class="col-35 fa fa-picture-o" aria-hidden="true"></i>\n' +
    '                    </label>\n' +
    '                    <input type="file" name="image_ans[]" multiple id="answer-image" style="display: none;">\n' +
'                </td>\n' +
'                <td>\n' +
    '                    <ul class="list-style">\n' +
        '                        <li><label>\n' +
            '                                <input name="correct_flg[]" type="hidden" value="0">\n' +
            '                                <input name="correct_flg[]" type="radio" value="1"><span>&nbsp;</span></label>\n' +
        '                        </li>\n' +
    '                    </ul>\n' +
'                </td>';
$("#list-answer").find('tbody')
.append($('<tr>')
    .append(tr)
    .append($('<td>')
        .append('<a href="javascript:;" class="btn-remove" style="color:#E26A6A"> <i class="fa fa-trash"></i></a>')
        )
        );
    });

    $(document).on('click', '.btn-remove', function(event) {
    event.preventDefault();
    $(this).closest('tr').remove();
});

$(document).on('click', '.answer-submit', function(event) {
$("#list-answer input[name='correct_flg[]']:checked").prev().remove();
});
});

</script>
<script>
    CKEDITOR.plugins.addExternal('confighelper','https://martinezdelizarrondo.com/ckplugins/confighelper/');
    CKEDITOR.replace( 'content', {
    extraPlugins: 'confighelper',
    height: 194
} );
CKEDITOR.replace( 'answer', {
extraPlugins: 'confighelper',
height: 98,
} );
CKEDITOR.replace( 'content_choice', {
extraPlugins: 'confighelper',
height: 194
} );
</script>
<script>
    $(".delete").on("click", function(){
        var self = $(this);

        var lessonAddId = $(this).data('id');
        var lessonId = {{ $lessonId }};
        var srcAdd = {!! '"'.$srcAdd.'"' !!};
        var srcDelete = {!! '"'.$srcDelete.'"' !!};
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
        $.ajax({
            type: 'GET',
            url: '/teacher/'+lessonId+'/add-wish-list',
            cache: false,
            data: {
                'lessonAddId': lessonAddId
            },
            success: function(data){
                if(data.action == 'delete'){
                    var classAdd = 'watching'
                    var classRemove = 'watched'
                    self.parent().parent().find('.col01').addClass(classAdd).removeClass(classRemove)
                    self.find('img').attr("src", srcAdd);
                }
                if(data.action == 'add'){
                    var classAdd = 'watched'
                    var classRemove = 'watching'
                    self.parent().parent().find('.col01').addClass(classAdd).removeClass(classRemove)
                    self.find('img').attr("src", srcDelete);
                }
            }
        });
    })
</script>
</body>
</html>