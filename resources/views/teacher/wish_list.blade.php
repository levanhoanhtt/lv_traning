<div class="tab-content-js01" id="tab-left-22">
    <div class="get-height ">
        <div class="content-listsoan">
            <div class="box-search-soan">
                <form method="GET" action="{{ route('teacher.index',['lessonId' => $lessonId]) }}">
                    <button>
                        <i aria-hidden="true" class="fa fa-search">
                        </i>
                    </button>
                    <input type="text" name="lesson_name" value="{{ request('lesson_name') }}">
                </form>
            </div>
            <div class="box-list-soan">
                <div class="head-list-soan">
                    <div class="col01">
                        List bài học của bạn
                    </div>
                    <div class="col02">
                        Ngày thêm
                    </div>
                    <div class="col03">
                        Lĩnh vực kiến thức
                    </div>
                    <div class="col04">
                        Số câu hỏi bạn đã soạn
                    </div>
                </div>
                <div class="custom-bar get-height5 hidden-control">
                    @foreach ($lessonLists as $lesson)
                        <div class="body-list-soan">
                            <div class="col01  {{ in_array($lesson->id, $wishList)? 'watched' : 'watching' }}">
                                <p class="ttl">
                                    {{ $lesson->name }}
                                </p>
                                <p>
                                    <a class="trans time" href="#">
                                        <i aria-hidden="true" class="fa fa-play-circle">
                                        </i>
                                        03:45
                                    </a>
                                    <a class="trans" href="#">
                                        <i aria-hidden="true" class="fa fa-download">
                                        </i>
                                    </a>
                                    <a class="trans" href="#">
                                        <i aria-hidden="true" class="fa fa-comments">
                                        </i>
                                    </a>
                                </p>
                            </div>
                            <div class="col02">
                                {{ $lesson->created_at->format('d/m/y') }}
                            </div>
                            <div class="col03">
                                {{ $lesson->knowledgeArea->name }}
                            </div>
                            <div class="col04">
                                <p>
                                    <span class="num">
                                        {{ $lesson->questionsEssayUser->count() }}
                                    </span>
                                    câu ( Tổng
                                    <span class="num">
                                        {{ $lesson->questionsEssay->count() }}
                                    </span>
                                    câu ) - Tự luận
                                </p>
                                <p>
                                    <span class="num">
                                        {{ $lesson->questionsChoiceUser->count() }}
                                    </span>
                                    câu ( Tổng
                                    <span class="num">
                                        {{ $lesson->questionsChoice->count() }}
                                    </span>
                                    câu ) - Trắc nghiệm
                                </p>
                                <a class="delete" data-id="{{ $lesson->id }}">
                                    @if (in_array($lesson->id, $wishList))
                                        <img alt=" " src="{{  $srcDelete }}"/>
                                    @else
                                        <img alt=" " src="{{  $srcAdd }}"/>
                                    @endif
                                </a>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
</div>