
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

window.Vue = require('vue')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('curriculum-content', require('./components/Admin/CurriculumContent.vue'))
Vue.component('curriculum-content-edit', require('./components/Admin/CurriculumContentEdit.vue'))
Vue.component('test-content', require('./components/Admin/TestContent.vue'))


const app = new Vue({
  el: '#admin'
})

$(function() {
  require('./shared')
  $(".level-lesson .fa-star").click(
	 	function () {
	 		$(this).prevAll().addClass('active');
	 		$(this).nextAll().removeClass('active');
			$(this).addClass('active');
			$('input:hidden[name=level]').val($(this).prevAll().length +1);
		}
	);
})
