
$(function() {


  $(document).on('click', '.form-submit', function (e) {
    e.preventDefault()
    const $this = $(this)
    const form = $this.data('form') || false
    const $form = form ? $(form) : $this.closest('form')

    if ($form.length) {
      $form.submit()
    }
  })

  $(document).on('click', '[data-swal]', function(e) {
    e.preventDefault()

    const $this = $(this)
    const url = $this.attr('href')
    const options = $this.data('swal') || {}
    const data = $this.data('swalData') || {}
    const method = $this.data('swalMethod') || 'GET'

    swal(options).then(() => {
      if (url) {
        axios.request({
          url,
          method,
          data
        }).then(({ data }) => {
          if (data.redirect) {
            setTimeout(() => {
              if (data.redirect === true) {
                window.location.reload()
              } else {
                window.location = data.redirect;
              }
            }, 2000)
          }
        })
      }
    }, dismiss => {
      // console.log(dismiss)
    })
  })
})
