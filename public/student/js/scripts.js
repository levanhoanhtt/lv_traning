var app = app || {};

app.init = function () {

  app.custom_bar();
  app.tab();
  app.accordion();
  app.change_value_input();
  app.placeholcer();
  app.up_down();
  $(window).on('load resize', function () {
    app.set_height();
  });
}

app.custom_bar = function(){
  $.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
  $.mCustomScrollbar.defaults.axis="yx"; //enable 2 axis scrollbars by default
  $(".custom-bar").mCustomScrollbar({theme:"dark-2"}); 
}

app.tab = function (){
	$(".tabs-menu-js01 a").click(function(event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href");
    $(".tab-content-js01").not(tab).css("display", "none");
    $(tab).fadeIn();
  });

  $(".tabs-menu-js a").click(function(event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href");
    $(".tab-content-js").not(tab).css("display", "none");
    $(tab).fadeIn();
  });


}

app.accordion = function () {
  $(".head-accor-js").click(function () {
    $(this).toggleClass("active");
    $(this).next(".body-accor-js").stop().slideToggle();
    return false;
  })
}

app.set_height = function(){
  var height_head = 80;
  var height_margin = 40;
  var height_tab = 61;
  var height_col = $( window ).height() - height_head - height_margin -height_tab -5;
  var height_col2 = $( window ).height() - height_head - height_margin;
  var height_top_detail = height_col2 - $(".top-main").outerHeight() - 16;
  $(".get-height").css("height",height_col);
  $(".get-height2").css("height",height_col2);
  $(".get-height3").css("height",height_top_detail);
}

app.change_value_input = function(){
  $(".change-value").change(function() {    
    var star = $(this).attr("data-class");
    $(this).parent().removeClass("star1");
    $(this).parent().removeClass("star2");
    $(this).parent().removeClass("star3");
    $(this).parent().removeClass("star4");
    $(this).parent().removeClass("star5");
    $(this).parent().addClass(star);
    
  });
}

app.fancybox = function (){
  
  $('[data-fancybox]').fancybox({
    smallBtn : true
  })

}

app.up_down = function(){
  $(".up").click(function(){
    
 
    $(this).parent().prev().children().children(".mCSB_scrollTools").children(".mCSB_buttonUp").click();
    $(this).parent().prev().children().children(".mCSB_scrollTools").children(".mCSB_buttonUp").addClass("sdsd");
  });
}


app.placeholcer = function (){
  $('input, textarea').placeholder();
}

$(function() {

  app.init();

});