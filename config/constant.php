<?php

/*
  |--------------------------------------------------------------------------
  | Application constants
  |--------------------------------------------------------------------------
  |
 */
// check if DEFINE_CONSTANT is defined.
// usually this file is not loaded twice or more,
// but this file is loaded during every unit test is called.
if (! defined('DEFINE_CONSTANT')) {
    define('DEFINE_CONSTANT', 1);

    define('ITEM_PER_PAGE', 10);

    // Role
    define('ROLE_ADMIN', 'admin');

    define('SESSION_LOCALE_NAME', '_locale');

    // Lesson type
    define('L_AUDIO', 'audio'); // audio
    define('L_BLOG', 'blog'); // blog
    define('L_VIDEO', 'video'); // video

    // Questions type
    define('Q_ESSAY', 'essay'); //tự luận
    define('Q_CHOICE', 'choice'); // trắc nghiệm
    define('Q_MATCH', 'match'); //ghép chéo
    define('Q_MISSING', 'missing'); //khuyết thiếu

    // Questions preview
    define('Q_NO_PREVIEW', 0); // không xem trước
    define('Q_PREVIEW', 1); // xem trước

    // Questions level
    define('Q_EASY', 0); // dễ
    define('Q_MEDIUM', 1); // trng bình
    define('Q_DIFFICUTL', 2); // khó

    // Question Approve status
    define('Q_PENDING', 0); // chờ duyệt
    define('Q_APPROVED', 1); // đã duyệt
    define('Q_CANCLE', 2); // Không duyệt
    define('Q_CONSIDER', 3); // Xem xét

    // Question Apply status
    define('Q_PAUSE', 0); // Tạm dừng
    define('Q_APPLIED', 1); // đng áp dụng

    // Number of answer
    define('A_SELECT_ONE', 0); // chọn 1
    define('A_SELECT_MULTI', 1); // chọn nhiều

    // Answer type
    define('A_SELECT_ONLY', 0); // chỉ chọn
    define('A_SELECT_EXPLAIN', 1); // chọn và giải thích

    // Answer correct
    define('A_NOT_CORRECT', 0); // không phải đáp án đúng
    define('A_IS_CORRECT', 1); // đáp án đúng
}

$configs = [

    'LESSON_TYPE' => [
        L_AUDIO => 'Audio',
        L_BLOG => 'Blog',
        L_VIDEO => 'Video',
    ],

    'LEVEL' => [
        0   => 'Dễ',
        1   => 'Trung bình',
        2   => 'Khó',
    ],

    'STATUS' => [
        0   => 'Không hoạt động',
        1   => 'Hoạt động',
    ],

    'QUESTION_CATE' => [
        Q_ESSAY     => 'Tự luận',
        Q_CHOICE    => 'Trắc nghiệm',
        Q_MATCH     => 'Ghép chéo',
        Q_MISSING   =>  'Khuyết thiếu'
    ],

    'QUESTION_TYPE' => [
        1   => 'Tư duy',
        2   => 'Logic',
        3   => 'Câu hỏi mở',
    ],

    'QUESTION_APPROVE' => [
        0   => 'Chờ duyệt',
        1   => 'Đã duyệt',
        2   => 'Không duyệt',
        3   => 'Xem xét',
    ],

    'QUESTION_APPLY' => [
        0   => 'Tạm dừng',
        1   => 'Đang áp dụng',
    ]

];

return $configs;
