<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Name of route
    |--------------------------------------------------------------------------
    |
    | Enter the routes name to enable dynamic imagecache manipulation.
    | This handle will define the first part of the URI:
    |
    | {route}/{template}/{filename}
    |
    | Examples: "images", "img/cache"
    |
    */

    'route' => 'images',

    /*
    |--------------------------------------------------------------------------
    | Storage paths
    |--------------------------------------------------------------------------
    |
    | The following paths will be searched for the image filename, submited
    | by URI.
    |
    | Define as many directories as you like.
    |
    */

    'paths' => [
        storage_path('app/public/upload'),
        public_path('images')
    ],

    /*
    |--------------------------------------------------------------------------
    | Manipulation templates
    |--------------------------------------------------------------------------
    |
    | Here you may specify your own manipulation filter templates.
    | The keys of this array will define which templates
    | are available in the URI:
    |
    | {route}/{template}/{filename}
    |
    | The values of this array will define which filter class
    | will be applied, by its fully qualified name.
    |
    */

    'templates' => [
        'small'     => Intervention\Image\Templates\Small::class,
        'medium'    => Intervention\Image\Templates\Medium::class,
        'large'     => Intervention\Image\Templates\Large::class,
        'hd'        => App\Support\Image\HD::class,
        'fullhd'    => App\Support\Image\FullHD::class,

        // Square
        'square32'  => App\Support\Image\Square32::class,
        'square64'  => App\Support\Image\Square64::class,
        'square128' => App\Support\Image\Square128::class,
        'square256' => App\Support\Image\Square256::class,
        'square300' => App\Support\Image\Square300::class,
        'square512' => App\Support\Image\Square512::class,

        //Circle
        'circle32'  => App\Support\Image\Circle32::class,

        'blog-small' => App\Support\Image\Blog\Small::class,
        'showcase-small' => App\Support\Image\Showcase\Small::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Image Cache Lifetime
    |--------------------------------------------------------------------------
    |
    | Lifetime in minutes of the images handled by the imagecache route.
    |
    */

    'lifetime' => 43200,

);
